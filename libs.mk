###
 #
 #                           γ-TRIS
 #
 #       Graph-Algorithm Making Multi-labeling Annotations
 #             for Tracking Retroviral Integration Sites
 #
 # Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 #
 # Distributed under the terms of the GNU General Public License (or the Lesser
 # GPL).
 #
 # This file is part of γ-TRIS.
 #
 # γ-TRIS is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 # 
 # γ-TRIS is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 # GNU General Public License for more details.
 # 
 # You should have received a copy of the GNU General Public License
 # along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 #
###

# LIBS
BLAST_DB_DATA_LOADER_LIBS = \
	-lncbi_xloader_blastdb_rmt \
	-lncbi_xloader_blastdb \

BLAST_LIBS = \
	-lxalgoalignutil \
	-lxalgoalignnw \
	-lxalnmgr \
	-lxblast \
	-lxalgoblastdbindex \
	-lcomposition_adjustment \
	-lxalgowinmask \
	-lxalgodustmask \
	-lblast_services \
	-lwritedb \
	-lseqdb \
	-lxobjread \
	-lblastdb \
	-lxnetblastcli \
	-lxnetblast \
	-lscoremat \
	-lxobjutil \
	-lcreaders \
	-ltables \

OBJMGR_LIBS:= \
	-lncbi_xloader_genbank \
	-lncbi_xreader_id1 \
	-lncbi_xreader_id2 \
	-lncbi_xreader_cache \
	-lxobjmgr \
	-lgenome_collection \
	-lseq \
	-lseqcode \
	-lncbi_xreader \
	-lid2 \
	-lid1 \
	-lseqsplit \
	-lseqset \
	-lpub \
	-lmedline \
	-lbiblio \
	-lgeneral \
	-lxser \
	-lsequtil \
	-lxcompress \
	-lxutil \
	-lxncbi \
	-lz \
	-lbz2 \

LIBS:= \
	-L${LOC_DIR}/lib \
	-lblastinput \
	-lalign_format \
	$(BLAST_DB_DATA_LOADER_LIBS) \
	$(BLAST_LIBS) \
	-lxconnect \
	$(OBJMGR_LIBS) \
	-lnsl \
	-ldl \
	-lpthread \

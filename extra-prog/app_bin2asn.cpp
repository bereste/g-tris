#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <sstream>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

using namespace std;

class bin2asn : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
};

void bin2asn::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions); \
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Program to convert ASN file from binary to text.");

    arg_desc->AddKey
        ("i", "input_ans_file",
         "Input ASN file.",
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey
        ("t", "file_type",
         "Input file type: 'map' or 'aln'.",
         CArgDescriptions::eString);

     // Setup arg.descriptions for this application
     SetupArgDescriptions(arg_desc.release());
}

int bin2asn::Run(void) {
    const CArgs& args = GetArgs();

    CFile in_file = args["i"].AsString();
    if(!in_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }

    if(args["t"].AsString() != "map" &&
       args["t"].AsString() != "aln") {
	ERROR("File type " << args["t"].AsString() << " not recognized.");
	ERROR("Specify a valid type: 'map' or 'aln'.");
	return 1;
    }
    auto_ptr< CObjectIStream > in_file_stream
	(CObjectIStream::Open(ASN_TYPE, args["i"].AsString()));
    if(!in_file_stream->InGoodState()) {
	ERROR("Error in opening " << args["i"].AsString() << " file.");
	return 1;
    }

    if(args["t"].AsString() == "map") {
	INFO("Reading MAP file.");
	UniqueSeqs unique_seq_map;
	*in_file_stream >> unique_seq_map;
	auto_ptr< CObjectOStream > out_stream
	    (CObjectOStream::Open(eSerial_AsnText, std::cout));
	*out_stream << unique_seq_map;
	INFO("Finished MAP file.");
    }
    if(args["t"].AsString() == "aln") {
	INFO("Reading ALN file.");
	auto_ptr<CObjectIStream> in_asn_stream
        (CObjectIStream::Open(ASN_TYPE, args["i"].AsString()));
	if(!in_asn_stream->InGoodState()) {
		ERROR("Error in opening " << args["i"].AsString() << " file.");
		return 1;
	}
	auto_ptr< CObjectOStream > out_stream
	    (CObjectOStream::Open(eSerial_AsnText, std::cout));
	//Create initial graph
	while(!in_asn_stream->EndOfData()) {
	  Alignments al;
	  *in_asn_stream >> al;
	  *out_stream << al;
	}
	INFO("Finished ALN file.");
    }
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");                                                                                             
    bin2asn app;
    return app.AppMain(argc, argv);
}

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <sstream>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class ValAssey : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
};

void ValAssey::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions); \
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
			      "Program to Merge Cluster Count Measures.");

    arg_desc->AddKey
	("l", "cluster_file_list",
	 "Input list of cluster files.",
	 CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddOptionalKey
	("i", "integrations_file",
	 "File containing the integrations to be found in TSV format.",
	 CArgDescriptions::eInputFile);

     // Setup arg.descriptions for this application
     SetupArgDescriptions(arg_desc.release());
}

int ValAssey::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));

    //Check input files
    CFile in_int_file = args["i"].AsString();
    if(!in_int_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }

    CFile in_list_file = args["l"].AsString();
    if(!in_list_file.Exists()) {
	ERROR("File " << args["l"].AsString() << " not found.");
	return 1;
    }

    //Output matrices
    string out_matr_clu_file = in_int_file.GetDir() + "clu_count.csv";
    ofstream out_matr_clu_stream(out_matr_clu_file.c_str());
    if(!out_matr_clu_stream.is_open()) {
	ERROR("Error in creating " << out_matr_clu_file << " file.");
	return 1;
    }

    string out_matr_tag_file = in_int_file.GetDir() + "tag_count.csv";
    ofstream out_matr_tag_stream(out_matr_tag_file.c_str());
    if(!out_matr_tag_stream.is_open()) {
	ERROR("Error in creating " << out_matr_tag_file << " file.");
	return 1;
    }

    string out_matr_shear_file = in_int_file.GetDir() + "shear_count.csv";
    ofstream out_matr_shear_stream(out_matr_shear_file.c_str());
    if(!out_matr_shear_stream.is_open()) {
	ERROR("Error in creating " << out_matr_shear_file << " file.");
	return 1;
    }

    string out_matr_combo_file = in_int_file.GetDir() + "combo_count.csv";
    ofstream out_matr_combo_stream(out_matr_combo_file.c_str());
    if(!out_matr_combo_stream.is_open()) {
	ERROR("Error in creating " << out_matr_combo_file << " file.");
	return 1;
    }

    string FS = ",";
    int dist = 3;

    INFO("PARAMETERS");
    INFO("Input integrations file: " << args["i"].AsString());
    INFO("Input list cluster file: " << args["l"].AsString());
    INFO("Output CLU count file: " << out_matr_clu_file);
    INFO("Output TAG count file: " << out_matr_tag_file);
    INFO("Output SHEAR count file: " << out_matr_shear_file);
    INFO("Output COMBO count file: " << out_matr_combo_file);

    INFO("Started reading input files.");
    string line;
    std::list< std::pair< string, double > > integration_list;
    std::ifstream in_int_stream(args["i"].AsString().c_str());
    if(!in_int_stream.is_open()) {
	ERROR("Error in opening " << args["i"].AsString() << " file.");
	return 1;
    }
    while(std::getline(in_int_stream, line)) {
	size_t pos = line.find("\t", 0);
	if(pos == string::npos) {
	    ERROR("Error in parsing integration entry.");
	    return 1;
	}
	string int_chr = line.substr(0, pos);
	double int_pos = atof(line.substr(pos + 1).c_str());
	INFO(line << ": " << int_chr << " -- " << int_pos);
	integration_list.push_back(std::make_pair(int_chr, int_pos));
    }
    in_int_stream.close();
    INFO("Num. integrations: " << integration_list.size());

    std::vector< string > ds_list;
    std::ifstream in_ds_stream(args["l"].AsString().c_str());
    if(!in_ds_stream.is_open()) {
	ERROR("Error in opening " << args["l"].AsString() << " file.");
	return 1;
    }
    while(std::getline(in_ds_stream, line)) {
	ds_list.push_back(line);
    }
    in_ds_stream.close();
    INFO("Num. datasets: " << ds_list.size());
    INFO("Finished reading input files.");

    std::list< std::pair< string, double > >::iterator hint_it;
    out_matr_clu_stream << "Dataset";
    out_matr_tag_stream << "Dataset";
    out_matr_shear_stream << "Dataset";
    out_matr_combo_stream << "Dataset";
    for(hint_it = integration_list.begin(); hint_it != integration_list.end(); ++hint_it) {
	out_matr_clu_stream << FS << hint_it->first << "-" << hint_it->second;
	out_matr_tag_stream << FS << hint_it->first << "-" << hint_it->second;
	out_matr_shear_stream << FS << hint_it->first << "-" << hint_it->second;
	out_matr_combo_stream << FS << hint_it->first << "-" << hint_it->second;
    }
    out_matr_clu_stream << FS << "Total\n";
    out_matr_tag_stream << FS << "Total\n";
    out_matr_shear_stream << FS << "Total\n";
    out_matr_combo_stream << FS << "Total\n";

    std::vector< string >::iterator ds_it;
    for(ds_it = ds_list.begin(); ds_it != ds_list.end(); ++ds_it) {
	INFO("Processing dataset " << *ds_it);
	{
	    string cs = *ds_it + ".clu";
	    auto_ptr< CObjectIStream > in_clu_stream
		(CObjectIStream::Open(CLU_TYPE, cs));
	    if(!in_clu_stream->InGoodState()) {
		ERROR("Error in opening " << cs << " file.");
		return 1;
	    }
	    std::list< ISCluster  > clist;
	    while (!in_clu_stream->EndOfData()) {
		ISCluster c;
		*in_clu_stream >> c;
		clist.push_back(c);
	    }
	    out_matr_clu_stream << *ds_it;
	    int num_integ = 0;
	    std::list< std::pair< string, double > >::iterator int_it;
	    for(int_it = integration_list.begin();
		int_it != integration_list.end(); ++int_it) {
		bool found = false;
		int weight = 0;
		for(std::list< ISCluster>::iterator cit = clist.begin();
		    cit != clist.end(); cit++) {
		    std::vector< Label >::iterator lab_it;
		    for(lab_it = cit->lab.begin(); lab_it != cit->lab.end(); lab_it++) {
			if(lab_it->target_id == int_it->first &&
			   abs(lab_it->centroid - int_it->second) < dist) {
			    if(found) {
				WARN("Integration already found!");
			    } else {
				num_integ++;
			    }
			    if(cit->weight > weight) {
				weight = cit->weight;
			    }
			    found = true;
			    continue;
			}
		    }
		}
		out_matr_clu_stream << FS << weight;
	    }
	    int total_w = 0;
	    for(std::list< ISCluster>::iterator cit = clist.begin();
		cit != clist.end(); cit++) {
		total_w += cit->weight;
	    }
	    out_matr_clu_stream << FS << total_w;
	    out_matr_clu_stream << "\n";
	    //INFO(*ds_it << ": " << num_integ);
	}
	{
	    string cs = *ds_it + ".tag";
	    auto_ptr< CObjectIStream > in_clu_stream
		(CObjectIStream::Open(CLU_TYPE, cs));
	    if(!in_clu_stream->InGoodState()) {
		ERROR("Error in opening " << cs << " file.");
		return 1;
	    }
	    std::list< ISCluster  > clist;
	    while (!in_clu_stream->EndOfData()) {
		ISCluster c;
		*in_clu_stream >> c;
		clist.push_back(c);
	    }
	    out_matr_tag_stream << *ds_it;
	    int num_integ = 0;
	    std::list< std::pair< string, double > >::iterator int_it;
	    for(int_it = integration_list.begin();
		int_it != integration_list.end(); ++int_it) {
		bool found = false;
		int weight = 0;
		for(std::list< ISCluster>::iterator cit = clist.begin();
		    cit != clist.end(); cit++) {
		    std::vector< Label >::iterator lab_it;
		    for(lab_it = cit->lab.begin(); lab_it != cit->lab.end(); lab_it++) {
			if(lab_it->target_id == int_it->first &&
			   abs(lab_it->centroid - int_it->second) < dist) {
			    if(found) {
				WARN("Integration already found!");
			    } else {
				num_integ++;
			    }
			    if(cit->weight > weight) {
				weight = cit->weight;
			    }
			    found = true;
			    continue;
			}
		    }
		}
		out_matr_tag_stream << FS << weight;
	    }
	    int total_w = 0;
	    for(std::list< ISCluster>::iterator cit = clist.begin();
		cit != clist.end(); cit++) {
		total_w += cit->weight;
	    }
	    out_matr_tag_stream << FS << total_w;
	    out_matr_tag_stream << "\n";
	    //INFO(*ds_it << ": " << num_integ);
	}
	{
	    string cs = *ds_it + ".shear";
	    auto_ptr< CObjectIStream > in_clu_stream
		(CObjectIStream::Open(CLU_TYPE, cs));
	    if(!in_clu_stream->InGoodState()) {
		ERROR("Error in opening " << cs << " file.");
		return 1;
	    }
	    std::list< ISCluster > clist;
	    while (!in_clu_stream->EndOfData()) {
		ISCluster c;
		*in_clu_stream >> c;
		clist.push_back(c);
	    }
	    out_matr_shear_stream << *ds_it;
	    int num_integ = 0;
	    std::list< std::pair< string, double > >::iterator int_it;
	    for(int_it = integration_list.begin();
		int_it != integration_list.end(); ++int_it) {
		bool found = false;
		int weight = 0;
		for(std::list< ISCluster>::iterator cit = clist.begin();
		    cit != clist.end(); cit++) {
		    std::vector< Label >::iterator lab_it;
		    for(lab_it = cit->lab.begin(); lab_it != cit->lab.end(); lab_it++) {
			if(lab_it->target_id == int_it->first &&
			   abs(lab_it->centroid - int_it->second) < dist) {
			    if(found) {
				WARN("Integration already found!");
			    } else {
				num_integ++;
			    }
			    if(cit->weight > weight) {
				weight = cit->weight;
			    }
			    found = true;
			    continue;
			}
		    }
		}
		out_matr_shear_stream << FS << weight;
	    }
	    int total_w = 0;
	    for(std::list< ISCluster>::iterator cit = clist.begin();
		cit != clist.end(); cit++) {
		total_w += cit->weight;
	    }
	    out_matr_shear_stream << FS << total_w;
	    out_matr_shear_stream << "\n";
	    //INFO(*ds_it << ": " << num_integ);
	}
	{
	    string cs = *ds_it + ".combo";
	    auto_ptr< CObjectIStream > in_clu_stream
		(CObjectIStream::Open(CLU_TYPE, cs));
	    if(!in_clu_stream->InGoodState()) {
		ERROR("Error in opening " << cs << " file.");
		return 1;
	    }
	    std::list< ISCluster  > clist;
	    while (!in_clu_stream->EndOfData()) {
		ISCluster c;
		*in_clu_stream >> c;
		clist.push_back(c);
	    }
	    out_matr_combo_stream << *ds_it;
	    int num_integ = 0;
	    std::list< std::pair< string, double > >::iterator int_it;
	    for(int_it = integration_list.begin();
		int_it != integration_list.end(); ++int_it) {
		bool found = false;
		int weight = 0;
		for(std::list< ISCluster>::iterator cit = clist.begin();
		    cit != clist.end(); cit++) {
		    std::vector< Label >::iterator lab_it;
		    for(lab_it = cit->lab.begin(); lab_it != cit->lab.end(); lab_it++) {
			if(lab_it->target_id == int_it->first &&
			   abs(lab_it->centroid - int_it->second) < dist) {
			    if(found) {
				WARN("Integration already found!");
			    } else {
				num_integ++;
			    }
			    if(cit->weight > weight) {
				weight = cit->weight;
			    }
			    found = true;
			    continue;
			}
		    }
		}
		out_matr_combo_stream << FS << weight;
	    }
	    int total_w = 0;
	    for(std::list< ISCluster>::iterator cit = clist.begin();
		cit != clist.end(); cit++) {
		total_w += cit->weight;
	    }
	    out_matr_combo_stream << FS << total_w;
	    out_matr_combo_stream << "\n";
	    //INFO(*ds_it << ": " << num_integ);
	}
    }
    out_matr_clu_stream.flush();
    out_matr_clu_stream.close();
    out_matr_tag_stream.flush();
    out_matr_tag_stream.close();
    out_matr_shear_stream.flush();
    out_matr_shear_stream.close();
    out_matr_combo_stream.flush();
    out_matr_combo_stream.close();
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    ValAssey app;
    return app.AppMain(argc, argv);
}

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <sstream>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class Clu2Bed : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
};

void Clu2Bed::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions); \
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Program to create a BED file from a CLU input file.");

    arg_desc->AddKey
        ("c", "cluster_file",
         "Input cluster file (.clu).",
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddOptionalKey
	("n", "name_conversion_file",
         "File containing the name conversion of cluster ids.",
	 CArgDescriptions::eInputFile);

     // Setup arg.descriptions for this application
     SetupArgDescriptions(arg_desc.release());
}


int Clu2Bed::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("c"));

    //Check input files
    CFile in_cluster_file = args["c"].AsString();
    if(!in_cluster_file.Exists()) {
	ERROR("File " << args["c"].AsString() << " not found.");
	return 1;
    }

    //Output cluster_id association
    string out_bed_file = in_cluster_file.GetDir()
    	+ in_cluster_file.GetBase()  + "_clu2bed.bed";
    ofstream out_bed_stream(out_bed_file.c_str());
    if(!out_bed_stream.is_open()) {
	ERROR("Error in creating " << out_bed_file << " file.");
	return 1;
    }

    INFO("PARAMETERS");
    INFO("Input CLU file: " << args["c"].AsString());
    INFO("Output BED file: " << out_bed_file);

    INFO("Started reading input files.");
    int num_elements = 0;
    std::vector< ISCluster > clu;
    std::map< string, string > clu_id_name_map;
    {
	auto_ptr< CObjectIStream > in_clu_stream
	    (CObjectIStream::Open(CLU_TYPE, args["c"].AsString()));
	if(!in_clu_stream->InGoodState()) {
	    ERROR("Error in opening " << args["c"].AsString() << " file.");
	    return 1;
	}
	while (!in_clu_stream->EndOfData()) {
	    num_elements++;
	    ISCluster c;
	    *in_clu_stream >> c;
	    clu.push_back(c);
	    size_t pos = c.clu_id.find("subg");
	    if(pos == string::npos) {
		ERROR("Error: cluster_id entry problem.");
		return 1;
	    }
	    string ds = c.clu_id.substr(0, pos - 1);
	    string sg = c.clu_id.substr(pos);
	}
    }
    INFO("Finished reading input files.");
    INFO("Num. read cluster_files: " << num_elements);

    if(args["n"].HasValue()) {
	ifstream in_nameconv_stream(args["n"].AsString().c_str());
	if(!in_nameconv_stream.is_open()) {
	    ERROR("Error in opening " << args["n"].AsString() << " file.");
	    return 1;
	}
	clu_id_name_map.clear();
	string line;
	while(std::getline(in_nameconv_stream, line)) {
	    stringstream ss(line);
	    std::vector<std::string> tokens;
	    string token;
	    while(std::getline(ss, token, '\t')) {
		tokens.push_back(token);
	    }
	    clu_id_name_map[tokens[0] + "_" + tokens[1]] = tokens[0] + "_" + tokens[2];
	}
	INFO("Num. clu_id names: " << clu_id_name_map.size());
    }
    out_bed_stream << "track name=clu2bed_clusters description=\"Integration Sites Clusters.\" useScore=1\n";
    int tot_weight = 0;
    for(std::vector< ISCluster>::iterator it = clu.begin(); it != clu.end(); it++) {
    	size_t pos = it->clu_id.find("subg");
    	if(pos == string::npos) {
    	    ERROR("Error: cluster_id entry problem.");
    	    return 1;
    	}
    	string ds = it->clu_id.substr(0, pos - 1);
    	string sg = it->clu_id.substr(pos);
	string name = "";
	if(!clu_id_name_map.empty()) {
	    if(clu_id_name_map.find(ds) == clu_id_name_map.end()) {
		ERROR("Error: dataset " << ds << " not found.");
		return 1;
	    }
	    name  += clu_id_name_map[ds] + "_" + sg;
	} else {
	    name += it->clu_id;
	}
	tot_weight += it->weight;
	for(std::vector< Label >::iterator lab_it = it->lab.begin();
	    lab_it != it->lab.end(); lab_it++) {
	    out_bed_stream << lab_it->target_id << "\t";
	    out_bed_stream << std::fixed << std::setprecision(0) << lab_it->centroid << "\t";
	    out_bed_stream << std::fixed << std::setprecision(0) << (lab_it->centroid)+1 << "\t";
	    out_bed_stream << name << "\t";
	    out_bed_stream << it->weight;
	    out_bed_stream << "\n";
	}
    }
    INFO("Total cluster weight (input reads): " << tot_weight);
    out_bed_stream.flush();
    out_bed_stream.close();
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    Clu2Bed app;
    return app.AppMain(argc, argv);
}

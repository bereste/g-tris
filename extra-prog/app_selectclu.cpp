#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <sstream>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class SelectClu : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
};

void SelectClu::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions); \
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
			      "Program to Merge Cluster Count Measures.");

    arg_desc->AddKey
	("c", "cluster_file",
	 "Input cluster file (.CLU).",
	 CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddOptionalKey
	("i", "integrations_file",
	 "File containing the integrations to be found in BED format.",
	 CArgDescriptions::eInputFile);

     // Setup arg.descriptions for this application
     SetupArgDescriptions(arg_desc.release());
}

int SelectClu::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("c"));

    //Check input files
    CFile in_int_file = args["i"].AsString();
    if(!in_int_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }

    CFile in_clu_file = args["c"].AsString();
    if(!in_clu_file.Exists()) {
	ERROR("File " << args["c"].AsString() << " not found.");
	return 1;
    }

    //Output matrices
    string out_clu_file = in_clu_file.GetDir() +
	in_clu_file.GetBase() +
	".selected" +
	in_clu_file.GetExt();

    string FS = ",";
    int dist = 3;

    INFO("PARAMETERS");
    INFO("Input integrations file: " << args["i"].AsString());
    INFO("Input cluster file: " << args["c"].AsString());
    INFO("Output cluster file: " << out_clu_file);

    INFO("Started reading input files.");
    string line;
    std::list< std::pair< string, double > > integration_list;
    std::ifstream in_int_stream(args["i"].AsString().c_str());
    if(!in_int_stream.is_open()) {
	ERROR("Error in opening " << args["i"].AsString() << " file.");
	return 1;
    }
    while(std::getline(in_int_stream, line)) {
	size_t pos = line.find("\t", 0);
	if(pos == string::npos) {
	    ERROR("Error in parsing integration entry.");
	    return 1;
	}
	string int_chr = line.substr(0, pos);
	double int_pos = atof(line.substr(pos + 1).c_str());
	//INFO(line << ": " << int_chr << " -- " << int_pos);
	integration_list.push_back(std::make_pair(int_chr, int_pos));
    }
    in_int_stream.close();
    INFO("Num. integrations: " << integration_list.size());
    std::list< ISCluster > clist;	
    {
	auto_ptr< CObjectIStream > in_clu_stream
	    (CObjectIStream::Open(CLU_TYPE, args["c"].AsString()));
	if(!in_clu_stream->InGoodState()) {
	    ERROR("Error in opening " << args["c"].AsString() << " file.");
	    return 1;
	}
	while (!in_clu_stream->EndOfData()) {
	    ISCluster c;
	    *in_clu_stream >> c;
	    clist.push_back(c);
	}
	INFO("Num. Initial Clusters: " << clist.size());
    }

    {
	auto_ptr< CObjectOStream > out_clu_stream
	    (CObjectOStream::Open(CLU_TYPE, out_clu_file));
	if(!out_clu_stream->InGoodState()) {
	    ERROR("Error in creating " << out_clu_file << " file.");
	    return 1;
	}
	int num_selected = 0;
	std::list< std::pair< string, double > >::iterator int_it;
	for(int_it = integration_list.begin();
	    int_it != integration_list.end(); ++int_it) {
	    //bool found = false;
	    for(std::list< ISCluster>::iterator cit = clist.begin(); cit != clist.end(); cit++) {
		std::vector< Label >::iterator lab_it;
		for(lab_it = cit->lab.begin(); lab_it != cit->lab.end(); lab_it++) {
		    if(lab_it->target_id == int_it->first &&
		       abs(lab_it->centroid - int_it->second) < dist) {
			*out_clu_stream << *cit;
			num_selected++;
			// found = true;
			// continue;
		    }
		}
	    }
	}
	INFO("Num. Selected Clusters: " << num_selected);	
    }
    // 	    out_matr_clu_stream << *ds_it;
    // 	    int num_integ = 0;
    // 	    std::list< std::pair< string, double > >::iterator int_it;
    // 	    for(int_it = integration_list.begin();
    // 		int_it != integration_list.end(); ++int_it) {
    // 		bool found = false;
    // 		int weight = 0;
    // 		for(std::list< ISCluster>::iterator cit = clist.begin();
    // 		    cit != clist.end(); cit++) {
    // 		    std::vector< Label >::iterator lab_it;
    // 		    for(lab_it = cit->lab.begin(); lab_it != cit->lab.end(); lab_it++) {
    // 			if(lab_it->target_id == int_it->first &&
    // 			   abs(lab_it->centroid - int_it->second) < dist) {
    // 			    if(found) {
    // 				WARN("Integration already found!");
    // 			    } else {
    // 				num_integ++;
    // 			    }
    // 			    if(cit->weight > weight) {
    // 				weight = cit->weight;
    // 			    }
    // 			    found = true;
    // 			    continue;
    // 			}
    // 		    }
    // 		}
    // 		out_matr_clu_stream << FS << weight;
    // 	    }
    // 	    int total_w = 0;
    // 	    for(std::list< ISCluster>::iterator cit = clist.begin();
    // 		cit != clist.end(); cit++) {
    // 		total_w += cit->weight;
    // 	    }
    // 	    out_matr_clu_stream << FS << total_w;
    // 	    out_matr_clu_stream << "\n";
    // 	    //INFO(*ds_it << ": " << num_integ);
    // 	}
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    SelectClu app;
    return app.AppMain(argc, argv);
}

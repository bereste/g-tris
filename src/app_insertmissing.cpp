/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"

#include <ncbi_pch.hpp>
#include <corelib/ncbiapp.hpp>
#include <serial/serial.hpp>
#include <serial/objostr.hpp>

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

KSEQ_INIT(gzFile, gzread)

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class InsertMissing : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    // virtual void Exit();
};

void InsertMissing::Init(void){
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Insert missing alignments in the graph");

    arg_desc->AddKey
        ("i", "inputfile",
         "Input FASTA file of the all sequences.",
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey
        ("mis", "missing",
         "File containing the IDs of the missing sequences.",
          CArgDescriptions::eInputFile);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}


int InsertMissing::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));

    //Check input files
    CFile in_fasta_file(args["i"].AsString());
    if(!in_fasta_file.Exists()) {
        ERROR("File " << args["i"].AsString() << " not found.");
        return 1;
    }

    CFile in_missing_file(args["mis"].AsString());
    if(!in_missing_file.Exists()) {
        ERROR("File " << args["mis"].AsString() << " not found.");
        return 1;
    }

    //Check output file
    string inoutasn = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.asn";
    auto_ptr< CObjectIStream > in_asn_stream
        (CObjectIStream::Open(ASN_TYPE, inoutasn));
    if(!in_asn_stream->InGoodState()) {
	ERROR("Error in opening " << inoutasn << " file.");
	return 1;
    }
#ifdef PRINT_CSV_ALN	
    string outcsv = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.csv";
    CNcbiOfstream out_csv_stream(outcsv.c_str(), std::ofstream::out | std::ofstream::app);
    if(!out_csv_stream.is_open()) {
        ERROR("Problem in opening " << outcsv << " file.");
        return 1;
    }
#endif
    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input FASTA sequence file: " << args["i"].AsString());
    INFO("Input missing file: " << args["mis"].AsString());
    INFO("Output ASN file: " << inoutasn);
#ifdef PRINT_CSV_ALN
    INFO("Output CSV file: " << outcsv);
#endif
    //Read existsing alignments
    std::map< string, Alignments > seq_aln;
    while(!in_asn_stream->EndOfData()) {
        Alignments al;
        *in_asn_stream >> al;
	seq_aln[al.query_id] = al;
    }
    in_asn_stream->Close();

    auto_ptr< CObjectOStream >
	out_asn_stream(CObjectOStream::Open(ASN_TYPE, inoutasn));
    if(!out_asn_stream->InGoodState()) {
        ERROR("Problem in opening " << inoutasn << " file.");
        return 1;
    }
    for(std::map< string, Alignments >::iterator it = seq_aln.begin();
	it != seq_aln.end(); ++it) {
	*out_asn_stream	<< it->second;	
    }

    ifstream in_missing_stream(args["mis"].AsString().c_str());
    std::set< string > missing_set;
    string line;
    int tot_mis = 0;
    while(std::getline(in_missing_stream, line)) {
	missing_set.insert(line);
	++tot_mis;
    }
    in_missing_stream.close();

    //Read Fasta sequences
    {
	gzFile fp = gzopen(args["i"].AsString().c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	while ((kseq_read(seq)) >= 0) {
	    string s = seq->seq.s;
	    int len = s.length();
	    string id = seq->name.s;
	    if (missing_set.find(id) != missing_set.end()) {
		Alignments al;
		al.query_id = id;
		al.query_len = len;
		al.max_score = len;
		AlnData data(id, 0, len - 1, 0, len - 1,
			     1, 1, len, len);
		al.al_data.push_back(data);
		*out_asn_stream << al;
#ifdef PRINT_CSV_ALN
		printAln(al, out_csv_stream);
#endif
	    }
	}
	INFO("Missing Sequences: " << tot_mis);
	kseq_destroy(seq);
	gzclose(fp);
    }
#ifdef PRINT_CSV_ALN
    out_csv_stream.flush();
    out_csv_stream.close();
#endif
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    InsertMissing app;
    return app.AppMain(argc, argv);
}

/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"
#include "uniqueseqs.hpp"
#include "consensus.hpp"

#include <queue>
#include <iterator>

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <omp.h>

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

#include "bwa/bwamem.h"

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class ComputeTAG : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    virtual void Exit();
    int createTagShearFile(std::vector< ISCluster >& clu,
			   const string& subg_dir_name,
			   const string& outtag,
			   const string& outR2,
			   const string& outCombo,
			   const UniqueSeqs& unique_seq_map,
			   const std::map< string, string >& tag_seqs,
			   const std::map< string, string >& r2_seqs,
			   const int num_threads);
    int readVsClusters(const string& in_tag_clu_name,
		       std::map< string, string >& tag_clusters);
    // int processSingleClu(std::vector< ISCluster >& clu,
    // 			 const string& subg_dir_name,
    // 			 const string& in_r2_name,
    // 			 const UniqueSeqs& unique_seq_map,
    // 			 const string& db_name,
    // 			 const int num_threads,
    // 			 const string& outsing);
    // int alignR2reads(const string& in_r2_name,
    // 		     const string& db_name,
    // 		     const int num_threads,
    // 		     std::map< string, Alignments >& r2_seq_map);
};


int ComputeTAG::readVsClusters(const string& in_tag_clu_name,
			       std::map< string, string >& tag_clusters) {
    CFile in_tag_clu_file(in_tag_clu_name.c_str());
    if(!in_tag_clu_file.Exists()) {
	ERROR("File " << in_tag_clu_name.c_str() << " not found.");
	return -1;
    }
    ifstream in_tag_clu_stream(in_tag_clu_name.c_str());
    if(!in_tag_clu_stream.is_open()) {
	ERROR("Error in opening " << in_tag_clu_name << " file.");
	return -1;
    }
    string line;
    int num_tag_seqs = 0;
    std::set< string > num_tag_clu;
    while(std::getline(in_tag_clu_stream, line)) {
	stringstream ss(line);
	string field;
	getline(ss, field, '\t');
	if(field != "C") {
	    num_tag_seqs++;
	    getline(ss, field, '\t');
	    string tag_clu_id = field;
	    num_tag_clu.insert(tag_clu_id);
	    for(int i = 0; i < 6; i++) {
		getline(ss, field, '\t');
	    }
	    getline(ss, field, '\t');
	    tag_clusters[field] = tag_clu_id;
	}
    }
    in_tag_clu_stream.close();
    return num_tag_clu.size();
}

int ComputeTAG::createTagShearFile(std::vector< ISCluster >& clu,
				   const string& subg_dir_name,
				   const string& outtag,
				   const string& outR2,
				   const string& outCombo,
				   const UniqueSeqs& unique_seq_map,
				   const std::map< string, string >& tag_seqs,
				   const std::map< string, string >& r2_seqs,
				   const int num_threads) {
    std::string bin_path = "";
    char* _bin_path = readlink_malloc("/proc/self/exe");
    if (_bin_path == NULL) {
	ERROR("Impossible to determine the path of the executable. " <<
	      "Let it unspecified (i.e., search in the PATH).");
    } else {
	bin_path = _bin_path;
	free(_bin_path);
	CDirEntry cd(bin_path);
	bin_path = cd.GetDir();
	INFO("The directory of the executable is '" << bin_path << "'.");
    }
    string vs_tag_in_file = "vsearch_tag_in.fa";
    string vs_tag_out_file = "vsearch_tag_out.table";
    string vs_r2_in_file = "vsearch_r2_in.fa";
    string vs_r2_out_file = "vsearch_r2_out.table";
    string vs_opt = " -id 0.9 -maxaccepts 0 -maxrejects 0 --minseqlength 1 -threads ";
    stringstream vs_tag_cmd;
    vs_tag_cmd << bin_path
	       << "_vsearch -cluster_fast "
	       << vs_tag_in_file
	       << vs_opt
	       << num_threads
	       << " -uc "
	       << vs_tag_out_file;
    stringstream vs_r2_cmd;
    vs_r2_cmd << bin_path
	       << "_vsearch -cluster_fast "
	       << vs_r2_in_file
	       << vs_opt
	       << num_threads
	       << " -uc "
	       << vs_r2_out_file;
    // TAG (serialized) output
    auto_ptr<CObjectOStream>
	out_tag_stream(CObjectOStream::Open(CLU_TYPE, outtag));
    if(!out_tag_stream->InGoodState()) {
	ERROR("Error in creating " << outtag << " file.");
	return 1;
    }
    // R2 (serialized) output
    auto_ptr<CObjectOStream>
	out_r2_stream(CObjectOStream::Open(CLU_TYPE, outR2));
    if(!out_r2_stream->InGoodState()) {
	ERROR("Error in creating " << outR2 << " file.");
	return 1;
    }
    // Combo (serialized) output
    auto_ptr<CObjectOStream>
	out_combo_stream(CObjectOStream::Open(CLU_TYPE, outCombo));
    if(!out_combo_stream->InGoodState()) {
	ERROR("Error in creating " << outCombo << " file.");
	return 1;
    }

    for(unsigned int i = 0; i < clu.size(); ++i) {
	int w  = clu[i].weight;
	int check_w = 0;
	std::list< string >::iterator merge_it;
	// TAG
	std::ofstream vs_tag_in_stream(vs_tag_in_file.c_str(),
				       std::ios_base::binary);
	if(!vs_tag_in_stream.is_open()) {
	    ERROR("Error in creating " << vs_tag_in_file << " file.");
	    continue;
	}
	// R2
	std::ofstream vs_r2_in_stream(vs_r2_in_file.c_str(),
				       std::ios_base::binary);
	if(!vs_r2_in_stream.is_open()) {
	    ERROR("Error in creating " << vs_r2_in_file << " file.");
	    continue;
	}
	for(merge_it = clu[i].merged.begin();
	    merge_it != clu[i].merged.end(); merge_it++) {
	    string fs = subg_dir_name + "/" + *merge_it + ".fa";
	    gzFile fp = gzopen(fs.c_str(), "r");
	    kseq_t* seq = kseq_init(fp);
	    while ((kseq_read(seq)) >= 0) {
		string un_seq = seq->name.s;
		check_w += unique_seq_map.fasta_id_map.at(un_seq).size() + 1;
		if(tag_seqs.find(un_seq) == tag_seqs.end()) {
		    WARN("Tag read id not found for " << un_seq);
		} else {
		    vs_tag_in_stream << ">" << un_seq << "\n"
				     << tag_seqs.at(un_seq) << "\n";
		}
		if(r2_seqs.find(un_seq) == r2_seqs.end()) {
		    WARN("R2 read id not found for " << un_seq);
		} else {
		    vs_r2_in_stream << ">" << un_seq << "\n"
				    << r2_seqs.at(un_seq) << "\n";
		}
		std::list< string >::const_iterator us_it;
		for(us_it = unique_seq_map.fasta_id_map.at(un_seq).begin();
		    us_it != unique_seq_map.fasta_id_map.at(un_seq).end();
		    us_it++) {
		    if(tag_seqs.find(*us_it) == tag_seqs.end()) {
			WARN("Tag read id not found for " << *us_it);
		    } else {
			vs_tag_in_stream << ">" << *us_it << "\n"
					 << tag_seqs.at(*us_it) << "\n";
		    }
		    if(r2_seqs.find(*us_it) == r2_seqs.end()) {
			WARN("R2 read id not found for " << *us_it);
		    } else {
			vs_r2_in_stream << ">" << *us_it << "\n"
					<< r2_seqs.at(*us_it) << "\n";
		    }
		}
	    }
	    kseq_destroy(seq);
	    gzclose(fp);
	}
	vs_tag_in_stream.flush();
	vs_tag_in_stream.close();
	vs_r2_in_stream.flush();
	vs_r2_in_stream.close();
	if(w != check_w) {
	    ERROR("Wrong original count for cluster " << clu[i].clu_id);
	    return 1;
	}
	// Compute TAG clustering with vsearch
	string vs_tag_call(vs_tag_cmd.str());
	INFO(vs_tag_call);
	if (system(vs_tag_call.c_str())) {
	    ERROR("vsearch invocation error on TAGs.");
	    return 1;
	}
	// Read TAG clustring file
	INFO("Clsuter: " << clu[i].clu_id);
	INFO("Number of sequences: " << w);
	std::map< string, string > tag_clusters;
	int num_tag_clu = readVsClusters(vs_tag_out_file, tag_clusters);
	if(num_tag_clu == -1) {
	    ERROR("Error in reading vsearch results for " << clu[i].clu_id);
	    return 1;
	}
	INFO("Number of TAG clusters: " << num_tag_clu);

	clu[i].weight = num_tag_clu;
	*out_tag_stream << clu[i];
	
	// Compute R2 clustering with vsearch
	string vs_r2_call(vs_r2_cmd.str());
	INFO(vs_r2_call);
	if (system(vs_r2_call.c_str())) {
	    ERROR("vsearch invocation error on R2 seqs.");
	    return 1;
	}
	// Read R2 clustring file
	std::map< string, string > r2_clusters;
	int num_r2_clu = readVsClusters(vs_r2_out_file, r2_clusters);
	if(num_r2_clu == -1) {
	    ERROR("Error in reading vsearch results for " << clu[i].clu_id);
	    return 1;
	}
	INFO("Number of shear clusters: " << num_r2_clu);
	clu[i].weight = num_r2_clu;
	*out_r2_stream << clu[i];

	// Combo
	std::map< string, std::set< string > > combo;
	for(std::map< string, string >:: iterator r2_it = r2_clusters.begin();
	    r2_it != r2_clusters.end(); ++r2_it) {
	    combo[r2_it->second].insert(tag_clusters[r2_it->first]);
	}
	int num_combo = 0;
	for(std::map< string, std::set< string > >::iterator c_it = combo.begin();
	    c_it != combo.end(); ++c_it) {
	    num_combo += c_it->second.size();
	}
	INFO("Number of combo (tag + shear): " << num_combo);
	clu[i].weight = num_combo;
	*out_combo_stream << clu[i];
    }
    return 0;
}

// int ComputeTAG::alignR2reads(const string& in_r2_name,
// 			     const string& db_name,
// 			     const int num_threads,
// 			     std::map< string, Alignments >& r2_seq_map) {
//     INFO("BWA alignment process started.");
//     bwaidx_t *idx;
//     // load the BWA index
//     idx = bwa_idx_load(db_name.c_str(), BWA_IDX_ALL);
//     if (idx == NULL) {
// 	ERROR("BWA index load failed.");
// 	return 1;
//     }
//     mem_opt_t *opt;
//     // initialize the BWA-MEM parameters to the default values
//     opt = mem_opt_init();
//     opt->w = BWA_W;
//     opt->max_occ = BWA_MAX_OCC;
//     opt->min_seed_len = BWA_MIN_SEED_LEN;
//     opt->n_threads = num_threads;
//     INFO("BWA band width: " << BWA_W);
//     INFO("BWA max number of occurrences: " << BWA_MAX_OCC);
//     INFO("BWA min seed len: " << BWA_MIN_SEED_LEN);
//     INFO("BWA min alignment score: " << BWA_ALN_THR);
//     gzFile fp = gzopen(in_r2_name.c_str(), "r");
//     kseq_t* seq = kseq_init(fp);
//     int num_input_seqs = 0;
//     int num_aln_seqs = 0;
//     int num_missing_seqs = 0;
//     int tot_alignments = 0;
//     while ((kseq_read(seq)) >= 0) {
// 	++num_input_seqs;
// 	string id = seq->name.s;
// 	string s = seq->seq.s;
// 	Alignments al;
// 	al.query_id = id;
// 	al.query_len = s.length();
// 	bool aligned = false;
// 	// get all the hits
// 	mem_alnreg_v ar = mem_align1(opt,
// 				     idx->bwt,
// 				     idx->bns,
// 				     idx->pac,
// 				     seq->seq.l,
// 				     seq->seq.s);
// 	// traverse each hit
// 	int max_bwa_score = 0;
// 	for (unsigned int i = 0; i < ar.n; ++i) {
// 	    // get forward-strand position and CIGAR
// 	    mem_aln_t a = mem_reg2aln(opt,
// 				      idx->bns,
// 				      idx->pac,
// 				      seq->seq.l,
// 				      seq->seq.s,
// 				      &ar.a[i]);
// 	    //Filter for valid alignments
// 	    if (a.rid < 0) continue; // || ar.a[i].secondary >= 0) continue;
// 	    if (ar.a[i].score < BWA_ALN_THR) continue;
// 	    int ref_aln_len = ar.a[i].re - ar.a[i].rb;
// 	    int query_aln_len = ar.a[i].qe - ar.a[i].qb;
// 	    //if (ar.a[i].qb > PCR_THR) continue;
// 	    //if (query_aln_len < ALN_PERC_ID * al.query_len) continue;
// 	    //Valid alignment
// 	    aligned = true;
// 	    ++tot_alignments;
// 	    AlnData data(idx->bns->anns[a.rid].name,
// 			 ar.a[i].qb,
// 			 ar.a[i].qb + query_aln_len - 1,
// 			 a.pos, //ar.a[i].rb,
// 			 a.pos + ref_aln_len - 1, //ar.a[i].re,
// 			 +1,
// 			 (a.is_rev ? -1 : +1),
// 			 query_aln_len,
// 			 ar.a[i].score);
// 	    al.al_data.push_back(data);
// 	    // Deallocate CIGAR
// 	    free(a.cigar);
// 	    if (ar.a[i].score > max_bwa_score) {
// 		max_bwa_score = ar.a[i].score;
// 	    }
// 	}
// 	al.max_score = max_bwa_score;
// 	if (aligned) {
// 	    ++num_aln_seqs;
// 	} else {
// 	    ++num_missing_seqs;
// 	}
// 	r2_seq_map[seq->name.s] = al;
// 	free(ar.a);
//     }
//     free(opt);
//     kseq_destroy(seq);
//     gzclose(fp);
//     bwa_idx_destroy(idx);
//     //Print Results
//     INFO("BWA RESULTS");
//     INFO("Num. Input Sequences: " << num_input_seqs);
//     INFO("Num. Aligned Sequences: " << num_aln_seqs);
//     INFO("Num. Total Alignments: " << tot_alignments);
//     INFO("Num. Not Aligned Sequences: " << num_missing_seqs);
//     INFO("BWA alignment process finished.");
//     return 0;
// }

// void printAln(string info,
// 	      string seq,
// 	      std::map< string, Alignments >& r2_seq_map,
// 	      ofstream& out) {
//     if(r2_seq_map[seq].al_data.size() == 0) {
// 	out << info << seq << ","
// 	     << "NO_R2_ALN" << ","
// 	     << "NO_R2_ALN" << ","
// 	     << "NO_R2_ALN"
// 	     << "\n";
//     } else {
// 	std::list< AlnData >::iterator aln_it;
// 	for(aln_it = r2_seq_map[seq].al_data.begin();
// 	    aln_it != r2_seq_map[seq].al_data.end();
// 	    aln_it++) {
// 	    double centroid;
// 	    int offset;
// 	    if (aln_it->target_strand == aln_it->query_strand) {
// 		centroid = aln_it->target_start;
// 		offset = aln_it->alignment_len;
// 	    } else {
// 		centroid = aln_it->target_stop;
// 		offset = -(aln_it->alignment_len);
// 	    }
// 	    out << info << seq << ","
// 		<< aln_it->target_id << ","
// 		<< std::fixed << std::setprecision(0)
// 		<< centroid << ","
// 		<< offset
// 		<< "\n";
// 	}
//     }
// }

// int ComputeTAG::processSingleClu(std::vector< ISCluster >& clu,
// 				 const string& subg_dir_name,
// 				 const string& in_r2_name,
// 				 const UniqueSeqs& unique_seq_map,
// 				 const string& db_name,
// 				 const int num_threads,
// 				 const string& outsing) {
//     //Output CSV R" single clusters
//     CNcbiOfstream out_csv_stream(outsing.c_str());
//     if(!out_csv_stream.is_open()) {
// 	ERROR("Error in creating output CSV file " << outsing);
// 	return 1;
//     }
//     // Align R2 reads
//     std::map< string, Alignments > r2_seq_map;
//     if(alignR2reads(in_r2_name,
// 		    db_name,
// 		    num_threads,
// 		    r2_seq_map) == 1) {
// 	return 1;
//     }
//     // Process sequences
//     int num_single = 0;
//     for(unsigned int i = 0; i < clu.size(); ++i) {
// 	if(clu[i].lab.size() == 1) {
// 	    num_single++;
// 	    std::list< string > clu_seq;
// 	    std::list< string >::iterator merge_it;
// 	    for(merge_it = clu[i].merged.begin();
// 		merge_it != clu[i].merged.end(); merge_it++) {
// 		string fs = subg_dir_name + "/" + *merge_it + ".fa";
// 		gzFile fp = gzopen(fs.c_str(), "r");
// 		kseq_t* seq = kseq_init(fp);
// 		stringstream out_info;
// 		out_info << clu[i].clu_id << "," <<
// 		    clu[i].lab[0].target_id << "," <<
// 		    std::fixed << std::setprecision(0) <<
// 		    clu[i].lab[0].centroid << "," <<
// 		    clu[i].lab[0].offset << ",";
// 		while ((kseq_read(seq)) >= 0) {
// 		    string un_seq = seq->name.s;
// 		    if(r2_seq_map.find(un_seq) == r2_seq_map.end()) {
// 			ERROR("R2 read not found for " << un_seq);
// 			return 1;
// 		    }
// 		    // Check aln
// 		    printAln(out_info.str(),
// 			     un_seq,
// 			     r2_seq_map,
// 			     out_csv_stream);
// 		    std::list< string >::const_iterator us_it;
// 		    for(us_it = unique_seq_map.fasta_id_map.at(un_seq).begin();
// 			us_it != unique_seq_map.fasta_id_map.at(un_seq).end();
// 			us_it++) {
// 			if(r2_seq_map.find(*us_it) == r2_seq_map.end()) {
// 			    ERROR("R2 read not found for " << *us_it);
// 			    return 1;
// 			}
// 			// Check aln
// 			printAln(out_info.str(),
// 				 *us_it,
// 				 r2_seq_map,
// 				 out_csv_stream);
// 		    }
// 		}
// 		kseq_destroy(seq);
// 		gzclose(fp);
// 	    }
// 	}
//     }
//     out_csv_stream.flush();
//     out_csv_stream.close();
//     INFO("Number of single label clusters: " << num_single);
//     return 0;
// }

void ComputeTAG::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
			      "Compute TAG program");
    arg_desc->AddKey
	("i", "inputClusterFile",
	 "Input file of serialized clsuters (.clu). ",
	 CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);
    // arg_desc->AddKey
    // ("db", "database",
    // "BWA database name.",
    // CArgDescriptions::eInputFile);
    arg_desc->AddKey
	("t", "inputTagFile",
	 "Input file containing the TAG sequences. ",
	 CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);
    arg_desc->AddKey
	("r", "r2FastaFile",
	 "Input R2 file of paired reads in FASTA format.",
	 CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);
    arg_desc->AddKey
	("m", "mapFile",
	 "File containing the map of unique sequences " \
	 "obtained with option '-uniqueseqs' of "       \
	 "makeblastdb program.",
	 CArgDescriptions::eInputFile);
    arg_desc->AddDefaultKey
	("num_threads", "num_threads",
	 "Number of threads used by BWA. Default: 1.",
	 CArgDescriptions::eInteger, BLAST_THREADS);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

int ComputeTAG::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));
    //Check input files
    CFile in_clu_file(args["i"].AsString());
    if(!in_clu_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }
    string subg_dir_name(in_clu_file.GetDir() + "subgraphs");
    CDir subg_dir(subg_dir_name);
    if(!subg_dir.Exists()) {
	ERROR("Cannot find subgraph directory: " << subg_dir_name << ".");
	return 1;
    }
    CFile in_tag_file(args["t"].AsString());
    if(!in_tag_file.Exists()) {
	ERROR("File " << args["t"].AsString() << " not found.");
	return 1;
    }
    CFile in_r2_file(args["r"].AsString());
    if(!in_r2_file.Exists()) {
	ERROR("File " << args["r"].AsString() << " not found.");
	return 1;
    }

    // Output TAG file name
    string outtag = in_clu_file.GetDir() + in_clu_file.GetBase() + ".tag";

    // Output R2 file name
    string outR2 = in_clu_file.GetDir() + in_clu_file.GetBase() + ".shear";

    // Output combo file name
    string outCombo = in_clu_file.GetDir() + in_clu_file.GetBase() + ".combo";

    int num_threads = 1;
#ifdef ENABLE_OPENMP
    if (args["num_threads"].AsInteger() > 1) {
	if (args["num_threads"].AsInteger() < omp_get_num_procs()) {
	    omp_set_num_threads(args["num_threads"].AsInteger());
	    num_threads = args["num_threads"].AsInteger();
	} else {
	    omp_set_num_threads(omp_get_num_procs());
	    num_threads = omp_get_num_procs();
	}
    }
#endif

    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input CLU file: " << args["i"].AsString());
    // INFO("Input DB file: " << args["db"].AsString());
    INFO("Input TAG file: " << args["t"].AsString());
    INFO("Input R2 file: " << args["r"].AsString());
    if(args["m"].HasValue()) {
	INFO("Input Map file: " << args["m"].AsString());
    }
    INFO("Input subgraphs dir: " << subg_dir_name);
    INFO("Num. Threads: " << num_threads);
    INFO("Output TAG file: " << outtag);
    INFO("Output Shear file: " << outR2);
    INFO("Started reading input files.");
    //Read clusters
    std::vector< ISCluster > clu;
    {
	auto_ptr< CObjectIStream > in_clu_stream
	    (CObjectIStream::Open(CLU_TYPE, args["i"].AsString()));
	if(!in_clu_stream->InGoodState()) {
	    ERROR("Error in opening " << args["i"].AsString() << " file.");
	    return 1;
	}
	while (!in_clu_stream->EndOfData()) {
	    ISCluster c;
	    *in_clu_stream >> c;
	    clu.push_back(c);
	}
    }
    INFO("Num. Read Clusters: " << clu.size());

    //Read serialized FASTA Id Map
    UniqueSeqs unique_seq_map;
    if (args["m"].HasValue()) {
	auto_ptr< CObjectIStream > in_map_stream
	    (CObjectIStream::Open(ASN_TYPE, args["m"].AsString()));
	if(!in_map_stream->InGoodState()) {
	    ERROR("Error in opening " << args["m"].AsString() << " file.");
	    return 1;
	}
	*in_map_stream >> unique_seq_map;
    } else {
	for(unsigned int i = 0; i < clu.size(); ++i) {
	    std::list< string >::iterator merge_it;
	    for(merge_it = clu[i].merged.begin();
		merge_it != clu[i].merged.end(); merge_it++) {
		string fs = subg_dir_name + "/" + *merge_it + ".fa";
		gzFile fp = gzopen(fs.c_str(), "r");
		kseq_t* seq = kseq_init(fp);
		while ((kseq_read(seq)) >= 0) {
		    unique_seq_map.fasta_id_map[seq->name.s];
		}
		kseq_destroy(seq);
		gzclose(fp);
	    }
	}
    }

    //Read TAG clusters
    std::map< string, string > tag_seqs;
    {
	gzFile fp = gzopen(args["t"].AsString().c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	while ((kseq_read(seq)) >= 0) {
	    string s = seq->seq.s;
	    string id = seq->name.s;
	    tag_seqs[id] = s;
	}
	kseq_destroy(seq);
	gzclose(fp);
    }

    //Read R2 clusters
    std::map< string, string > r2_seqs;
    double mean = 0;
    unsigned int min = 200;
    unsigned int max = 0;
    int c = 0;
    {
	gzFile fp = gzopen(args["r"].AsString().c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	while ((kseq_read(seq)) >= 0) {
	    string s = seq->seq.s;
	    string id = seq->name.s;
	    if(s.length() > 20) {
		r2_seqs[id] = s.substr(20, 30);
	    } else {
		r2_seqs[id] = s;
	    }
	    c++;
	    if(s.length() < min) {
		min = s.length();
	    }
	    if(s.length() > max) {
		max = s.length();
	    }
	    mean += s.length();
	}
	kseq_destroy(seq);
	gzclose(fp);
    }
    INFO("Finished reading input files.");
    INFO("Min: " << min);
    INFO("Max: " << max);
    INFO("Mean: " << mean/c);
    //TAG and Shear site count
    INFO("Started creating TAG and Shear site files.");
    if(createTagShearFile(clu, subg_dir_name,
			  outtag,
			  outR2,
			  outCombo,
			  unique_seq_map,
			  tag_seqs,
			  r2_seqs,
			  num_threads) != 0) {
	ERROR("Error in creating TAG and Shear site files.");
	return 1;
    }
    INFO("Finished creating TAG and Shear site files.");

    // INFO("Started processing single aln clusters.");
    // if(processSingleClu(clu,
    // 			subg_dir_name,
    // 			args["r"].AsString(),
    // 			unique_seq_map,
    // 			args["db"].AsString(),
    // 			num_threads,
    // 			outsing) != 0) {
    // 	ERROR("Error in processing single aln clusters.");
    // 	return 1;
    // }
    // INFO("Finished processing single aln clusters.");
    return 0;
}

void ComputeTAG::Exit(){

}

int main(int argc, char* argv[]){
    INFO("Program started");
    ComputeTAG app;
    return app.AppMain(argc, argv);
}

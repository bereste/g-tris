/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "blast_executer.hpp"

#include <objtools/readers/rm_reader.hpp>

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

#include "bwa/bwamem.h"

KSEQ_INIT(gzFile, gzread)

USING_NCBI_SCOPE;
USING_SCOPE(objects);


CConstRef< CUser_object > FindExt(const CSeq_feat& feat,
                                  const string& ext_type) {
    CConstRef< CUser_object > ret;
    if (feat.IsSetExts()) {
        ITERATE(CSeq_feat::TExts, it, feat.GetExts()) {
            const CObject_id& obj_type = (*it)->GetType();
            if (obj_type.IsStr() && obj_type.GetStr() == ext_type) {
                ret.Reset(it->GetPointer());
                break;
            }
        }
    }
    return ret;
}

CRef< CUser_object > FindExt(CSeq_feat& feat,
                             const string& ext_type) {
    CRef< CUser_object > ret;
    if (feat.IsSetExts()) {
        NON_CONST_ITERATE(CSeq_feat::TExts, it, feat.SetExts()) {
            const CObject_id& obj_type = (*it)->GetType();
            if (obj_type.IsStr() && obj_type.GetStr() == ext_type) {
                ret.Reset(it->GetPointer());
                break;
            }
        }
    }
    return ret;
}

class AlignmentMissing : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    // virtual void Exit();
};

void AlignmentMissing::Init(void){
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Iterative Sequence Alignment program");

    arg_desc->AddKey
        ("i", "inputfile",
         "Input FASTA file of all the sequences.",
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddOptionalKey
        ("db", "database",
         "BLAST database name. Default: input FASTA file.",
          CArgDescriptions::eInputFile);

    arg_desc->AddOptionalKey
        ("mask", "mask_aln",
         "RepeatMasker out file. Default: none.",
          CArgDescriptions::eInputFile);

    arg_desc->AddDefaultKey
        ("num_threads", "num_threads",
         "Number of threads used by BLAST. Default: 1.",
         CArgDescriptions::eInteger, BLAST_THREADS);

    CArgAllow* threads_cond =
        new CArgAllow_Integers(1,32);

    arg_desc->SetConstraint("num_threads", threads_cond);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

struct rpt {
    bool tot_masked;
    vector< bool > q_mask;
    vector< string > rpt_id;
    vector< pair< int, int > > rpt_loc;
};

int AlignmentMissing::Run(void) {
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));
    assert(args.Exist("db"));

    //Check input files
    CFile in_fasta_file(args["i"].AsString());
    if(!in_fasta_file.Exists()) {
        ERROR("File " << args["i"].AsString() << " not found.");
        return 1;
    }
    CFile in_db_file(args["db"].AsString());
    if(!in_db_file.Exists()) {
        ERROR("File " << args["db"].AsString() << " not found.");
        return 1;
    }
    if (args["mask"].HasValue()) {
        CFile in_mask_file(args["mask"].AsString());
        if(!in_mask_file.Exists()) {
            ERROR("File " << args["mask"].AsString() << " not found.");
            return 1;
        }
    }

    //Check output files
    string outasn = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.asn";
    auto_ptr< CObjectOStream >
        out_asn_stream(CObjectOStream::Open(ASN_TYPE, outasn));
    if(!out_asn_stream->InGoodState()) {
        ERROR("Problem in creating " << outasn << " file.");
        return 1;
    }
#ifdef PRINT_CSV_ALN
    string outcsv = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.csv";
    CNcbiOfstream out_csv_stream(outcsv.c_str());
    if(!out_csv_stream.is_open()) {
        ERROR("Error in creating " << outcsv << " file.");
        return 1;
    }
#endif
    string outmissing = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.missing";
    ofstream out_missing_stream(outmissing.c_str(), ios::out);
	if(!out_missing_stream.is_open()) {
		ERROR("Error in opening " << outmissing << " file.");
		return 1;
	}
	
    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input FASTA file: " << args["i"].AsString());
    INFO("Input DB file: " << args["db"].AsString());
    if(args["mask"].HasValue()) {
        INFO("Input RepeatMasker file: " << args["mask"].AsString());
    }
    INFO("Output ASN file: " << outasn);
#ifdef PRINT_CSV_ALN
    INFO("Output CSV file: " << outcsv);
#endif
    INFO("Output Missing file: " << outmissing);
    INFO("Num. Threads: " << args["num_threads"].AsString());

    // Check RepeatMasker file
    std::map< string, rpt > masked_seq;
    if (args["mask"].HasValue()) {
        CNcbiIfstream in_mask_stream(args["mask"].AsString().c_str());
	    if(!in_mask_stream.is_open()) {
	        ERROR("Error in opening " << args["mask"].AsString() << " file.");
	        return 1;
	    }
        //Skip RepeatMasker out file header (2 lines)
        string l = "";
        NcbiGetlineEOL(in_mask_stream, l);
        NcbiGetlineEOL(in_mask_stream, l);
        CRepeatMaskerReader reader(CRepeatMaskerReader::fStandardizeNomenclature |
                                   CRepeatMaskerReader::fIncludeAll);

        CRef< CSeq_annot > annot(reader.ReadSeqAnnot(in_mask_stream));
        CSeq_annot::C_Data::TFtable ftable = annot->GetData().GetFtable();
        for (CSeq_annot::TData::TFtable::iterator feat_it = ftable.begin();
             ftable.end() != feat_it; ++feat_it) {
            CRef< CSeq_feat > feature = (*feat_it);
            if (!feature->CanGetLocation() ){
                ERROR("RepeatMasker out: can't get location.");
                continue;
            }
            string q_id = feature->GetLocation().GetId()->GetSeqIdString();
            int q_start = feature->GetLocation().GetTotalRange().GetFrom();
            int q_stop = feature->GetLocation().GetTotalRange().GetTo();
            int q_len = FindExt(*feature, "RepeatMasker")->GetField("query_length").GetData().GetInt();
            string rpt_id = feature->GetNamedQual("rpt_family") + "_";
            rpt_id += feature->GetNamedQual("satellite");
            rpt_id += feature->GetNamedQual("rpt_unit");
            rpt_id += feature->GetNamedQual("rpt_unit_seq");
            rpt_id += feature->GetNamedQual("rpt_type");
            rpt_id += feature->GetNamedQual("standard_name");
            rpt_id += feature->GetNamedQual("mobile_element");
            if (rpt_id == "_") {
                WARN("RepeatMasker out: can't get repeat info.");
            }
            if (masked_seq.find(q_id) == masked_seq.end()) {
                rpt r;
                r.tot_masked = false;
                r.q_mask.clear();
                r.q_mask.resize(q_len, false);
                masked_seq[q_id] = r;
            }
            fill(masked_seq[q_id].q_mask.begin() + q_start,
                 masked_seq[q_id].q_mask.begin() + q_stop,
                 true);
            if (std::count(masked_seq[q_id].q_mask.begin(),
                           masked_seq[q_id].q_mask.end(), false) <
                FILTER_LEN_THR) {
                masked_seq[q_id].tot_masked = true;
            }
            masked_seq[q_id].rpt_id.push_back(rpt_id);
            masked_seq[q_id].rpt_loc.push_back(make_pair(q_start, q_stop));
        }
        std::map< string, rpt >::const_iterator mask_it;
        int t = 0;
        for (mask_it = masked_seq.begin();
             mask_it != masked_seq.end(); ++mask_it) {
            if (mask_it->second.tot_masked) {
                ++t;
            }
        }
        INFO("Masked Seqs.: " << masked_seq.size());
        INFO("Tot. Masked Seqs.: " << t);
    }

    {{
            INFO("BWA alignment process started.");
            bwaidx_t *idx;
            // load the BWA index
            idx = bwa_idx_load(args["db"].AsString().c_str(), BWA_IDX_ALL);
            if (idx == NULL) {
                ERROR("BWA index load failed.");
                return 1;
            }
            mem_opt_t *opt;
            // initialize the BWA-MEM parameters to the default values
            opt = mem_opt_init();
            opt->w = BWA_W;
            opt->max_occ = BWA_MAX_OCC;
            opt->min_seed_len = BWA_MIN_SEED_LEN;
            opt->n_threads = args["num_threads"].AsInteger();
            INFO("BWA band width: " << BWA_W);
            INFO("BWA max number of occurrences: " << BWA_MAX_OCC);
            INFO("BWA min seed len: " << BWA_MIN_SEED_LEN);
            gzFile fp = gzopen(args["i"].AsString().c_str(), "r");
            kseq_t* seq = kseq_init(fp);
            int num_input_seqs = 0;
            int num_aln_seqs = 0;
            int num_missing_seqs = 0;
            int tot_alignments = 0;
            while ((kseq_read(seq)) >= 0) {
                ++num_input_seqs;
                string id = seq->name.s;
                string s = seq->seq.s;
                Alignments al;
                al.query_id = id;
                al.query_len = s.length();
                bool aligned = false;
                // get all the hits
                mem_alnreg_v ar = mem_align1(opt,
                                             idx->bwt,
                                             idx->bns,
                                             idx->pac,
                                             seq->seq.l,
                                             seq->seq.s);
                // traverse each hit
                int max_bwa_score = 0;
                for (unsigned int i = 0; i < ar.n; ++i) {
                    // get forward-strand position and CIGAR
                    mem_aln_t a = mem_reg2aln(opt,
                                              idx->bns,
                                              idx->pac,
                                              seq->seq.l,
                                              seq->seq.s,
                                              &ar.a[i]);

                    // INFO("Chr: " << idx->bns->anns[a.rid].name <<
                    //      " Pos: " << a.pos <<
                    //      " Rid: " << a.rid <<
                    //      " Secondary: " << ar.a[i].secondary <<
                    //      " Qual: " << ar.a[i].score <<
                    //      " Query Begin: " << ar.a[i].qb <<
                    //      " Aln_len: " << (ar.a[i].qe - ar.a[i].qb));

                    //Filter for valid alignments
                    if (a.rid < 0) continue;
                    //if (ar.a[i].score < al.query_len) continue;
					if (ar.a[i].truesc < al.query_len) continue;
                    int ref_aln_len = ar.a[i].re - ar.a[i].rb;
                    int query_aln_len = ar.a[i].qe - ar.a[i].qb;
                    if (ar.a[i].qb > PCR_THR) continue;
                    //Valid alignment
                    aligned = true;
                    ++tot_alignments;
                    AlnData data(idx->bns->anns[a.rid].name,
                                 ar.a[i].qb,
                                 ar.a[i].qb + query_aln_len - 1,
                                 a.pos, //ar.a[i].rb,
                                 a.pos + ref_aln_len - 1, //ar.a[i].re,
                                 +1,
                                 (a.is_rev ? -1 : +1),
                                 query_aln_len,
                                 ar.a[i].truesc);
                    al.al_data.push_back(data);
                    // Deallocate CIGAR
                    free(a.cigar);
                    if (ar.a[i].score > max_bwa_score) {
                        max_bwa_score = ar.a[i].score;
                    }
                }
                al.max_score = max_bwa_score;
                if (aligned) {
                    if (!masked_seq.empty()) {
                        if (masked_seq.find(al.query_id) !=  masked_seq.end()) {
                            // //Remove non-max_score aln for tot_masked seqs
                            // if (masked_seq[al.query_id].tot_masked) {
                            //     al.max_score = 0;
                            //     std::list< AlnData >::iterator it;
                            //     for (it = al.al_data.begin();
                            //          it != al.al_data.end();) {
                            //         if (it->score < max_bwa_score) {
                            //             it = al.al_data.erase(it);
                            //         } else {
                            //             ++it;
                            //         }
                            //     }
                            // }
                            //Add RepeatMasker to alignments
                            int max = masked_seq[al.query_id].rpt_id.size();
                            for (int i = 0; i < max; ++i) {
                                al.masked.push_back(masked_seq[al.query_id].rpt_id[i]);
                            }
                        }
                    }
                    ++num_aln_seqs;
                    //Print ASN out
                    *out_asn_stream << al;
#ifdef PRINT_CSV_ALN
                    //Print CSV out
                    printAln(al, out_csv_stream);
#endif
                } else {
                    ++num_missing_seqs;
                    out_missing_stream << id << "\n";
                }
#ifdef PRINT_CSV_ALN
                out_csv_stream.flush();
#endif
		out_missing_stream.flush();
                // Deallocate the hit list
                free(ar.a);
            }
            free(opt);
            kseq_destroy(seq);
            gzclose(fp);
            bwa_idx_destroy(idx);
            INFO("BWA alignment process finished.");
            //Print Results
            INFO("BWA RESULTS");
            INFO("Num. Input Sequences: " << num_input_seqs);
            INFO("Num. Aligned Sequences: " << num_aln_seqs);
            INFO("Num. Total Alignments: " << tot_alignments);
            INFO("Num. Not Aligned Sequences: " << num_missing_seqs);
        }}
    out_missing_stream.close();
#ifdef PRINT_CSV_ALN
    out_csv_stream.close();
#endif
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    AlignmentMissing app;
    return app.AppMain(argc, argv);
}

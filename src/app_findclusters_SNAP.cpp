/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"

#include <queue>
#include <iterator>

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <Snap.h>

#include <omp.h>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class FindClusters : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    int findSimilarClusters(const std::vector< ISCluster >& in_clu1,
			    const std::vector< ISCluster >& in_clu2,
			    const string& lab1,
			    const string& lab2,
			    PUNGraph& g,
			    const std::map< string, int >& nodes);
};

void writeSNAPGraph(const PUNGraph& g,
		    const std::map< int, std::string >& cluster_id,
		    const std::map< int, int >& weight,
		    const std::map< int, std::string >& exp_label,
		    ofstream& out) {
    out << "@nodes\n";
    out << "label\tclu_id\tweight\texp_label\n";
    for (TUNGraph::TNodeI ni = g->BegNI(); ni < g->EndNI(); ni++) {
	int node_id = ni.GetId();
	out << node_id << "\t"
	    << cluster_id.at(node_id) << "\t"
	    << weight.at(node_id) << "\t"
	    << exp_label.at(node_id) << "\n";
    }
    out << "@edges\n";
    out << "\t\tlabel\n";
    int edge_id = 0;
    for (TUNGraph::TEdgeI ei = g->BegEI(); ei < g->EndEI(); ei++) {
    	out << ei.GetSrcNId() << "\t" << ei.GetDstNId() << "\t" << edge_id << "\n";
	edge_id++;
    }
}

void FindClusters::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Program to find similar clusters obtained " \
			      "in different pipeline steps or experiments.");

    arg_desc->AddKey
        ("c1", "cluster1",
         "Input file in ASN format containing "	     \
         "the first set of clusters.",					\
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey
        ("c2", "cluster2",
         "Input file in ASN format containing "	      \
         "the second set of clusters.",					\
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddOptionalKey
	("l1", "label1",
	 "Label of the first dataset which will "	\
	 "be prefix of the clusters ids.",
	 CArgDescriptions::eString);

    arg_desc->AddOptionalKey
	("l2", "label2",
	 "Label of the second dataset which will "	\
	 "be prefix of the cluster ids.",
	 CArgDescriptions::eString);

    arg_desc->AddKey
	("o", "out",
	 "Output graph file: destination of serialization of similarities.",
	 CArgDescriptions::eOutputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddDefaultKey
	("num_threads", "num_threads",
	 "Number of threads used to analyze clusters. Default: 1.",
	 CArgDescriptions::eInteger, "1");

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

void printMessage() {
    std::cout << "**********************************\n";
    std::cout << "WARNING:\n";
    std::cout << "The program assumes that clusters\n";
    std::cout << "in the same list have been already\n";
    std::cout << "merged among themselves and they\n";
    std::cout << "are distinct.\n";
    std::cout << "**********************************\n";
    std::cout << std::endl;
}

int FindClusters::findSimilarClusters(const std::vector< ISCluster >& in_clu1,
				      const std::vector< ISCluster >& in_clu2,
				      const string& lab1,
				      const string& lab2,
				      PUNGraph& g,
				      const std::map< string, int >& nodes) {
    int num_similar = 0;
    SingleClusterOperations sclu_op;
#pragma omp parallel for schedule(dynamic, 1) shared(num_similar, g) private(sclu_op)
    for(unsigned int it1 = 0; it1 < in_clu1.size(); ++it1) {
	std::set< std::pair< string, string > > comm_edges;
        for(unsigned int it2 = 0; it2 < in_clu2.size(); ++it2) {
	    // bool similar = sclu_op.checkSingleClusters_old(in_clu1.at(it1), in_clu2.at(it2));
            bool similar = sclu_op.checkSingleClusters(in_clu1.at(it1), in_clu2.at(it2));
	    if (similar) {
		comm_edges.insert(make_pair(lab1 + FS_LABEL + in_clu1.at(it1).clu_id,
					    lab2 + FS_LABEL + in_clu2.at(it2).clu_id));
	    }
	}
#pragma omp critical
	{
	    for(std::set< std::pair< string, string > >::const_iterator ce_it = comm_edges.begin();
		ce_it != comm_edges.end(); ++ce_it) {
		g->AddEdge(nodes.at(ce_it->first), nodes.at(ce_it->second));
	    }
	    num_similar += comm_edges.size();
	}
#pragma omp flush(num_similar, g)
    }
    return num_similar;
}

int FindClusters::Run(void){
    printMessage();
    const CArgs& args = GetArgs();
    assert(args.Exist("c1"));
    assert(args.Exist("c2"));
    //Chech input files
    CFile in_c1_file(args["c1"].AsString());
    if(!in_c1_file.Exists()) {
	ERROR("File " << args["c1"].AsString() << " not found.");
	return 1;
    }
    CFile in_c2_file(args["c2"].AsString());
    if(!in_c2_file.Exists()) {
	ERROR("File " << args["c2"].AsString() << " not found.");
	return 1;
    }

    string lab1 = args["c1"].AsString();
    if(args["l1"].HasValue()) {
	lab1 = args["l1"].AsString();
    }

    string lab2 = args["c2"].AsString();
    if(args["l2"].HasValue()) {
        lab2 = args["l2"].AsString();
    }

    //Output
    string outgraph = args["o"].AsString();
    ofstream out_graph_stream(outgraph.c_str());
    if(!out_graph_stream.is_open()) {
	ERROR("Error in creating " << outgraph << " file.");
	return 1;
    }

    INFO("PARAMETERS");
    INFO("Input Cluster1 file: " << args["c1"].AsString());
    INFO("Input Cluster2 file: " << args["c2"].AsString());
    INFO("Input Label Cluster1: " << lab1);
    INFO("Input Label Cluster2: " << lab2);
    INFO("Output Graph File: " << outgraph);
#ifdef ENABLE_OPENMP
    if (args["num_threads"].AsInteger() > 1) {
        if (args["num_threads"].AsInteger() < omp_get_num_procs()) {
            omp_set_num_threads(args["num_threads"].AsInteger());
            INFO("N. Threads: " << args["num_threads"].AsInteger());
        } else {
            omp_set_num_threads(omp_get_num_procs());
            INFO("N. Threads: " << omp_get_num_procs());
        }
    } else {
        INFO("N. Threads: 1");
    }
#endif
    INFO("Started reading input files.");
    PUNGraph g = TUNGraph::New();
    std::map< int, std::string > cluster_id;
    std::map< int, int > weight;
    std::map< int, std::string > exp_label;
    std::map< string, int > nodes;
    
    int node_id = 0;
    std::vector< ISCluster > clu1;
    {{
	    auto_ptr< CObjectIStream > in_clu1_stream
		(CObjectIStream::Open(CLU_TYPE, args["c1"].AsString()));
	    if(!in_clu1_stream->InGoodState()) {
		ERROR("Error in opening " << args["c1"].AsString() << " file.");
		return 1;
	    }
	    //Read cluster1
	    while(!in_clu1_stream->EndOfData()) {
		ISCluster c;
		*in_clu1_stream >> c;
		clu1.push_back(c);
		g->AddNode(node_id);
		string n_id = lab1 + FS_LABEL + c.clu_id;
		cluster_id[node_id] = n_id;
		weight[node_id] = c.weight;
		exp_label[node_id] = lab1;
		nodes[n_id] = node_id;
		node_id++;
	    }
    }}
	
    std::vector< ISCluster > clu2;
    {{
	    auto_ptr< CObjectIStream > in_clu2_stream
		(CObjectIStream::Open(CLU_TYPE, args["c2"].AsString()));
	    if(!in_clu2_stream->InGoodState()) {
		ERROR("Error in opening " << args["c2"].AsString() << " file.");
		return 1;
	    }
	    //Read cluster2
	    while(!in_clu2_stream->EndOfData()) {
		ISCluster c;
		*in_clu2_stream >> c;
		clu2.push_back(c);
		g->AddNode(node_id);
		string n_id = lab2 + FS_LABEL + c.clu_id;
		cluster_id[node_id] = n_id;
		weight[node_id] = c.weight;
		exp_label[node_id] = lab2;
                nodes[n_id] = node_id;
		node_id++;
	    }
    }}
    INFO("Finished reading input files.");

    //Find similar clusters
    INFO("Started finding similar clusters.");
    int num_similar = findSimilarClusters(clu1, clu2, lab1, lab2, g, nodes);
    INFO("Finished finding similar clusters.");

    //Serialize graph
    writeSNAPGraph(g, cluster_id, weight, exp_label, out_graph_stream);
    out_graph_stream.flush();
    out_graph_stream.close();

    INFO("Num. Cluster1 clusters: " << clu1.size());
    INFO("Num. Cluster2 clusters: " << clu2.size());
    INFO("Num. Similar clusters: " << num_similar);
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    FindClusters app;
    return app.AppMain(argc, argv);
}

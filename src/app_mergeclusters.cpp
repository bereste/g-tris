/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"

#include <queue>
#include <iterator>

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class MergeClusters : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    int mergeSimilarClusters(std::list< ISCluster >& in_clu1,
			     std::list< ISCluster >& in_clu2);
};

void MergeClusters::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Program to merge similar clusters obtained " \
			      "in different pipeline steps or experiments.");

    arg_desc->AddKey
        ("c1", "cluster1",
         "Input file in ASN format containing "	     \
         "the first set of clusters.",					\
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey
        ("c2", "cluster2",
         "Input file in ASN format containing "                         \
         "the second set of clusters.",               \
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

void printMessage() {
    std::cout << "**********************************\n";
    std::cout << "WARNING:\n";
    std::cout << "The program assumes that clusters\n";
    std::cout << "in the same list have been already\n";
    std::cout << "merged among themselves and they\n";
    std::cout << "are distinct.\n";
    std::cout << "**********************************\n";
    std::cout << std::endl;
}

int MergeClusters::mergeSimilarClusters(std::list< ISCluster >& in_clu1,
					std::list< ISCluster >& in_clu2) {
    int num_merged = 0;
    SingleClusterOperations sclu_op;
    for(std::list< ISCluster >::iterator it1 = in_clu1.begin(); it1 != in_clu1.end(); ++it1) {
        for(std::list< ISCluster >::iterator it2 = in_clu2.begin(); it2 != in_clu2.end();) {
	    bool merged = sclu_op.mergeSingleClusters(*it1, *it2);
	    if (merged) {
		++num_merged;
		it2 = in_clu2.erase(it2);
	    } else {
                ++it2;
            }
        }
    }
    return num_merged;
}

int MergeClusters::Run(void){
    printMessage();
    const CArgs& args = GetArgs();
    assert(args.Exist("c1"));
    assert(args.Exist("c2"));
    //Chech input files
    CFile in_c1_file(args["c1"].AsString());
    if(!in_c1_file.Exists()) {
	ERROR("File " << args["c1"].AsString() << " not found.");
	return 1;
    }
    CFile in_c2_file(args["c2"].AsString());
    if(!in_c2_file.Exists()) {
	ERROR("File " << args["c2"].AsString() << " not found.");
	return 1;
    }
    //Bed output
    //string outbed = in_c1_file.GetBase() + ".merged.bed";
    string outbed = in_c1_file.GetDir() + in_c1_file.GetBase() + ".merged.bed";
    ofstream out_bed_stream(outbed.c_str());
    if(!out_bed_stream.is_open()) {
	ERROR("Error in creating " << outbed << " file.");
	return 1;
    }

    //Merged clusters (serialized) output
    //string outclu = in_c1_file.GetBase() + ".merged.clu";
    string outclu = in_c1_file.GetDir() + in_c1_file.GetBase() + ".merged.clu";
    auto_ptr<CObjectOStream>
        out_clu_stream(CObjectOStream::Open(CLU_TYPE, outclu));
    if(!out_clu_stream->InGoodState()) {
	ERROR("Error in creating " << outclu << " file.");
	return 1;
    }

    INFO("PARAMETERS");
    INFO("Input Cluster1 file: " << args["c1"].AsString());
    INFO("Input Cluster2 file: " << args["c2"].AsString());
    INFO("Output Merged BED file: " << outbed);
    INFO("Output Merged CLU file: " << outclu);

    INFO("Started reading input files.");
    std::list< ISCluster > clu1;
    {{
	    auto_ptr< CObjectIStream > in_clu1_stream
		(CObjectIStream::Open(CLU_TYPE, args["c1"].AsString()));
	    if(!in_clu1_stream->InGoodState()) {
		ERROR("Error in opening " << args["c1"].AsString() << " file.");
		return 1;
	    }
	    //Read cluster1
	    while(!in_clu1_stream->EndOfData()) {
		ISCluster c;
		*in_clu1_stream >> c;
		clu1.push_back(c);
	    }
    }}
	
    std::list< ISCluster > clu2;
    {{
	    auto_ptr< CObjectIStream > in_clu2_stream
		(CObjectIStream::Open(CLU_TYPE, args["c2"].AsString()));
	    if(!in_clu2_stream->InGoodState()) {
		ERROR("Error in opening " << args["c2"].AsString() << " file.");
		return 1;
	    }
	    //Read cluster2
	    while(!in_clu2_stream->EndOfData()) {
		ISCluster c;
		*in_clu2_stream >> c;
		clu2.push_back(c);
	    }
    }}
    INFO("Finished reading input files.");
    int num_clu1_init = clu1.size();
    int num_clu2_init = clu2.size();

    //Merge similar clusters
    INFO("Started merging similar clusters.");
    int num_merged = mergeSimilarClusters(clu1, clu2);
    clu1.splice(clu1.end(), clu2);
    INFO("Finished merging similar clusters.");

    //Print header in BED format
    out_bed_stream << "track name=clusters ";
    out_bed_stream << "description=\"Integration Sites Clusters.\" ";
    out_bed_stream << "useScore=1" << std::endl;
    //Serialize the CLU output and print on BED file
    std::list< ISCluster >::const_iterator clu_it;
    for(clu_it = clu1.begin(); clu_it != clu1.end(); ++clu_it) {
        *out_clu_stream << *clu_it;
	out_bed_stream << *clu_it;
    }
    //Flush and Close
    out_bed_stream.flush();
    out_bed_stream.close();
    out_clu_stream->Flush();

    INFO("Num. Initial Cluster1 clusters: " << num_clu1_init);
    INFO("Num. Initial Cluster2 clusters: " << num_clu2_init);
    INFO("Num. Merged clusters: " << num_merged);
    INFO("Num. Final clusters: " << clu1.size());
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    MergeClusters app;
    return app.AppMain(argc, argv);
}

/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "blast_executer.hpp"

#include <objtools/readers/rm_reader.hpp>

#include <omp.h>

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

#include "bwa/bwamem.h"

KSEQ_INIT(gzFile, gzread)

USING_NCBI_SCOPE;
USING_SCOPE(objects);


CConstRef< CUser_object > FindExt(const CSeq_feat& feat,
                                  const string& ext_type) {
    CConstRef< CUser_object > ret;
    if (feat.IsSetExts()) {
        ITERATE(CSeq_feat::TExts, it, feat.GetExts()) {
            const CObject_id& obj_type = (*it)->GetType();
            if (obj_type.IsStr() && obj_type.GetStr() == ext_type) {
                ret.Reset(it->GetPointer());
                break;
            }
        }
    }
    return ret;
}

CRef< CUser_object > FindExt(CSeq_feat& feat,
                             const string& ext_type) {
    CRef< CUser_object > ret;
    if (feat.IsSetExts()) {
        NON_CONST_ITERATE(CSeq_feat::TExts, it, feat.SetExts()) {
            const CObject_id& obj_type = (*it)->GetType();
            if (obj_type.IsStr() && obj_type.GetStr() == ext_type) {
                ret.Reset(it->GetPointer());
                break;
            }
        }
    }
    return ret;
}

class IterativeAlignment : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    // virtual void Exit();
};

void IterativeAlignment::Init(void){
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Iterative Sequence Alignment program");

    arg_desc->AddKey
        ("i", "inputfile",
         "Input FASTA file of all the sequences.",
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddOptionalKey
        ("db", "database",
         "BLAST database name. Default: input FASTA file.",
          CArgDescriptions::eInputFile);

    arg_desc->AddOptionalKey
        ("mask", "mask_aln",
         "RepeatMasker out file. Default: none.",
          CArgDescriptions::eInputFile);

    arg_desc->AddDefaultKey
        ("batch_size", "batch_size",
         "Number of characters of the sequences to be processed " \
         "for every batch of the alignment. Default: 1000000 (1M).",
         CArgDescriptions::eInteger, BLAST_BATCH_SIZE);

    CArgAllow* batch_cond =
        new CArgAllow_Integers(100,1000000000);

    arg_desc->SetConstraint("batch_size", batch_cond);

    arg_desc->AddDefaultKey
        ("num_threads", "num_threads",
         "Number of threads used by BLAST. Default: 1.",
         CArgDescriptions::eInteger, BLAST_THREADS);

    CArgAllow* threads_cond =
        new CArgAllow_Integers(1,32);

    arg_desc->SetConstraint("num_threads", threads_cond);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

struct rpt {
    bool tot_masked;
    vector< bool > q_mask;
    vector< string > rpt_id;
    vector< pair< int, int > > rpt_loc;
};

int IterativeAlignment::Run(void) {
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));
    assert(args.Exist("db"));

    //Check input files
    CFile in_fasta_file(args["i"].AsString());
    if(!in_fasta_file.Exists()) {
        ERROR("File " << args["i"].AsString() << " not found.");
        return 1;
    }
    CFile in_db_file(args["db"].AsString());
    if(!in_db_file.Exists()) {
        ERROR("File " << args["db"].AsString() << " not found.");
        return 1;
    }
    if (args["mask"].HasValue()) {
        CFile in_mask_file(args["mask"].AsString());
        if(!in_mask_file.Exists()) {
            ERROR("File " << args["mask"].AsString() << " not found.");
            return 1;
        }
    }

    //Check output files
    string outasn = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.asn";
    auto_ptr< CObjectOStream >
        out_asn_stream(CObjectOStream::Open(ASN_TYPE, outasn));
    if(!out_asn_stream->InGoodState()) {
        ERROR("Problem in creating " << outasn << " file.");
        return 1;
    }
#ifdef PRINT_CSV_ALN
    string outcsv = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.csv";
    CNcbiOfstream out_csv_stream(outcsv.c_str());
    if(!out_csv_stream.is_open()) {
        ERROR("Error in creating " << outcsv << " file.");
        return 1;
    }
#endif
    string outmissing = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".out.missing";

    //Prepare tmp files
    string tmpfasta = in_fasta_file.GetDir() + "_tmp_fasta_file_";
    CNcbiOfstream tmp_fasta_stream(tmpfasta.c_str());
    if(!tmp_fasta_stream.is_open()) {
        ERROR("Error in creating " << tmpfasta << " file.");
        return 1;
    }
    string tmpasn = in_fasta_file.GetDir() + "_tmp_asn_file_";
#ifdef PRINT_CSV_ALN
    string tmpcsv = in_fasta_file.GetDir() + "_tmp_csv_file_";
#endif
    string tmpmissing = in_fasta_file.GetDir() + "_tmp_missing_file_";

    int num_threads = 1;
#ifdef ENABLE_OPENMP
    if (args["num_threads"].AsInteger() > 1) {
        if (args["num_threads"].AsInteger() < omp_get_num_procs()) {
            omp_set_num_threads(args["num_threads"].AsInteger());
            num_threads = args["num_threads"].AsInteger();
        } else {
            omp_set_num_threads(omp_get_num_procs());
            num_threads = omp_get_num_procs();
        }
    }
#endif

    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input FASTA file: " << args["i"].AsString());
    INFO("Input DB file: " << args["db"].AsString());
    if(args["mask"].HasValue()) {
        INFO("Input RepeatMasker file: " << args["mask"].AsString());
    }
    INFO("Output ASN file: " << outasn);
#ifdef PRINT_CSV_ALN
    INFO("Output CSV file: " << outcsv);
#endif
    INFO("Output Missing file: " << outmissing);
    INFO("Num. Threads: " << num_threads);
    INFO("BLAST Batch Size: " << args["batch_size"].AsString());

    // Check RepeatMasker file
    std::map< string, rpt > masked_seq;
    if (args["mask"].HasValue()) {
        CNcbiIfstream in_mask_stream(args["mask"].AsString().c_str());
	if(!in_mask_stream.is_open()) {
	    ERROR("Error in opening " << args["mask"].AsString() << " file.");
	    return 1;
	}
        //Skip RepeatMasker out file header (2 lines)
        string l = "";
        NcbiGetlineEOL(in_mask_stream, l);
        NcbiGetlineEOL(in_mask_stream, l);
        CRepeatMaskerReader reader(CRepeatMaskerReader::fStandardizeNomenclature |
                                   CRepeatMaskerReader::fIncludeAll);

        CRef< CSeq_annot > annot(reader.ReadSeqAnnot(in_mask_stream));
        CSeq_annot::C_Data::TFtable ftable = annot->GetData().GetFtable();
        for (CSeq_annot::TData::TFtable::iterator feat_it = ftable.begin();
             ftable.end() != feat_it; ++feat_it) {
            CRef< CSeq_feat > feature = (*feat_it);
            if (!feature->CanGetLocation() ){
                ERROR("RepeatMasker out: can't get location.");
                continue;
            }
            string q_id = feature->GetLocation().GetId()->GetSeqIdString();
            int q_start = feature->GetLocation().GetTotalRange().GetFrom();
            int q_stop = feature->GetLocation().GetTotalRange().GetTo();
            int q_len = FindExt(*feature, "RepeatMasker")->GetField("query_length").GetData().GetInt();
            string rpt_id = feature->GetNamedQual("rpt_family") + "_";
            rpt_id += feature->GetNamedQual("satellite");
            rpt_id += feature->GetNamedQual("rpt_unit");
            rpt_id += feature->GetNamedQual("rpt_unit_seq");
            rpt_id += feature->GetNamedQual("rpt_type");
            rpt_id += feature->GetNamedQual("standard_name");
            rpt_id += feature->GetNamedQual("mobile_element");
            if (rpt_id == "_") {
                WARN("RepeatMasker out: can't get repeat info.");
            }
            if (masked_seq.find(q_id) == masked_seq.end()) {
                rpt r;
                r.tot_masked = false;
                r.q_mask.clear();
                r.q_mask.resize(q_len, false);
                masked_seq[q_id] = r;
            }
            fill(masked_seq[q_id].q_mask.begin() + q_start,
                 masked_seq[q_id].q_mask.begin() + q_stop,
                 true);
            if (std::count(masked_seq[q_id].q_mask.begin(),
                           masked_seq[q_id].q_mask.end(), false) <
                FILTER_LEN_THR) {
                masked_seq[q_id].tot_masked = true;
            }
            masked_seq[q_id].rpt_id.push_back(rpt_id);
            masked_seq[q_id].rpt_loc.push_back(make_pair(q_start, q_stop));
        }
        std::map< string, rpt >::const_iterator mask_it;
        int t = 0;
        for (mask_it = masked_seq.begin();
             mask_it != masked_seq.end(); ++mask_it) {
            //     std::cout << mask_it->first << " ";
            if (mask_it->second.tot_masked) {
                //         std::cout << "TOT";
                ++t;
            }
            //     std::cout << std::endl;
            //     int max = mask_it->second.rpt_id.size();
            //     for (int i = 0; i < max; ++i) {
            //         std::cout << mask_it->second.rpt_id[i] << ":";
            //         std::cout << mask_it->second.rpt_loc[i].first << "-";
            //         std::cout << mask_it->second.rpt_loc[i].second << " ";
            //     }
            //     std::cout << std::endl;
        }
        INFO("Masked Seqs.: " << masked_seq.size());
        INFO("Tot. Masked Seqs.: " << t);
    }

    {
	INFO("BWA alignment process started.");
	bwaidx_t *idx;
	// load the BWA index
	idx = bwa_idx_load(args["db"].AsString().c_str(), BWA_IDX_ALL);
	if (idx == NULL) {
	    ERROR("BWA index load failed.");
	    return 1;
	}
	mem_opt_t *opt;
	// initialize the BWA-MEM parameters to the default values
	opt = mem_opt_init();
	opt->w = BWA_W;
	opt->max_occ = BWA_MAX_OCC;
	opt->min_seed_len = BWA_MIN_SEED_LEN;
	opt->n_threads = args["num_threads"].AsInteger();
	INFO("BWA band width: " << BWA_W);
	INFO("BWA max number of occurrences: " << BWA_MAX_OCC);
	INFO("BWA min seed len: " << BWA_MIN_SEED_LEN);
	INFO("BWA min alignment score: " << BWA_ALN_THR);
	gzFile fp = gzopen(args["i"].AsString().c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	int num_input_seqs = 0;
	int num_aln_seqs = 0;
	int num_missing_seqs = 0;
	int tot_alignments = 0;
	while ((kseq_read(seq)) >= 0) {
	    ++num_input_seqs;
	    string id = seq->name.s;
	    string s = seq->seq.s;
	    Alignments al;
	    al.query_id = id;
	    al.query_len = s.length();
	    bool aligned = false;
	    // get all the hits
	    mem_alnreg_v ar = mem_align1(opt,
					 idx->bwt,
					 idx->bns,
					 idx->pac,
					 seq->seq.l,
					 seq->seq.s);
	    // traverse each hit
	    int max_bwa_score = 0;
	    for (unsigned int i = 0; i < ar.n; ++i) {
		// get forward-strand position and CIGAR
		mem_aln_t a = mem_reg2aln(opt,
					  idx->bns,
					  idx->pac,
					  seq->seq.l,
					  seq->seq.s,
					  &ar.a[i]);

		// INFO("Chr: " << idx->bns->anns[a.rid].name <<
		//      " Pos: " << a.pos <<
		//      " Rid: " << a.rid <<
		//      " Secondary: " << ar.a[i].secondary <<
		//      " Qual: " << ar.a[i].score <<
		//      " Query Begin: " << ar.a[i].qb <<
		//      " Aln_len: " << (ar.a[i].qe - ar.a[i].qb));
		
		//Filter for valid alignments
		if (a.rid < 0) continue; // || ar.a[i].secondary >= 0) continue;
		if (ar.a[i].score < BWA_ALN_THR) continue;
		int ref_aln_len = ar.a[i].re - ar.a[i].rb;
		int query_aln_len = ar.a[i].qe - ar.a[i].qb;
		if (ar.a[i].qb > PCR_THR) continue;
		if (query_aln_len < ALN_PERC_ID * al.query_len) continue;
		//Valid alignment
		aligned = true;
		++tot_alignments;
		AlnData data(idx->bns->anns[a.rid].name,
			     ar.a[i].qb,
			     ar.a[i].qb + query_aln_len - 1,
			     a.pos, //ar.a[i].rb,
			     a.pos + ref_aln_len - 1, //ar.a[i].re,
			     +1,
			     (a.is_rev ? -1 : +1),
			     query_aln_len,
			     ar.a[i].score);
		al.al_data.push_back(data);
		// Deallocate CIGAR
		free(a.cigar);
		if (ar.a[i].score > max_bwa_score) {
		    max_bwa_score = ar.a[i].score;
		}
	    }
	    al.max_score = max_bwa_score;
	    if (aligned) {
		if (!masked_seq.empty()) {
		    if (masked_seq.find(al.query_id) !=  masked_seq.end()) {
			// //Remove non-max_score aln for tot_masked seqs
			// if (masked_seq[al.query_id].tot_masked) {
			//     al.max_score = 0;
			//     std::list< AlnData >::iterator it;
			//     for (it = al.al_data.begin();
			//          it != al.al_data.end();) {
			//         if (it->score < max_bwa_score) {
			//             it = al.al_data.erase(it);
			//         } else {
			//             ++it;
			//         }
			//     }
			// }
			//Add RepeatMasker to alignments
			int max = masked_seq[al.query_id].rpt_id.size();
			for (int i = 0; i < max; ++i) {
			    al.masked.push_back(masked_seq[al.query_id].rpt_id[i]);
			}
		    }
		}
		++num_aln_seqs;
		//Print ASN out
		*out_asn_stream << al;
#ifdef PRINT_CSV_ALN
		//Print CSV out
		printAln(al, out_csv_stream);
#endif
	    } else {
		++num_missing_seqs;
		tmp_fasta_stream << ">" << id << "\n" << s << std::endl;
	    }
#ifdef PRINT_CSV_ALN
	    out_csv_stream.flush();
#endif
	    // Deallocate the hit list
	    free(ar.a);
	}
	free(opt);
	kseq_destroy(seq);
	gzclose(fp);
	bwa_idx_destroy(idx);
	INFO("BWA alignment process finished.");
	//Print Results
	INFO("BWA RESULTS");
	INFO("Num. Input Sequences: " << num_input_seqs);
	INFO("Num. Aligned Sequences: " << num_aln_seqs);
	INFO("Num. Total Alignments: " << tot_alignments);
	INFO("Num. Not Aligned Sequences: " << num_missing_seqs);
    }
    tmp_fasta_stream.flush();
    tmp_fasta_stream.close();
    out_asn_stream->Flush();
    out_asn_stream->Close();
#ifdef PRINT_CSV_ALN
    out_csv_stream.flush();
    out_csv_stream.close();
#endif
    int ws[] = {20, 15, 11, 11, 11, 11, 11, 11, 11, 11};
    double perc_ids[] = {100, 100, 100, 99, 98, 95, 90, 88, 85, 80};
    double evals[] = {0.0000001,
                      0.0000001,
                      0.000001,
                      0.00001,
                      0.0001,
                      0.001,
                      0.001,
                      0.01,
                      0.1,
                      0.1};
    for (int i = 0; i < 7; ++i) {
        INFO("Executing BLAST -> ws: " << ws[i] <<
             " eval: " << evals[i] <<
             " %id: " << perc_ids[i]);

	vector< std::ofstream* > input_streams;
	for(int j=0; j < num_threads; ++j) {
	    stringstream ss;
	    ss << j;
	    string tmp_f_name = tmpfasta + ss.str() + "_";
	    std::ofstream* tmp_f_stream = new ofstream(tmp_f_name.c_str());
	    if(!tmp_f_stream->is_open()) {
		ERROR("Error in creating " << tmp_f_name << " file.");
		return 1;
	    }
	    input_streams.push_back(tmp_f_stream);
	}

	gzFile fp = gzopen(tmpfasta.c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	int num_seq = 0;
	while ((kseq_read(seq)) >= 0) {
	    string id = seq->name.s;
	    string s = seq->seq.s;
	    int num_thr = num_seq % num_threads;
	    (*input_streams[num_thr]) << ">" << id << "\n" << s << "\n";
	    num_seq++;
	}
	gzclose(fp);
	for(int j=0; j < num_threads; ++j) {
	    input_streams[j]->close();
	}
	input_streams.clear();
	int num_input_seqs = 0;
	int num_aln_seqs = 0;
	int tot_alignments = 0;
	int num_missing_seqs = 0;
	bool blast_error = false;
	INFO("Num.sequences to align: " << num_seq);

#pragma omp parallel for schedule(dynamic, 1)
	for(int j = 0; j < num_threads; ++j) {
	    stringstream ss;
	    ss << j;
	    string tmp_in_asn_name = tmpasn + ss.str() + "_in_";
	    string tmp_out_asn_name = tmpasn + ss.str() + "_out_";
#ifdef PRINT_CSV_ALN
	    string tmp_csv_name = tmpcsv + ss.str() + "_";
#endif
            string tmp_missing_name = tmpmissing + ss.str() + "_";
	    BlastExecuter blast(tmpfasta + ss.str() + "_",
				args["db"].AsString(),
				tmp_in_asn_name,
#ifdef PRINT_CSV_ALN
				tmp_csv_name,
#endif
				tmp_missing_name,
				ws[i],
				evals[i],
				perc_ids[i],
				args["batch_size"].AsInteger(),
				1,
				false);
	    int blast_stat = blast.executeBlast();

#pragma omp critical
	    {
		if(blast_stat == 1) {
		    ERROR("Problem in executing BLAST.");
		    blast_error = true;
		}
		num_input_seqs += blast.num_input_seqs;
		num_aln_seqs += blast.num_aln_seqs;
		tot_alignments += blast.tot_alignments;
		num_missing_seqs += blast.num_missing_seqs;
	    }

	    //Read tmp asn results
            auto_ptr< CObjectIStream > tmp_in_asn_stream
                (CObjectIStream::Open(ASN_TYPE, tmp_in_asn_name.c_str()));
            if(!tmp_in_asn_stream->InGoodState()) {
                ERROR("Error in opening " << tmp_in_asn_name << " file.");
                blast_error = true;
            }
	    //Write tmp asn results
	    auto_ptr< CObjectOStream > tmp_out_asn_stream
                (CObjectOStream::Open(ASN_TYPE, tmp_out_asn_name.c_str()));
            if(!tmp_out_asn_stream->InGoodState()) {
                ERROR("Error in creating " << tmp_out_asn_name << " file.");
                blast_error = true;
            }
#ifdef PRINT_CSV_ALN
	    //Re-write CSV tmp out
            ofstream tmp_csv_stream(tmp_csv_name.c_str());
            if(!tmp_csv_stream.is_open()) {
                ERROR("Error in creating " << tmp_csv_name << " file.");
                blast_error = true;
            }
#endif
            //Append tmp missing results
            ofstream tmp_missing_stream(tmp_missing_name.c_str(), ios::out | ios::app);
            if(!tmp_missing_stream.is_open()) {
                ERROR("Error in opening " << tmp_missing_name << " file.");
                blast_error = true;
            }
	    while (!tmp_in_asn_stream->EndOfData()) {
                Alignments al;
                *tmp_in_asn_stream >> al;
		//Filter not valid alignments
		std::list< AlnData >::iterator it;
                //Filter invalid alignments
                //Remove aln starting after PCR_THR position
                //Remove aln aligning less than ALN_PERC_ID of the seq
                for (it = al.al_data.begin(); it != al.al_data.end();) {
                    if (it->query_start > PCR_THR ||
                        (it->alignment_len < ALN_PERC_ID * al.query_len)) {
                        it = al.al_data.erase(it);
                    } else {
                        ++it;
                    }
                }
                //Compute max_score after removing invalid alignments
                int max_blast_score = 0;
                for (it = al.al_data.begin(); it != al.al_data.end();++it) {
                    if (it->score > max_blast_score) {
                        max_blast_score = it->score;
                    }
                }
                al.max_score = max_blast_score;
                //Check if valid alignments exist
                if (!al.al_data.empty()) {
                    if (!masked_seq.empty() &&
                        masked_seq.find(al.query_id) !=  masked_seq.end()) {
                        //Add RepeatMasker to alignments
                        int max = masked_seq[al.query_id].rpt_id.size();
                        for (int i = 0; i < max; ++i) {
                            al.masked.push_back(masked_seq[al.query_id].rpt_id[i]);
                        }
                    }
                    if (al.al_data.size() > MAX_VALID_ALN) {
                        //Remove non-max_score aln for non-masked
                        //aln having more than MAX_VALID_ALN aln
                        for (it = al.al_data.begin();
                             it != al.al_data.end();) {
                            if (it->score < max_blast_score) {
                                it = al.al_data.erase(it);
                            } else {
                                ++it;
                            }
                        }
                    }
                    //Print ASN out
                    *tmp_out_asn_stream << al;
#ifdef PRINT_CSV_ALN
                    //Print CSV out
                    printAln(al, tmp_csv_stream);
#endif
                } else {
                    //Only not valid alignments.
		    tmp_missing_stream << al.query_id << "\n";
                }
            }
	    tmp_in_asn_stream->Close();
	    remove(tmp_in_asn_name.c_str());
	    tmp_out_asn_stream->Flush();
	    tmp_out_asn_stream->Close();
#ifdef PRINT_CSV_ALN
	    tmp_csv_stream.flush();
	    tmp_csv_stream.close();
#endif
	    tmp_missing_stream.flush();
	    tmp_missing_stream.close();
	}
#ifdef PRINT_CSV_ALN
	//CSV
	std::ofstream out_csv_stream(outcsv.c_str(), std::ios_base::binary | ios::app);
	if(!out_csv_stream.is_open()) {
	    ERROR("Error in creating " << outcsv << " file.");
	    return 1;
	}
#endif
	//ASN
	std::ofstream out_asn_stream(outasn.c_str(), std::ios_base::binary | ios::app);
	if(!out_asn_stream.is_open()) {
	    ERROR("Error in creating " << outasn << " file.");
	    return 1;
	}
	//MISSING
	std::ofstream out_missing_stream(outmissing.c_str(), std::ios_base::binary);
	if(!out_missing_stream.is_open()) {
	    ERROR("Error in creating " << outmissing << " file.");
	    return 1;
	}

        for(int j=0; j < num_threads; ++j) {
	    stringstream ss;
	    ss << j;
	    //FASTA
	    string tmp_f_name = tmpfasta + ss.str() + "_";
	    remove(tmp_f_name.c_str());
#ifdef PRINT_CSV_ALN
	    //CSV
            string tmp_csv_name = tmpcsv + ss.str() + "_";
	    CFile tmp_csv_file(tmp_csv_name);
	    if(tmp_csv_file.GetLength() > 0) {
		std::ifstream tmp_csv_stream(tmp_csv_name.c_str(), std::ios_base::binary);
		if(!tmp_csv_stream.is_open()) {
		    ERROR("Error in opening " << tmp_csv_name.c_str() << " file.");
		    return 1;
		}
		out_csv_stream << tmp_csv_stream.rdbuf();
		tmp_csv_stream.close();
	    }
	    tmp_csv_file.Remove();
#endif
	    //ASN
            string tmp_out_asn_name = tmpasn + ss.str() + "_out_";
	    CFile tmp_out_asn_file(tmp_out_asn_name);
	    if(tmp_out_asn_file.GetLength() > 0) {
		std::ifstream tmp_asn_stream(tmp_out_asn_name.c_str(), std::ios_base::binary);
		if(!tmp_asn_stream.is_open()) {
		    ERROR("Error in opening " << tmp_out_asn_name.c_str() << " file.");
		    return 1;
		}
		out_asn_stream << tmp_asn_stream.rdbuf();
		tmp_asn_stream.close();
	    }
	    tmp_out_asn_file.Remove();
	    //MISSING
	    string tmp_missing_name = tmpmissing + ss.str() + "_";
	    CFile tmp_missing_file(tmp_missing_name);
	    if(tmp_missing_file.GetLength() > 0) {
		std::ifstream tmp_missing_stream(tmp_missing_name.c_str(), std::ios_base::binary);
		if(!tmp_missing_stream.is_open()) {
		    ERROR("Error in opening " << tmp_missing_name.c_str() << " file.");
		    return 1;
		}
		out_missing_stream << tmp_missing_stream.rdbuf();
		tmp_missing_stream.close();
	    }
	    tmp_missing_file.Remove();

        }
#ifdef PRINT_CSV_ALN
	out_csv_stream.flush();
	out_csv_stream.close();
#endif
	out_asn_stream.flush();
	out_asn_stream.close();
	out_missing_stream.flush();
	out_missing_stream.close();
	//Look for BLAST errors
	if(blast_error) {
	    ERROR("BLAST execution error.");
	    return 1;
	}

	//Parse missing sequences
	std::set< string > missing_id;
        string line;
        ifstream in_missing_stream(outmissing.c_str());
        if(in_missing_stream.is_open()) {
            while(getline(in_missing_stream, line)) {
                missing_id.insert(line);
            }
            in_missing_stream.close();
        } else {
            ERROR("Error in opening " << outmissing << " file.");
            return 1;
        }
        if (missing_id.size() == 0) {
            INFO("All sequences are aligned.");
            break;
        }
        ofstream tmp_fasta_stream(tmpfasta.c_str());
        if(!tmp_fasta_stream.is_open()) {
            ERROR("Error in opening " << tmpfasta << " file.");
            return 1;
        }
        {
            gzFile fp = gzopen(args["i"].AsString().c_str(), "r");
            kseq_t* seq = kseq_init(fp);
            while ((kseq_read(seq)) >= 0) {
                string id = seq->name.s;
                string s = seq->seq.s;
                if(missing_id.find(id) != missing_id.end()){
                    tmp_fasta_stream << ">" << id << "\n" << s << "\n";
                }
            }
            tmp_fasta_stream.flush();
            gzclose(fp);
        }
        tmp_fasta_stream.close();

	//Print Results
	INFO("BLAST RESULTS");
	INFO("Num. Input Sequences: " << num_input_seqs);
	INFO("Num. Aligned Sequences: " << num_aln_seqs);
	INFO("Num. Total Alignments: " << tot_alignments);
	INFO("Num. Not Aligned Sequences: " << num_missing_seqs);
    }
    //Remove tmp files
    CFile tmp_fasta_file(tmpfasta);
    if(tmp_fasta_file.Exists()) {
	tmp_fasta_file.Remove();
    }
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    IterativeAlignment app;
    return app.AppMain(argc, argv);
}

/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "iscluster_extended.hpp"
#include <fstream>

BEGIN_CLASS_INFO(ISClusterExtended) {
    ADD_STD_MEMBER(clu_id);
    ADD_STD_MEMBER(cons_seq);
    ADD_STD_MEMBER(num_aln);
    ADD_STD_MEMBER(max_aln_score);
    ADD_STD_MEMBER(seq_len);
    ADD_STD_MEMBER(clu_score);
    ADD_MEMBER(lab, STL_vector, (CLASS, (Label)));
    ADD_MEMBER(merged, STL_list, (STD, (string)));
    ADD_MEMBER(masked, STL_list, (STD, (string)));
    ADD_STD_MEMBER(weight);
    ADD_MEMBER(weight_labels, STL_list, (STD, (string)));
    ADD_STD_MEMBER(r2_weight);
    ADD_MEMBER(r2_cons_seq, STL_list, (STD, (string)));
    ADD_STD_MEMBER(tag_weight);
    ADD_MEMBER(tag_cons_seq, STL_list, (STD, (string)));
    ADD_STD_MEMBER(combo_weight);
    ADD_MEMBER(combo_cons_seq, STL_list, (STD, (string)));
}
END_CLASS_INFO;

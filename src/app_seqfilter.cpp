/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"

#include <ncbi_pch.hpp>
#include <corelib/ncbiapp.hpp>
#include <serial/serial.hpp>
#include <serial/objostr.hpp>

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

KSEQ_INIT(gzFile, gzread)

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class SeqFilter : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    // virtual void Exit();
    void createFilter(const Alignments& al,
                      std::map< string, int >& filter);
};

void SeqFilter::Init(void){
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Sequence Filter program");

    arg_desc->AddKey
        ("i", "inputfile",
         "Input FASTA file of all the sequences.",
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey
        ("aln", "alnfile",
         "BLAST alignment file in ASN format containing the reads to filter.",
          CArgDescriptions::eInputFile);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

void SeqFilter::createFilter(const Alignments& al,
                             std::map< string, int >& filter) {
    std::list< AlnData >::const_iterator it;
    int max_pos = 0;
    for (it = al.al_data.begin(); it != al.al_data.end(); ++it) {
        if (it->query_start < PCR_THR && it->query_stop > max_pos) {
            max_pos = it->query_stop;
            //std::cout << al.query_id << " " << max_pos << "\n";
        }
    }
    filter[al.query_id] = max_pos;
}

int SeqFilter::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));

    //Check input files
    CFile in_fasta_file(args["i"].AsString());
    if(!in_fasta_file.Exists()) {
        ERROR("File " << args["i"].AsString() << " not found.");
        return 1;
    }

    auto_ptr< CObjectIStream > in_aln_stream
        (CObjectIStream::Open(ASN_TYPE, args["aln"].AsString()));
    if(!in_aln_stream->InGoodState()) {
        ERROR("Problem in opening " << args["aln"].AsString() << " file.");
        return 1;
    }

    //Check output file
    string outfasta = in_fasta_file.GetDir() + in_fasta_file.GetBase() + ".filt.fa";
    ofstream out_fasta_stream(outfasta.c_str());
    if(!out_fasta_stream.is_open()) {
        ERROR("Problem in creating " << outfasta << " file.");
        return 1;
    }

    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input sequence file: " << args["i"].AsString());
    INFO("Input aln filter file: " << args["aln"].AsString());
    INFO("Output FASTA file: " << outfasta);

    //Create initial filter
    std::map< string, int > filter;
    while (!in_aln_stream->EndOfData()) {
        Alignments al;
        *in_aln_stream >> al;
        createFilter(al, filter);
    }

    //Read Fasta sequences
    int num_seq = 0;
    int trim_seq = 0;
    int del_seq = 0;
    int init_seq = 0;
    {{
            gzFile fp = gzopen(args["i"].AsString().c_str(), "r");
            kseq_t* seq = kseq_init(fp);
            while ((kseq_read(seq)) >= 0) {
                init_seq++;
                string s = seq->seq.s;
                //Trim final N
                bool trim = false;
                do{
                    trim = false;
                    if(s.length() > 0 && s.at(s.length() - 1) == 'N') {
                        s = s.substr(0, s.length() - 1);
                        trim = true;
                    }
                } while(trim);
                //Trim initial contamination
                if (s.length() > 0 && filter.find(seq->name.s) == filter.end()) {
                    num_seq++;
                    out_fasta_stream << ">" << seq->name.s << "\n";
                    out_fasta_stream << s << std::endl;
                } else {
                    if (s.length() - filter[seq->name.s] > FILTER_LEN_THR) {
                        num_seq++;
                        trim_seq++;
                        out_fasta_stream << ">" << seq->name.s << "\n";
                        out_fasta_stream << s.substr(filter[seq->name.s]) << std::endl;
                    } else {
                        del_seq++;
                    }
                }
            }
            INFO("Initial Sequences: " << init_seq);
            INFO("Sequences removed: " << del_seq);
            INFO("Sequences kept: " << num_seq);
            INFO("Sequences trimmed: " << trim_seq);
            kseq_destroy(seq);
            gzclose(fp);
    }}
    out_fasta_stream.flush();
    out_fasta_stream.close();
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    SeqFilter app;
    return app.AppMain(argc, argv);
}

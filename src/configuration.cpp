/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

char fasta_nucl[18] = {
    'A', //A
    'C', //C
    'G', //G
    'T', //T
    'U', //U
    'R', //A or G
    'Y', //C, T or U
    'K', //G, T or U
    'M', //A or C
    'S', //C or G
    'W', //A, T or U
    'B', //not A (i.e. C, G, T or U)
    'D', //not C (i.e. A, G, T or U)
    'H', //not G (i.e., A, C, T or U)
    'V', //neither T nor U (i.e. A, C or G)
    'N', //A C G T U
    'X', //masked
    '-'  //gap of indeterminate length
};

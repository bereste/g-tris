/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster_extended.hpp"
#include "uniqueseqs.hpp"
#include "consensus.hpp"

#include <queue>
#include <iterator>

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <omp.h>

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

#include "bwa/bwamem.h"

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class ComputeTAG : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    virtual void Exit();
    int createTagShearFile(std::vector< ISClusterExtended >& clu,
			   const string& subg_dir_name,
			   const string& outCluExt,
			   const UniqueSeqs& unique_seq_map,
			   const std::map< string, string >& tag_seqs,
			   const std::map< string, string >& r2_seqs,
			   const int num_threads);
    std::map< string, int > readVsClusters(const string& in_vs_clu_name,
					   std::map< string, string >& tag_clusters);
};


std::map< string, int > ComputeTAG::readVsClusters(const string& in_vs_clu_name,
						   std::map< string, string >& vs_clusters) {
    std::map< string, int > clu_cons_seqs;
    CFile in_vs_clu_file(in_vs_clu_name.c_str());
    if(!in_vs_clu_file.Exists()) {
	ERROR("File " << in_vs_clu_name.c_str() << " not found.");
	return clu_cons_seqs;
    }
    ifstream in_vs_clu_stream(in_vs_clu_name.c_str());
    if(!in_vs_clu_stream.is_open()) {
	ERROR("Error in opening " << in_vs_clu_name << " file.");
	return clu_cons_seqs;
    }
    string line;
    while(std::getline(in_vs_clu_stream, line)) {
	stringstream ss(line);
	string field;
	getline(ss, field, '\t');
	if(field != "C") {
	    for(int i = 0; i < 7; i++) {
		getline(ss, field, '\t');
	    }
	    getline(ss, field, '\t');
	    string read_id = field;
	    getline(ss, field, '\t');
	    string clu_repr = field;
	    if (clu_repr == "*") {
		clu_repr = read_id;
	    }
	    vs_clusters[read_id] = clu_repr;
	    if (clu_cons_seqs.find(clu_repr) == clu_cons_seqs.end()) {
		clu_cons_seqs[clu_repr] = 0;
	    }
	    clu_cons_seqs[clu_repr]++;
	}
    }
    in_vs_clu_stream.close();
    return clu_cons_seqs;
}

int ComputeTAG::createTagShearFile(std::vector< ISClusterExtended >& clu,
				   const string& subg_dir_name,
				   const string& outCluExt,
				   const UniqueSeqs& unique_seq_map,
				   const std::map< string, string >& tag_seqs,
				   const std::map< string, string >& r2_seqs,
				   const int num_threads) {
    std::string bin_path = "";
    char* _bin_path = readlink_malloc("/proc/self/exe");
    if (_bin_path == NULL) {
	ERROR("Impossible to determine the path of the executable. " <<
	      "Let it unspecified (i.e., search in the PATH).");
    } else {
	bin_path = _bin_path;
	free(_bin_path);
	CDirEntry cd(bin_path);
	bin_path = cd.GetDir();
	INFO("The directory of the executable is '" << bin_path << "'.");
    }
    string vs_tag_in_file = "vsearch_tag_in.fa";
    string vs_tag_out_file = "vsearch_tag_out.table";
    string vs_r2_in_file = "vsearch_r2_in.fa";
    string vs_r2_out_file = "vsearch_r2_out.table";
    string vs_opt = " -id 0.9 -maxaccepts 0 -maxrejects 0 --minseqlength 1 -threads ";
    stringstream vs_tag_cmd;
    vs_tag_cmd << bin_path
	       << "_vsearch -cluster_fast "
	       << vs_tag_in_file
	       << vs_opt
	       << num_threads
	       << " -uc "
	       << vs_tag_out_file;
    stringstream vs_r2_cmd;
    vs_r2_cmd << bin_path
	       << "_vsearch -cluster_fast "
	       << vs_r2_in_file
	       << vs_opt
	       << num_threads
	       << " -uc "
	       << vs_r2_out_file;
    // Clu Extended (serialized) output
    auto_ptr<CObjectOStream>
	out_clu_stream(CObjectOStream::Open(CLU_TYPE, outCluExt));
    if(!out_clu_stream->InGoodState()) {
	ERROR("Error in creating " << outCluExt << " file.");
	return 1;
    }
    // Parse clusters
    for(unsigned int i = 0; i < clu.size(); ++i) {
	int w  = clu[i].weight;
	int check_w = 0;
	std::list< string >::iterator merge_it;
	// TAG
	std::ofstream vs_tag_in_stream(vs_tag_in_file.c_str(),
				       std::ios_base::binary);
	if(!vs_tag_in_stream.is_open()) {
	    ERROR("Error in creating " << vs_tag_in_file << " file.");
	    continue;
	}
	// R2
	std::ofstream vs_r2_in_stream(vs_r2_in_file.c_str(),
				      std::ios_base::binary);
	if(!vs_r2_in_stream.is_open()) {
	    ERROR("Error in creating " << vs_r2_in_file << " file.");
	    continue;
	}
	for(merge_it = clu[i].merged.begin();
	    merge_it != clu[i].merged.end(); merge_it++) {
	    string fs = subg_dir_name + "/" + *merge_it + ".fa";
	    gzFile fp = gzopen(fs.c_str(), "r");
	    kseq_t* seq = kseq_init(fp);
	    while ((kseq_read(seq)) >= 0) {
		string un_seq = seq->name.s;
		check_w += unique_seq_map.fasta_id_map.at(un_seq).size() + 1;
		if(tag_seqs.find(un_seq) == tag_seqs.end()) {
		    WARN("Tag read id not found for " << un_seq);
		} else {
		    vs_tag_in_stream << ">" << un_seq << "\n"
				     << tag_seqs.at(un_seq) << "\n";
		}
		if(r2_seqs.find(un_seq) == r2_seqs.end()) {
		    WARN("R2 read id not found for " << un_seq);
		} else {
		    vs_r2_in_stream << ">" << un_seq << "\n"
				    << r2_seqs.at(un_seq) << "\n";
		}
		clu[i].weight_labels.push_back(un_seq);
		std::list< string >::const_iterator us_it;
		for(us_it = unique_seq_map.fasta_id_map.at(un_seq).begin();
		    us_it != unique_seq_map.fasta_id_map.at(un_seq).end();
		    us_it++) {
		    if(tag_seqs.find(*us_it) == tag_seqs.end()) {
			WARN("Tag read id not found for " << *us_it);
		    } else {
			vs_tag_in_stream << ">" << *us_it << "\n"
					 << tag_seqs.at(*us_it) << "\n";
		    }
		    if(r2_seqs.find(*us_it) == r2_seqs.end()) {
			WARN("R2 read id not found for " << *us_it);
		    } else {
			vs_r2_in_stream << ">" << *us_it << "\n"
					<< r2_seqs.at(*us_it) << "\n";
		    }
		    clu[i].weight_labels.push_back(*us_it);
		}
	    }
	    kseq_destroy(seq);
	    gzclose(fp);
	}
	vs_tag_in_stream.flush();
	vs_tag_in_stream.close();
	vs_r2_in_stream.flush();
	vs_r2_in_stream.close();
	if(w != check_w) {
	    ERROR("Wrong original count for cluster " << clu[i].clu_id);
	    return 1;
	}
	// Compute TAG clustering with vsearch
	string vs_tag_call(vs_tag_cmd.str());
	INFO(vs_tag_call);
	if (system(vs_tag_call.c_str())) {
	    ERROR("vsearch invocation error on TAGs.");
	    return 1;
	}
	// Read TAG clustering file
	INFO("Cluster: " << clu[i].clu_id);
	INFO("Number of sequences: " << w);
	std::map< string, string > tag_clusters;
        std::map< string, int > tag_clu_rep = readVsClusters(vs_tag_out_file, tag_clusters);
	if(tag_clu_rep.size() <= 0) {
	    ERROR("Error in reading vsearch results for " << clu[i].clu_id);
	    return 1;
	}
	INFO("Number of TAG clusters: " << tag_clu_rep.size());
	clu[i].tag_weight = tag_clu_rep.size();
	for(std::map< string, int >:: iterator tag_clu_rep_it = tag_clu_rep.begin();
	    tag_clu_rep_it != tag_clu_rep.end(); ++tag_clu_rep_it) {
	    if (tag_seqs.find(tag_clu_rep_it->first) == tag_seqs.end()) {
		WARN("Tag read sequence not found for " << tag_clu_rep_it->first);
	    } else {
		stringstream ss;
		ss << tag_clu_rep_it->second;
		clu[i].tag_cons_seq.push_back(tag_seqs.at(tag_clu_rep_it->first) + ":" + ss.str());
	    }
	}

	// Compute R2 clustering with vsearch
	string vs_r2_call(vs_r2_cmd.str());
	INFO(vs_r2_call);
	if (system(vs_r2_call.c_str())) {
	    ERROR("vsearch invocation error on R2 seqs.");
	    return 1;
	}
	// Read R2 clustering file
	std::map< string, string > r2_clusters;
	std::map< string, int > r2_clu_rep = readVsClusters(vs_r2_out_file, r2_clusters);
	if(r2_clu_rep.size() <= 0) {
	    ERROR("Error in reading vsearch results for " << clu[i].clu_id);
	    return 1;
	}
	INFO("Number of shear clusters: " << r2_clu_rep.size());
	clu[i].r2_weight = r2_clu_rep.size();
	for(std::map< string, int >:: iterator r2_clu_rep_it = r2_clu_rep.begin();
	    r2_clu_rep_it != r2_clu_rep.end(); ++r2_clu_rep_it) {
	    if (r2_seqs.find(r2_clu_rep_it->first) == r2_seqs.end()) {
		WARN("R2 read sequence not found for " << r2_clu_rep_it->first);
	    } else {
		stringstream ss;
		ss << r2_clu_rep_it->second;
		clu[i].r2_cons_seq.push_back(r2_seqs.at(r2_clu_rep_it->first) + ":" + ss.str());
	    }
	}

	// Combo
	std::map< std::pair< string, string >, int > combo;
	for(std::map< string, string >:: iterator r2_it = r2_clusters.begin();
	    r2_it != r2_clusters.end(); ++r2_it) {
	    if (tag_clusters.find(r2_it->first) != tag_clusters.end()) {
		std::pair< string, string > r2_tag = make_pair(r2_it->second, tag_clusters[r2_it->first]);
		if (combo.find(r2_tag) == combo.end()) {
		    combo[r2_tag] = 0;
		}
		combo[r2_tag]++;
	    }
	}
	for(std::map< std::pair< string, string >, int >::iterator c_it = combo.begin();
	    c_it != combo.end(); ++c_it) {
	    if (r2_seqs.find(c_it->first.first) == r2_seqs.end()) {
		WARN("R2 read sequence not found for " << c_it->first.first);
	    } else {
		if (tag_seqs.find(c_it->first.second) == tag_seqs.end()) {
		    WARN("Tag read sequence not found for " << c_it->first.second);
		} else {
		    string combo_repr = r2_seqs.at(c_it->first.first) + "_" + tag_seqs.at(c_it->first.second);
		    stringstream ss;
		    ss << c_it->second;
		    clu[i].combo_cons_seq.push_back(combo_repr + ":" + ss.str());
		}
	    }
	}
	INFO("Number of combo (tag + shear): " << combo.size());
	clu[i].combo_weight = combo.size();
	*out_clu_stream << clu[i];
    }
    return 0;
}


void ComputeTAG::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
			      "Compute TAG program");
    arg_desc->AddKey
	("i", "inputClusterFile",
	 "Input file of serialized clsuters (.clu). ",
	 CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);
    arg_desc->AddKey
	("t", "inputTagFile",
	 "Input file containing the TAG sequences. ",
	 CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);
    arg_desc->AddKey
	("r", "r2FastaFile",
	 "Input R2 file of paired reads in FASTA format.",
	 CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);
    arg_desc->AddKey
	("m", "mapFile",
	 "File containing the map of unique sequences " \
	 "obtained with option '-uniqueseqs' of "       \
	 "makeblastdb program.",
	 CArgDescriptions::eInputFile);
    arg_desc->AddDefaultKey
	("num_threads", "num_threads",
	 "Number of threads used by BWA. Default: 1.",
	 CArgDescriptions::eInteger, BLAST_THREADS);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

int ComputeTAG::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));
    //Check input files
    CFile in_clu_file(args["i"].AsString());
    if(!in_clu_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }
    string subg_dir_name(in_clu_file.GetDir() + "subgraphs");
    CDir subg_dir(subg_dir_name);
    if(!subg_dir.Exists()) {
	ERROR("Cannot find subgraph directory: " << subg_dir_name << ".");
	return 1;
    }
    CFile in_tag_file(args["t"].AsString());
    if(!in_tag_file.Exists()) {
	ERROR("File " << args["t"].AsString() << " not found.");
	return 1;
    }
    CFile in_r2_file(args["r"].AsString());
    if(!in_r2_file.Exists()) {
	ERROR("File " << args["r"].AsString() << " not found.");
	return 1;
    }

    // Output file name
    string outCluExt = in_clu_file.GetDir() + in_clu_file.GetBase() + ".clu_extended";

    int num_threads = 1;
#ifdef ENABLE_OPENMP
    if (args["num_threads"].AsInteger() > 1) {
	if (args["num_threads"].AsInteger() < omp_get_num_procs()) {
	    omp_set_num_threads(args["num_threads"].AsInteger());
	    num_threads = args["num_threads"].AsInteger();
	} else {
	    omp_set_num_threads(omp_get_num_procs());
	    num_threads = omp_get_num_procs();
	}
    }
#endif

    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input CLU file: " << args["i"].AsString());
    // INFO("Input DB file: " << args["db"].AsString());
    INFO("Input TAG file: " << args["t"].AsString());
    INFO("Input R2 file: " << args["r"].AsString());
    if(args["m"].HasValue()) {
	INFO("Input Map file: " << args["m"].AsString());
    }
    INFO("Input subgraphs dir: " << subg_dir_name);
    INFO("Num. Threads: " << num_threads);
    INFO("Output CLU Extended file: " << outCluExt);
    INFO("Started reading input files.");
    //Read clusters
    std::vector< ISClusterExtended > clu;
    {
	auto_ptr< CObjectIStream > in_clu_stream
	    (CObjectIStream::Open(CLU_TYPE, args["i"].AsString()));
	if(!in_clu_stream->InGoodState()) {
	    ERROR("Error in opening " << args["i"].AsString() << " file.");
	    return 1;
	}
	while (!in_clu_stream->EndOfData()) {
	    ISCluster c;
	    *in_clu_stream >> c;
	    clu.push_back(ISClusterExtended(c));
	}
    }
    INFO("Num. Read Clusters: " << clu.size());

    //Read serialized FASTA Id Map
    UniqueSeqs unique_seq_map;
    if (args["m"].HasValue()) {
	auto_ptr< CObjectIStream > in_map_stream
	    (CObjectIStream::Open(ASN_TYPE, args["m"].AsString()));
	if(!in_map_stream->InGoodState()) {
	    ERROR("Error in opening " << args["m"].AsString() << " file.");
	    return 1;
	}
	*in_map_stream >> unique_seq_map;
    } else {
	for(unsigned int i = 0; i < clu.size(); ++i) {
	    std::list< string >::iterator merge_it;
	    for(merge_it = clu[i].merged.begin();
		merge_it != clu[i].merged.end(); merge_it++) {
		string fs = subg_dir_name + "/" + *merge_it + ".fa";
		gzFile fp = gzopen(fs.c_str(), "r");
		kseq_t* seq = kseq_init(fp);
		while ((kseq_read(seq)) >= 0) {
		    unique_seq_map.fasta_id_map[seq->name.s];
		}
		kseq_destroy(seq);
		gzclose(fp);
	    }
	}
    }
    INFO("Finished Reading FASTA Id Map");
    //Read TAG clusters
    std::map< string, string > tag_seqs;
    {
	gzFile fp = gzopen(args["t"].AsString().c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	while ((kseq_read(seq)) >= 0) {
	    string s = seq->seq.s;
	    string id = seq->name.s;
	    tag_seqs[id] = s;
	}
	kseq_destroy(seq);
	gzclose(fp);
    }
    INFO("Finished Reading TAG clusters");
    //Read R2 clusters
    std::map< string, string > r2_seqs;
    double mean = 0;
    unsigned int min = 200;
    unsigned int max = 0;
    int c = 0;
    {
	gzFile fp = gzopen(args["r"].AsString().c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	while ((kseq_read(seq)) >= 0) {
	    string s = seq->seq.s;
	    string id = seq->name.s;
	    if(s.length() > 20) {
		r2_seqs[id] = s.substr(20, 30);
	    } else {
		r2_seqs[id] = s;
	    }
	    c++;
	    if(s.length() < min) {
		min = s.length();
	    }
	    if(s.length() > max) {
		max = s.length();
	    }
	    mean += s.length();
	}
	kseq_destroy(seq);
	gzclose(fp);
    }
    INFO("Finished reading input files.");
    INFO("Min: " << min);
    INFO("Max: " << max);
    INFO("Mean: " << mean/c);
    //TAG and Shear site count
    INFO("Started creating TAG and Shear site files.");
    if(createTagShearFile(clu, subg_dir_name,
			  outCluExt,
			  unique_seq_map,
			  tag_seqs,
			  r2_seqs,
			  num_threads) != 0) {
	ERROR("Error in creating TAG and Shear site files.");
	return 1;
    }
    INFO("Finished creating TAG and Shear site files.");
    return 0;
}

void ComputeTAG::Exit(){

}

int main(int argc, char* argv[]){
    INFO("Program started");
    ComputeTAG app;
    return app.AppMain(argc, argv);
}

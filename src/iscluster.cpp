/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "iscluster.hpp"
#include "consensus.hpp"
#include <fstream>

BEGIN_CLASS_INFO(Label) {
    ADD_STD_MEMBER(target_id);
    ADD_STD_MEMBER(centroid);
    ADD_STD_MEMBER(offset);
    ADD_STD_MEMBER(aln_score);
    ADD_MEMBER(seq_id, STL_list, (STD, (string)));
}
END_CLASS_INFO;

BEGIN_CLASS_INFO(ISCluster) {
    ADD_STD_MEMBER(clu_id);
    ADD_STD_MEMBER(cons_seq);
    ADD_STD_MEMBER(num_aln);
    ADD_STD_MEMBER(max_aln_score);
    ADD_STD_MEMBER(seq_len);
    ADD_STD_MEMBER(weight);
    ADD_STD_MEMBER(clu_score);
    ADD_MEMBER(lab, STL_vector, (CLASS, (Label)));
    ADD_MEMBER(merged, STL_list, (STD, (string)));
    ADD_MEMBER(masked, STL_list, (STD, (string)));
}
END_CLASS_INFO;

ostream& operator<<(ostream& os, const ISCluster& cluster) {
    std::vector< Label >::const_iterator it;
    for (it = cluster.lab.begin(); it != cluster.lab.end(); ++it) {
        os << it->target_id << "\t"
           << std::fixed << std::setprecision(0) << it->centroid
           << "\t"
           << std::fixed << std::setprecision(0) << (it->centroid)+1
           << "\t"
           << cluster.clu_id << "\n";
    }
    return os;
}

std::ofstream SingleClusterOperations::null_stream;

bool SingleClusterOperations::checkSingleClusters_old(const ISCluster& c1,
						      const ISCluster& c2) {
    int common = 0;
    std::vector< bool > lab1_found(c1.lab.size(), false);
    std::vector< bool > lab2_found(c2.lab.size(), false);
    for (unsigned int lab1 = 0; lab1 < c1.lab.size(); ++lab1) {
	for (unsigned int lab2 = 0; lab2 < c2.lab.size(); ++lab2) {
	    if (c1.lab[lab1].target_id == c2.lab[lab2].target_id &&
		abs(c1.lab[lab1].centroid - c2.lab[lab2].centroid) < PCR_THR) {
		double s1l = min(c1.lab[lab1].centroid,
				 c1.lab[lab1].centroid + c1.lab[lab1].offset);
		double s1r = max(c1.lab[lab1].centroid,
				 c1.lab[lab1].centroid + c1.lab[lab1].offset);
		double s2l = min(c2.lab[lab2].centroid,
				 c2.lab[lab2].centroid + c2.lab[lab2].offset);
		double s2r = max(c2.lab[lab2].centroid,
				 c2.lab[lab2].centroid + c2.lab[lab2].offset);
		//double L1 = s1l;
		double R1 = s1r;
		double L2 = s2l;
		double R2 = s2r;
		if (s1l >= s2l) {
		    //L1 = s2l;
		    R1 = s2r;
		    L2 = s1l;
		    R2 = s1r;
		}
		double ov = min(max(0.0, R1-L2), R2-L2);
		if (ov > PCR_THR) {
		    ++common;
		    lab1_found[lab1] = true;
		    lab2_found[lab2] = true;
		}
	    }
	}
    }
    if(common > 0) {
	INFO(c1.clu_id << "(" << c1.lab.size() << ") - " <<
	     c2.clu_id << "(" << c2.lab.size() << ") -> common: " <<
	     common);
    }
    //Add labels of c1 not in common
    bool c1_only_max = true;
    for (unsigned int pos1 = 0; pos1 < lab1_found.size(); ++pos1) {
	if (lab1_found[pos1]) {
	    //Check if the common labels have all max_aln_score or not
	    if (c1.lab[pos1].aln_score != c1.max_aln_score) {
		c1_only_max = false;
	    }
	}
    }
    //Add labels of c2 not in common
    bool c2_only_max = true;
    for (unsigned int pos2 = 0; pos2 < lab2_found.size(); ++pos2) {
	if (lab2_found[pos2]) {
	    //Check if the common labels have all max_aln_score or not
	    if (c2.lab[pos2].aln_score != c2.max_aln_score) {
		c2_only_max = false;
	    }
	}
    }
    //Merge c1 and c2 if they share COM_THR % of the labels
    if (common > COM_THR * c1.lab.size() && common > COM_THR * c2.lab.size()) {
	INFO("Clusters " << c1.clu_id << " and " << c2.clu_id << " are similar.");
	return true;
    }
    //Merge c1 and c2 if they share masked label and COM_THR/2 % of the labels
    if (!c1.masked.empty() && !c2.masked.empty()) {
	for (std::list< string >::const_iterator m1_it = c1.masked.begin();
	     m1_it != c1.masked.end(); ++m1_it) {
	    for (std::list< string >::const_iterator m2_it = c2.masked.begin();
		 m2_it != c2.masked.end(); ++m2_it) {
		if (*m1_it == *m2_it &&
		    common > (COM_THR / 2) * c1.lab.size() &&
		    common > (COM_THR / 2) * c2.lab.size()) {
		    INFO("Clusters " << c1.clu_id
			 << " and " << c2.clu_id
			 << " are similar by mask labels.");
		    return true;
		}
	    }
	}
    }
    //Merge c1 and c2 if they shere only max_score alignments
    if (common > 0 && c1_only_max && c2_only_max &&
	c1.lab.size() < MAX_VALID_ALN / 20 &&
	c2.lab.size() < MAX_VALID_ALN / 20) {
	INFO("Clusters " << c1.clu_id << " and " << c2.clu_id
	     << " are similar by max_score.");
	return true;
    }
    return false;
}

bool SingleClusterOperations::mergeSingleClusters_old(ISCluster& c1,
						      ISCluster& c2) {
    int common = 0;
    std::vector< Label > new_labs;
    std::vector< bool > lab1_found(c1.lab.size(), false);
    std::vector< bool > lab2_found(c2.lab.size(), false);
    std::set< pair< string, string > > dot_edges;
    for (unsigned int lab1 = 0; lab1 < c1.lab.size(); ++lab1) {
	for (unsigned int lab2 = 0; lab2 < c2.lab.size(); ++lab2) {
	    if (c1.lab[lab1].target_id == c2.lab[lab2].target_id &&
		abs(c1.lab[lab1].centroid - c2.lab[lab2].centroid) < PCR_THR) {
		double s1l = min(c1.lab[lab1].centroid,
				 c1.lab[lab1].centroid + c1.lab[lab1].offset);
		double s1r = max(c1.lab[lab1].centroid,
				 c1.lab[lab1].centroid + c1.lab[lab1].offset);
		double s2l = min(c2.lab[lab2].centroid,
				 c2.lab[lab2].centroid + c2.lab[lab2].offset);
		double s2r = max(c2.lab[lab2].centroid,
				 c2.lab[lab2].centroid + c2.lab[lab2].offset);
		//double L1 = s1l;
		double R1 = s1r;
		double L2 = s2l;
		double R2 = s2r;
		if (s1l >= s2l) {
		    //L1 = s2l;
		    R1 = s2r;
		    L2 = s1l;
		    R2 = s1r;
		}
		double ov = min(max(0.0, R1-L2), R2-L2);
		if (ov > PCR_THR) {
		    ++common;
		    lab1_found[lab1] = true;
		    lab2_found[lab2] = true;
		    double new_cent =
			((c1.lab[lab1].centroid * c1.weight) +
			 (c2.lab[lab2].centroid * c2.weight)) /
			(c1.weight + c2.weight);
		    //Add new edges to dot graph
		    std::list< string >::iterator it1;
		    for(it1 = c1.lab[lab1].seq_id.begin();
			it1 != c1.lab[lab1].seq_id.end(); ++it1) {
			std::list< string >::iterator it2;
			for(it2 = c2.lab[lab2].seq_id.begin();
			    it2 != c2.lab[lab2].seq_id.end(); ++it2) {
			    dot_edges.insert(make_pair(*it1, *it2));
			    //dot_edges.insert(make_pair(string(min(*it1,*it2)),
			    //                          string(max(*it1,*it2))));
			}
		    }
		    std::list< string > new_seq_id(c1.lab[lab1].seq_id);
		    new_seq_id.splice(new_seq_id.end(), c2.lab[lab2].seq_id);
		    if (c1.lab[lab1].offset > 0 && c2.lab[lab2].offset > 0) {
			new_labs.push_back(Label(c1.lab[lab1].target_id,
						 new_cent,
						 max(c1.lab[lab1].offset,
						     c2.lab[lab2].offset),
						 max(c1.lab[lab1].aln_score,
						     c2.lab[lab2].aln_score),
						 new_seq_id));
		    } else {
			if (c1.lab[lab1].offset < 0 && c2.lab[lab2].offset < 0) {
			    new_labs.push_back(Label(c1.lab[lab1].target_id,
						     new_cent,
						     min(c1.lab[lab1].offset,
							 c2.lab[lab2].offset),
						     max(c1.lab[lab1].aln_score,
							 c2.lab[lab2].aln_score),
						     new_seq_id));
			} else {
			    WARN("Discordant strand alignments.");
			    new_labs.push_back(c1.lab[lab1]);
			    new_labs.push_back(c2.lab[lab2]);
			}
		    }
		}
	    }
	}
    }
    if(common > 0) {
	INFO(c1.clu_id << "(" << c1.lab.size() << ") - " <<
	     c2.clu_id << "(" << c2.lab.size() << ") -> common: " <<
	     common);
    }
    //Add labels of c1 not in common
    bool c1_only_max = true;
    for (unsigned int pos1 = 0; pos1 < lab1_found.size(); ++pos1) {
	if (!lab1_found[pos1]) {
	    new_labs.push_back(c1.lab[pos1]);
	} else {
	    //Check if the common labels have all max_aln_score or not
	    if (c1.lab[pos1].aln_score != c1.max_aln_score) {
		c1_only_max = false;
	    }
	}
    }
    //Add labels of c2 not in common
    bool c2_only_max = true;
    for (unsigned int pos2 = 0; pos2 < lab2_found.size(); ++pos2) {
	if (!lab2_found[pos2]) {
	    new_labs.push_back(c2.lab[pos2]);
	} else {
	    //Check if the common labels have all max_aln_score or not
	    if (c2.lab[pos2].aln_score != c2.max_aln_score) {
		c2_only_max = false;
	    }
	}
    }
    //Merge c1 and c2 if they share COM_THR % of the labels
    if (common > COM_THR * c1.lab.size() && common > COM_THR * c2.lab.size()) {
	// for(std::list< string >::iterator merge_it1 = c1.merged.begin();
	//     merge_it1 != c1.merged.end(); ++merge_it1) {
	//     for(std::list< string >::iterator merge_it2 = c2.merged.begin();
	//         merge_it2 != c2.merged.end(); ++merge_it2) {
	//       out_dot << *merge_it1
	//               << " -- "
	//               << *merge_it2
	//               << " [color=black];\n";
	//     }
	// }

	std::set< pair< string, string > >::iterator e_it;
	for(e_it = dot_edges.begin(); e_it != dot_edges.end(); ++e_it) {
	    out_dot_stream << e_it->first
			   << " -- "
			   << e_it->second
			   << " [color=black];\n";
	}
	c1.merged.splice(c1.merged.end(), c2.merged);
	c1.masked.splice(c1.masked.end(), c2.masked);
	c1.lab.clear();
	c1.lab = new_labs;
	c1.num_aln = c1.lab.size();
	c1.weight += c2.weight;
	if (c2.max_aln_score > c1.max_aln_score) {
	    c1.max_aln_score = c2.max_aln_score;
	}
	INFO("Merging " << c1.clu_id << " with " << c2.clu_id);
	return true;
    }
    //Merge c1 and c2 if they share masked label and COM_THR/2 % of the labels
    if (!c1.masked.empty() && !c2.masked.empty()) {
	for (std::list< string >::const_iterator m1_it = c1.masked.begin();
	     m1_it != c1.masked.end(); ++m1_it) {
	    for (std::list< string >::const_iterator m2_it = c2.masked.begin();
		 m2_it != c2.masked.end(); ++m2_it) {
		if (*m1_it == *m2_it &&
		    common > (COM_THR / 2) * c1.lab.size() &&
		    common > (COM_THR / 2) * c2.lab.size()) {
		    std::set< pair< string, string > >::iterator e_it;
		    for(e_it = dot_edges.begin(); e_it != dot_edges.end(); ++e_it) {
			out_dot_stream << e_it->first
				       << " -- "
				       << e_it->second
				       << " [color=red, penwidth=4];\n";
		    }
		    c1.merged.splice(c1.merged.end(), c2.merged);
		    c1.masked.splice(c1.masked.end(), c2.masked);
		    c1.lab.clear();
		    c1.lab = new_labs;
		    c1.num_aln = c1.lab.size();
		    c1.weight += c2.weight;
		    if (c2.max_aln_score > c1.max_aln_score) {
			c1.max_aln_score = c2.max_aln_score;
		    }
		    INFO("Merging (mask) " << c1.clu_id
			 << " with " << c2.clu_id);
		    return true;
		}
	    }
	}
    }
    //Merge c1 and c2 if they shere only max_score alignments
    if (common > 0 && c1_only_max && c2_only_max &&
	c1.lab.size() < MAX_VALID_ALN / 20 &&
	c2.lab.size() < MAX_VALID_ALN / 20) {
	std::set< pair< string, string > >::iterator e_it;
	for(e_it = dot_edges.begin(); e_it != dot_edges.end(); ++e_it) {
	    out_dot_stream << e_it->first
			   << " -- "
			   << e_it->second
			   << " [color=green, penwidth=4];\n";
	}
	c1.merged.splice(c1.merged.end(), c2.merged);
	c1.masked.splice(c1.masked.end(), c2.masked);
	c1.lab.clear();
	c1.lab = new_labs;
	c1.num_aln = c1.lab.size();
	c1.weight += c2.weight;
	if (c2.max_aln_score > c1.max_aln_score) {
	    c1.max_aln_score = c2.max_aln_score;
	}
	INFO("Merging (max_score) " << c1.clu_id << " with " << c2.clu_id);
	return true;
    }
    return false;
}


bool SingleClusterOperations::checkSingleClusters(const ISCluster& c1,
						  const ISCluster& c2) {
    int common = 0;
    std::vector< bool > lab1_found(c1.lab.size(), false);
    // Vector of multimap: alignment distance, alignment label
    std::vector< std::multimap< unsigned int, unsigned int > > lab1_adj(c1.lab.size());
    std::vector< bool > lab2_found(c2.lab.size(), false);
    // Vector of multimap: alignment distance, alignment label
    std::vector< std::multimap< unsigned int, unsigned int > > lab2_adj(c2.lab.size());
    for (unsigned int lab1 = 0; lab1 < c1.lab.size(); ++lab1) {
	for (unsigned int lab2 = 0; lab2 < c2.lab.size(); ++lab2) {
	    int l_dist = abs(c1.lab[lab1].centroid - c2.lab[lab2].centroid);
	    if (c1.lab[lab1].target_id == c2.lab[lab2].target_id &&
		l_dist < PCR_THR) {
		if(l_dist == 0 && !lab1_found[lab1] && !lab2_found[lab2]) {
		    if((c1.lab[lab1].offset > 0 && c2.lab[lab2].offset > 0) ||
		       (c1.lab[lab1].offset < 0 && c2.lab[lab2].offset < 0)) {
			processCommonLabels(lab1_adj, lab1, lab2_adj, lab2);
			lab1_found[lab1] = true;
			lab2_found[lab2] = true;
			common++;
			continue;
		    } else {
			WARN("Discordant strand alignments.");
		    }
		}
		if(lab1_found[lab1]) continue;
		
		double s1l = min(c1.lab[lab1].centroid,
				 c1.lab[lab1].centroid + c1.lab[lab1].offset);
		double s1r = max(c1.lab[lab1].centroid,
				 c1.lab[lab1].centroid + c1.lab[lab1].offset);
		double s2l = min(c2.lab[lab2].centroid,
				 c2.lab[lab2].centroid + c2.lab[lab2].offset);
		double s2r = max(c2.lab[lab2].centroid,
				 c2.lab[lab2].centroid + c2.lab[lab2].offset);
		//double L1 = s1l;
		double R1 = s1r;
		double L2 = s2l;
		double R2 = s2r;
		if (s1l >= s2l) {
		    //L1 = s2l;
		    R1 = s2r;
		    L2 = s1l;
		    R2 = s1r;
		}
		double ov = min(max(0.0, R1-L2), R2-L2);		
		if(ov > PCR_THR && !lab1_found[lab1] && !lab2_found[lab2]) {
		    if((c1.lab[lab1].offset > 0 && c2.lab[lab2].offset > 0) ||
			(c1.lab[lab1].offset < 0 && c2.lab[lab2].offset < 0)) {
			lab1_adj[lab1].insert(make_pair(l_dist, lab2));
			lab2_adj[lab2].insert(make_pair(l_dist, lab1));
		    } else {
			WARN("Discordant strand alignments.");
		    }
		}
	    }
	}
    }

    bool operations = false;
    do {
	operations = false;
	for(unsigned int i = 0; i < lab1_adj.size(); ++i) {
	    if(lab1_adj[i].empty()) {
		continue;
	    }
	    int l2_first = lab1_adj[i].begin()->second;
	    if(lab2_adj[l2_first].begin()->second == i) {
		operations = true;
		processCommonLabels(lab1_adj, i, lab2_adj, l2_first);
		lab1_found[i] = true;
		lab2_found[l2_first] = true;
		common++;
		continue;
	    }
	}
    } while(operations);

    if(common > 0) {
	INFO(c1.clu_id << "(" << c1.lab.size() << ") - " <<
	     c2.clu_id << "(" << c2.lab.size() << ") -> common: " <<
	     common);
    }
    //Add labels of c1 not in common
    bool c1_only_max = true;
    for (unsigned int pos1 = 0; pos1 < lab1_found.size(); ++pos1) {
	if (lab1_found[pos1]) {
	    //Check if the common labels have all max_aln_score or not
	    if (c1.lab[pos1].aln_score != c1.max_aln_score) {
		c1_only_max = false;
	    }
	}
    }
    //Add labels of c2 not in common
    bool c2_only_max = true;
    for (unsigned int pos2 = 0; pos2 < lab2_found.size(); ++pos2) {
	if (lab2_found[pos2]) {
	    //Check if the common labels have all max_aln_score or not
	    if (c2.lab[pos2].aln_score != c2.max_aln_score) {
		c2_only_max = false;
	    }
	}
    }
    //Merge c1 and c2 if they share COM_THR % of the labels
    if (common > COM_THR * c1.lab.size() && common > COM_THR * c2.lab.size()) {
	INFO("Clusters " << c1.clu_id << " and " << c2.clu_id << " are similar.");
	return true;
    }
    //Merge c1 and c2 if they share masked label and COM_THR/2 % of the labels
    if (!c1.masked.empty() && !c2.masked.empty()) {
	for (std::list< string >::const_iterator m1_it = c1.masked.begin();
	     m1_it != c1.masked.end(); ++m1_it) {
	    for (std::list< string >::const_iterator m2_it = c2.masked.begin();
		 m2_it != c2.masked.end(); ++m2_it) {
		if (*m1_it == *m2_it && common > 0) {
		    // common > (COM_THR / 2) * c1.lab.size() &&
		    // common > (COM_THR / 2) * c2.lab.size()) {
		    INFO("Clusters " << c1.clu_id
			 << " and " << c2.clu_id
			 << " are similar by mask labels.");
		    return true;
		}
	    }
	}
    }
    //Merge c1 and c2 if they shere only max_score alignments
    if (common > 0 && c1_only_max && c2_only_max &&
	c1.lab.size() < MAX_VALID_ALN / 20 &&
	c2.lab.size() < MAX_VALID_ALN / 20) {
	INFO("Clusters " << c1.clu_id << " and " << c2.clu_id
	     << " are similar by max_score.");
	return true;
    }
    return false;
}

void SingleClusterOperations::
processCommonLabels(std::vector< std::multimap< unsigned int, unsigned int > >& lab1_adj,
		    unsigned int lab1_entry,
		    std::vector< std::multimap< unsigned int, unsigned int > >& lab2_adj,
		    unsigned int lab2_entry) {
    std::multimap< unsigned int, unsigned int >::iterator mm_it1;
    std::multimap< unsigned int, unsigned int >::iterator mm_it2;
    // Remove from first vector
    for(mm_it1 = lab1_adj[lab1_entry].begin();
	mm_it1 != lab1_adj[lab1_entry].end(); ++mm_it1) {
	for(mm_it2 = lab2_adj[mm_it1->second].begin();
	    mm_it2 != lab2_adj[mm_it1->second].end(); ++mm_it2) {
	    if(mm_it2->second == lab1_entry) {
		lab2_adj[mm_it1->second].erase(mm_it2);
		continue;
	    }	
	}
    }
    // Remove from second vector
    for(mm_it2 = lab2_adj[lab2_entry].begin();
	mm_it2 != lab2_adj[lab2_entry].end(); ++mm_it2) {
	for(mm_it1 = lab1_adj[mm_it2->second].begin();
	    mm_it1 != lab1_adj[mm_it2->second].end(); ++mm_it1) {
	    if(mm_it1->second == lab2_entry) {
		lab1_adj[mm_it2->second].erase(mm_it1);
		continue;
	    }	
	}
    }
    lab1_adj[lab1_entry].clear();
    lab2_adj[lab2_entry].clear();
}

void SingleClusterOperations::createNewLabel(std::set< pair< string, string > >& dot_edges,
					     std::vector< Label >& new_labs,
					     ISCluster& c1,
					     int lab1,
					     ISCluster& c2,
					     int lab2) {
    double new_cent =
	((c1.lab[lab1].centroid * c1.weight) +
	 (c2.lab[lab2].centroid * c2.weight)) /
	(c1.weight + c2.weight);
    //Add new edges to dot graph
    std::list< string >::iterator it1;
    for(it1 = c1.lab[lab1].seq_id.begin();
	it1 != c1.lab[lab1].seq_id.end(); ++it1) {
	std::list< string >::iterator it2;
	for(it2 = c2.lab[lab2].seq_id.begin();
	    it2 != c2.lab[lab2].seq_id.end(); ++it2) {
	    dot_edges.insert(make_pair(*it1, *it2));
	    //dot_edges.insert(make_pair(string(min(*it1,*it2)),
	    //                          string(max(*it1,*it2))));
	}
    }
    std::list< string > new_seq_id(c1.lab[lab1].seq_id);
    new_seq_id.splice(new_seq_id.end(), c2.lab[lab2].seq_id);
    if (c1.lab[lab1].offset > 0 && c2.lab[lab2].offset > 0) {
	new_labs.push_back(Label(c1.lab[lab1].target_id,
				 new_cent,
				 min(c1.lab[lab1].offset,
				     c2.lab[lab2].offset),
				 max(c1.lab[lab1].aln_score,
				     c2.lab[lab2].aln_score),
				 new_seq_id));
    } else {
	if (c1.lab[lab1].offset < 0 && c2.lab[lab2].offset < 0) {
	    new_labs.push_back(Label(c1.lab[lab1].target_id,
				     new_cent,
				     max(c1.lab[lab1].offset,
					 c2.lab[lab2].offset),
				     max(c1.lab[lab1].aln_score,
					 c2.lab[lab2].aln_score),
				     new_seq_id));
	} else {
	    WARN("Discordant strand alignments.");
	    new_labs.push_back(c1.lab[lab1]);
	    new_labs.push_back(c2.lab[lab2]);
	}
    }
}


bool SingleClusterOperations::mergeSingleClusters(ISCluster& c1,
						  ISCluster& c2) {
    int common = 0;
    std::set< pair< string, string > > dot_edges;
    std::vector< Label > new_labs;
    std::vector< bool > lab1_found(c1.lab.size(), false);
    // Vector of multimap: alignment distance, alignment label
    std::vector< std::multimap< unsigned int, unsigned int > > lab1_adj(c1.lab.size());
    std::vector< bool > lab2_found(c2.lab.size(), false);
    // Vector of multimap: alignment distance, alignment label
    std::vector< std::multimap< unsigned int, unsigned int > > lab2_adj(c2.lab.size());
    for (unsigned int lab1 = 0; lab1 < c1.lab.size(); ++lab1) {
	for (unsigned int lab2 = 0; lab2 < c2.lab.size(); ++lab2) {
	    int l_dist = abs(c1.lab[lab1].centroid - c2.lab[lab2].centroid);
	    if (c1.lab[lab1].target_id == c2.lab[lab2].target_id &&
		l_dist < PCR_THR) {
		if(l_dist == 0 && !lab1_found[lab1] && !lab2_found[lab2]) {
		    if((c1.lab[lab1].offset > 0 && c2.lab[lab2].offset > 0) ||
		       (c1.lab[lab1].offset < 0 && c2.lab[lab2].offset < 0)) {
			processCommonLabels(lab1_adj, lab1, lab2_adj, lab2);
			createNewLabel(dot_edges, new_labs, c1, lab1, c2, lab2);
			lab1_found[lab1] = true;
			lab2_found[lab2] = true;
			common++;
			continue;
		    } else {
			WARN("Discordant strand alignments.");
		    }
		}
		if(lab1_found[lab1]) continue;
		
		double s1l = min(c1.lab[lab1].centroid,
				 c1.lab[lab1].centroid + c1.lab[lab1].offset);
		double s1r = max(c1.lab[lab1].centroid,
				 c1.lab[lab1].centroid + c1.lab[lab1].offset);
		double s2l = min(c2.lab[lab2].centroid,
				 c2.lab[lab2].centroid + c2.lab[lab2].offset);
		double s2r = max(c2.lab[lab2].centroid,
				 c2.lab[lab2].centroid + c2.lab[lab2].offset);
		//double L1 = s1l;
		double R1 = s1r;
		double L2 = s2l;
		double R2 = s2r;
		if (s1l >= s2l) {
		    //L1 = s2l;
		    R1 = s2r;
		    L2 = s1l;
		    R2 = s1r;
		}
		double ov = min(max(0.0, R1-L2), R2-L2);
		if(ov > PCR_THR && !lab1_found[lab1] && !lab2_found[lab2]) {
		    if((c1.lab[lab1].offset > 0 && c2.lab[lab2].offset > 0) ||
			(c1.lab[lab1].offset < 0 && c2.lab[lab2].offset < 0)) {
			lab1_adj[lab1].insert(make_pair(l_dist, lab2));
			lab2_adj[lab2].insert(make_pair(l_dist, lab1));
		    } else {
			WARN("Discordant strand alignments.");
		    }
		}
	    }
	}
    }

    bool operations = false;
    do {
	operations = false;
	for(unsigned int i = 0; i < lab1_adj.size(); ++i) {
	    if(lab1_adj[i].empty()) {
		continue;
	    }
	    int l2_first = lab1_adj[i].begin()->second;
	    if(lab2_adj[l2_first].begin()->second == i) {
		operations = true;
		processCommonLabels(lab1_adj, i, lab2_adj, l2_first);
		createNewLabel(dot_edges, new_labs, c1, i, c2, l2_first);
		lab1_found[i] = true;
		lab2_found[l2_first] = true;
		common++;
		continue;
	    }
	}
    } while(operations);

    if(common > 0) {
	INFO(c1.clu_id << "(" << c1.lab.size() << ") - " <<
	     c2.clu_id << "(" << c2.lab.size() << ") -> common: " <<
	     common);
    }
    //Add labels of c1 not in common
    bool c1_only_max = true;
    for (unsigned int pos1 = 0; pos1 < lab1_found.size(); ++pos1) {
	if (!lab1_found[pos1]) {
	    new_labs.push_back(c1.lab[pos1]);
	} else {
	    //Check if the common labels have all max_aln_score or not
	    if (c1.lab[pos1].aln_score != c1.max_aln_score) {
		c1_only_max = false;
	    }
	}
    }
    //Add labels of c2 not in common
    bool c2_only_max = true;
    for (unsigned int pos2 = 0; pos2 < lab2_found.size(); ++pos2) {
	if (!lab2_found[pos2]) {
	    new_labs.push_back(c2.lab[pos2]);
	} else {
	    //Check if the common labels have all max_aln_score or not
	    if (c2.lab[pos2].aln_score != c2.max_aln_score) {
		c2_only_max = false;
	    }
	}
    }
    //Merge c1 and c2 if they share COM_THR % of the labels
    if (common > COM_THR * c1.lab.size() && common > COM_THR * c2.lab.size()) {
	// for(std::list< string >::iterator merge_it1 = c1.merged.begin();
	//     merge_it1 != c1.merged.end(); ++merge_it1) {
	//     for(std::list< string >::iterator merge_it2 = c2.merged.begin();
	//         merge_it2 != c2.merged.end(); ++merge_it2) {
	//       out_dot << *merge_it1
	//               << " -- "
	//               << *merge_it2
	//               << " [color=black];\n";
	//     }
	// }

	std::set< pair< string, string > >::iterator e_it;
	for(e_it = dot_edges.begin(); e_it != dot_edges.end(); ++e_it) {
	    out_dot_stream << e_it->first
			   << " -- "
			   << e_it->second
			   << " [color=black];\n";
	}
	c1.merged.splice(c1.merged.end(), c2.merged);
	c1.masked.splice(c1.masked.end(), c2.masked);
	c1.lab.clear();
	c1.lab = new_labs;
	c1.num_aln = c1.lab.size();
	c1.weight += c2.weight;
	if (c2.max_aln_score > c1.max_aln_score) {
	    c1.max_aln_score = c2.max_aln_score;
	}
	INFO("Merging " << c1.clu_id << " with " << c2.clu_id);
	return true;
    }
    //Merge c1 and c2 if they share masked label and COM_THR/2 % of the labels
    if (!c1.masked.empty() && !c2.masked.empty()) {
	for (std::list< string >::const_iterator m1_it = c1.masked.begin();
	     m1_it != c1.masked.end(); ++m1_it) {
	    for (std::list< string >::const_iterator m2_it = c2.masked.begin();
		 m2_it != c2.masked.end(); ++m2_it) {
		if (*m1_it == *m2_it && common > 0) {
		    // common > (COM_THR / 2) * c1.lab.size() &&
		    // common > (COM_THR / 2) * c2.lab.size()) {
		    std::set< pair< string, string > >::iterator e_it;
		    for(e_it = dot_edges.begin(); e_it != dot_edges.end(); ++e_it) {
			out_dot_stream << e_it->first
				       << " -- "
				       << e_it->second
				       << " [color=red, penwidth=4];\n";
		    }
		    c1.merged.splice(c1.merged.end(), c2.merged);
		    c1.masked.splice(c1.masked.end(), c2.masked);
		    c1.lab.clear();
		    c1.lab = new_labs;
		    c1.num_aln = c1.lab.size();
		    c1.weight += c2.weight;
		    if (c2.max_aln_score > c1.max_aln_score) {
			c1.max_aln_score = c2.max_aln_score;
		    }
		    INFO("Merging (mask) " << c1.clu_id
			 << " with " << c2.clu_id);
		    return true;
		}
	    }
	}
    }
    //Merge c1 and c2 if they shere only max_score alignments
    if (common > 0 && c1_only_max && c2_only_max &&
	c1.lab.size() < MAX_VALID_ALN / 20 &&
	c2.lab.size() < MAX_VALID_ALN / 20) {
	std::set< pair< string, string > >::iterator e_it;
	for(e_it = dot_edges.begin(); e_it != dot_edges.end(); ++e_it) {
	    out_dot_stream << e_it->first
			   << " -- "
			   << e_it->second
			   << " [color=green, penwidth=4];\n";
	}
	c1.merged.splice(c1.merged.end(), c2.merged);
	c1.masked.splice(c1.masked.end(), c2.masked);
	c1.lab.clear();
	c1.lab = new_labs;
	c1.num_aln = c1.lab.size();
	c1.weight += c2.weight;
	if (c2.max_aln_score > c1.max_aln_score) {
	    c1.max_aln_score = c2.max_aln_score;
	}
	INFO("Merging (max_score) " << c1.clu_id << " with " << c2.clu_id);
	return true;
    }
    return false;
}

int refineClusters(std::list< ISCluster >& clu,
		    ofstream& out_dot_stream) {
    INFO("Num. Initial Clusters: " << clu.size());
    int refinements = 0;
    SingleClusterOperations sclu_op(out_dot_stream);
    for(std::list< ISCluster >::iterator it1 = clu.begin();
	it1 != clu.end(); ++it1) {
	std::list< ISCluster >::iterator it2 = it1;
	++it2;
	for( ; it2 != clu.end(); ) {
	    bool merged = sclu_op.mergeSingleClusters(*it1, *it2);
	    if(merged) {
		refinements++;
		it2 = clu.erase(it2);
	    } else {
		++it2;
	    }
	}
    }
    INFO("Num. Final Clusters: " << clu.size());
    return refinements;
}

void computeSimpleClusterScore(std::list< ISCluster >& clu) {
    std::list< ISCluster >::iterator clu_it;
    for(clu_it = clu.begin(); clu_it != clu.end(); ++clu_it) {
        clu_it->clu_score = ((double)clu_it->seq_len * clu_it->weight) / clu_it->num_aln;
    }
}

int computeClusterConsensus(std::list< ISCluster >& clu,
			    std::map< string, std::list< string > >& fasta_id_map,
			    const std::map< string, string >& cons_map,
			    const string& subg_dir_name,
			    int num_threads) {
    bool no_map = false;
    if(fasta_id_map.empty()) {
        no_map = true;
    }
    //Multiple sequence alignment and consensus
    std::string bin_path = "";
    char* _bin_path = readlink_malloc("/proc/self/exe");
    if (_bin_path == NULL) {
        ERROR("Impossible to determine the path of the executable. " <<
              "Let it unspecified (i.e., search in the PATH).");
    } else {
        bin_path = _bin_path;
        free(_bin_path);
        CDirEntry cd(bin_path);
        bin_path = cd.GetDir();
        INFO("The directory of the executable is '" << bin_path << "'.");
    }
    // MultipleSequenceAligner_ClustalW msa_clustalw(bin_path);
    MultipleSequenceAligner_ClustalW_MPI msa_clustalw(bin_path,num_threads);

    //Create for each cluster a file with all the original sequences
    //which correspond to it
    std::list< ISCluster >::iterator clu_it;
    for(clu_it = clu.begin(); clu_it != clu.end(); ++clu_it) {
	//Remove duplicated masked labels
	if(!clu_it->masked.empty()) {
	    clu_it->masked.sort();
	    clu_it->masked.unique();
	}
	//Alignment position approximation
	for(std::vector< Label >::iterator lab_it = clu_it->lab.begin();
	    lab_it != clu_it->lab.end(); ++lab_it) {
	    lab_it->centroid = round(lab_it->centroid);
	}
        //Cluster ID
	std::cout << "Cluster: " << clu_it->clu_id << std::endl;
        string outclu_cons(subg_dir_name + "/c_" + clu_it->clu_id + ".fa");
	std::ofstream out_clu_cons_stream(outclu_cons.c_str(),
                                          std::ios_base::binary);
        if(!out_clu_cons_stream.is_open()) {
            ERROR("Error in creating " << outclu_cons << " file.");
            return 1;
        }
        for(std::list< string >::iterator merge_it = clu_it->merged.begin();
            merge_it != clu_it->merged.end(); ++merge_it) {
            string inmerged_cons(subg_dir_name
                                 + "/"
                                 + (*merge_it)
                                 + ".fa");
	    std::ifstream in_merged_cons_stream(inmerged_cons.c_str(),
                                                std::ios_base::binary);
            if(!in_merged_cons_stream.is_open()) {
                ERROR("Error in opening " << inmerged_cons << " file.");
                return 1;
            }
	    out_clu_cons_stream << in_merged_cons_stream.rdbuf();
	    //std::cout << *merge_it << " ";
	    in_merged_cons_stream.close();
	}
	//std::cout << std::endl;
	out_clu_cons_stream.flush();
	out_clu_cons_stream.close();
	//Check file
	int num_seq = 0;
	try{
	    gzFile fp = gzopen(outclu_cons.c_str(), "r");
	    kseq_t* seq = kseq_init(fp);
	    while ((kseq_read(seq)) >= 0) {
		++num_seq;
		if(no_map) {
		    fasta_id_map[seq->name.s];
		}
	    }
	    gzclose(fp);
	} catch(exception& e) {
	    ERROR("Problem in reading FASTA file: " << e.what());
	}
	string consensus = "";
	if(clu_it->merged.size() == 1) {
	    std::cout << "Single merged subgraph consensus. No ClustalW alignment.\n";
	    consensus = clu_it->cons_seq;
	} else {
	    if(num_seq == 1) {
		//If only 1 sequence no need for ClustalW
		try {
		    std::cout << "Single sequence. No ClustalW alignment.\n";
		    gzFile fp = gzopen(outclu_cons.c_str(), "r");
		    kseq_t* seq = kseq_init(fp);
		    kseq_read(seq);
		    std::cout << seq->name.s << "\t  ";
		    string s = seq->seq.s;
		    int weight = fasta_id_map.at(seq->name.s).size() + 1;
		    std::cout << s << "\t";
		    std::cout << s.length() << "\t";
		    std::cout << weight << "\t";
		    std::cout << "\n";
		    gzclose(fp);
		    consensus = s;
		} catch(exception& e) {
		    ERROR("Single sequence error: " << e.what());
		}
	    } else {
		//If the number of sequences exceedes the limit
		//the consensus is computed by using the consensus
		//of the merged clusters
		if(num_seq > CLUSTAL_MAX_SEQ) {
		    WARN("Number of sequences exceeding ClustalW limit.");
		    std::cout << "Computing consensus only on merged subgraph consensus.\n";
		    std::ofstream out_clu_merg_stream(outclu_cons.c_str());
		    if(!out_clu_merg_stream.is_open()) {
			ERROR("Error in creating " << outclu_cons << " file.");
			return 1;
		    }
		    std::map< string, std::list< string > > cons_id_map;
		    int num_con_cons = 0;
		    for(std::list< string >::iterator merge_it = clu_it->merged.begin();
			merge_it != clu_it->merged.end(); ++merge_it) {
			out_clu_merg_stream << ">" << *merge_it << "\n";
			out_clu_merg_stream << cons_map.at(*merge_it) << "\n";
			cons_id_map[*merge_it];
			num_con_cons++;
		    }
		    out_clu_merg_stream.flush();
		    out_clu_merg_stream.close();
		    if(num_con_cons > CLUSTAL_MAX_SEQ) {
			WARN("Num. of consensus exceeding ClustalW limit: " << num_con_cons);
			std::cout << "Selecting " << CLUSTAL_MAX_SEQ << " consensus sequences." << std::endl;
			outclu_cons = subg_dir_name + "/c_c_" + clu_it->clu_id + ".fa";
			std::ofstream out_clu_merg_stream(outclu_cons.c_str());
			if(!out_clu_merg_stream.is_open()) {
			    ERROR("Error in creating " << outclu_cons << " file.");
			    return 1;
			}
			cons_id_map.clear();
			int step = num_con_cons / CLUSTAL_MAX_SEQ;
			int num_taken = 0;
			for(std::list< string >::iterator merge_it = clu_it->merged.begin();
			    merge_it != clu_it->merged.end(); merge_it++) {
			    if(num_taken % step == 0) {
				out_clu_merg_stream << ">" << *merge_it << "\n";
				out_clu_merg_stream << cons_map.at(*merge_it) << "\n";
				cons_id_map[*merge_it];
			    }
			    num_taken++;
			}
			out_clu_merg_stream.flush();
			out_clu_merg_stream.close();
		    }
		    //Output file
		    CFile in_clu_cons_file(outclu_cons);
		    if(!in_clu_cons_file.Exists()) {
			ERROR("File " << outclu_cons << " not found.");
			return 1;
		    }
		    //Multiple sequence structure
		    consensus = representativeString(cons_id_map,
						     outclu_cons,
						     msa_clustalw,
						     INIT_W_SEQ_THR,
						     INIT_W_SYM_THR,
						     END_MED_W_SEQ_THR,
						     END_MED_W_SYM_THR,
						     END_HIGH_W_SEQ_THR,
						     END_HIGH_W_SYM_THR,
						     true);
		} else {
		    //MSA on sequences belonging to the clusters
		    //Output file
		    CFile in_clu_cons_file(outclu_cons);
		    if(!in_clu_cons_file.Exists()) {
			ERROR("File " << outclu_cons << " not found.");
			return 1;
		    }
		    //Multiple sequence structure
		    consensus = representativeString(fasta_id_map,
						     outclu_cons,
						     msa_clustalw,
						     INIT_W_SEQ_THR,
						     INIT_W_SYM_THR,
						     END_MED_W_SEQ_THR,
						     END_MED_W_SYM_THR,
						     END_HIGH_W_SEQ_THR,
						     END_HIGH_W_SYM_THR,
						     true);
		    if((consensus.length() < FILTER_LEN_THR / 2) && num_seq < 5) {
			std::cout << "Consensus:\t";
			std::cout << consensus << "\t";
			std::cout << consensus.length() << "\n";
			INFO("Short consensus sequence: computing extended consensus.");
			consensus = representativeString(fasta_id_map,
							 outclu_cons,
							 msa_clustalw,
							 INIT_W_SEQ_THR,
							 INIT_W_SYM_THR,
							 0.51,
							 0,
							 0.51,
							 0,
							 true);
		    }
		}
		if(consensus.length() < FILTER_LEN_THR) {
		    WARN("Short consensus sequence: "
			 << clu_it->clu_id << " ("
			 << consensus.length() << ")");
		}
	    }
	}
	std::cout << "Consensus:\t";
	std::cout << consensus << "\t";
	std::cout << consensus.length() << "\n";
	std::cout << "----------------------------------------\n";
	clu_it->cons_seq = consensus;
	clu_it->seq_len = consensus.length();
	clu_it->clu_score = ((double)clu_it->seq_len * clu_it->weight) / clu_it->num_aln;
    }
    return 0;
}

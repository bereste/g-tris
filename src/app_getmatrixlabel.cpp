/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"
#include "iscluster.hpp"

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <sstream>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

class GetMatrixLabel : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
};

void GetMatrixLabel::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions); \
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Program to get the labels of the matrix entries " \
                              "for those having a small number of labels.");

    arg_desc->AddKey
        ("cf", "cluster_file",
         "Input file containing a list of cluster files "       \
         "with their corresponding labels.",				\
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

     arg_desc->AddKey
	 ("cm", "cluster_matrix",
	  "Cluster matrix file in CSV format.",
	  CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

     arg_desc->AddKey
	 ("cr", "cluster_representant",
	  "Input file containing a list of representative clusters.",
	  CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

     // Setup arg.descriptions for this application
     SetupArgDescriptions(arg_desc.release());
}

void printMessage() {
    INFO("Warning Message: this program works only on Pipeline 1.0.");
    std::cout << "****************************************\n"
	      << "The program assumes that the cluster_id \n"
	      << "names are only in the form: subg_N_N_N. \n"
	      << "No missing (mis_subg_N_N_N) or \n"
	      << "consensus (cons_[mis_]subg_N_N_N) names \n"
	      << "are recognized. \n"
	      << "****************************************"
	      << std::endl;
}

int GetMatrixLabel::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("cf"));
    assert(args.Exist("cm"));
    assert(args.Exist("cr"));
    //Print warning message
    printMessage();

    //Check input files
    string in_cluster_file = args["cf"].AsString();
    ifstream in_cluster_stream(in_cluster_file.c_str());
    if(!in_cluster_stream.is_open()) {
        ERROR("Error in opening " << in_cluster_file << " file.");
        return 1;
    }

    //Input CSV matrix
    CFile in_cluster_csv_file(args["cm"].AsString());
    ifstream in_cluster_csv_stream(args["cm"].AsString().c_str());
    if(!in_cluster_csv_stream.is_open()) {
        ERROR("Error in opening " << args["cm"].AsString() << " file.");
        return 1;
    }

    //Input Representative Cluster file
    CFile in_repr_clu_file(args["cr"].AsString());
    ifstream in_repr_clu_stream(args["cr"].AsString().c_str());
    if(!in_repr_clu_stream.is_open()) {
	ERROR("Error in opening " << args["cr"].AsString() << " file.");
	return 1;
    }

    //Output cluster_id association
    string out_label_file = in_cluster_csv_file.GetDir()
	+ in_cluster_csv_file.GetBase()  + "_labels.csv";
    ofstream out_label_stream(out_label_file.c_str());
    if(!out_label_stream.is_open()) {
        ERROR("Error in creating " << out_label_file << " file.");
        return 1;
    }

    //Output representative clusters
    string out_repr_clu_file = in_repr_clu_file.GetDir()
	+ in_repr_clu_file.GetBase() + ".clu";
    auto_ptr<CObjectOStream>
        out_repr_clu_stream(CObjectOStream::Open(CLU_TYPE, out_repr_clu_file));
    if(!out_repr_clu_stream->InGoodState()) {
	ERROR("Error in creating " << out_repr_clu_file << " file.");
	return 1;
    }

    INFO("PARAMETERS");
    INFO("Input file of clusters: " << in_cluster_file);
    INFO("Input file of matrix clusters: " << args["cm"].AsString());
    INFO("Input file of representative clusters: " << args["cr"].AsString());
    INFO("Output label CSV file: " << out_label_file);

    INFO("Started reading input files.");
    string line;
    int num_elements = 0;
    std::map< string, std::map< string, ISCluster > > clu_map;
    while(std::getline(in_cluster_stream, line)) {
	size_t pos = line.find(",", 0);
	if(pos == string::npos) {
	    ERROR("Error in parsing cluster file entry.");
	    return 1;
	}
	string clu_file = line.substr(0, pos);
	string clu_label = line.substr(pos + 1);
	//std::cout << clu_file << " -- " << clu_label << "\n";
	clu_map[clu_label];
	auto_ptr< CObjectIStream > in_clu_stream
	    (CObjectIStream::Open(CLU_TYPE, clu_file));
	if(!in_clu_stream->InGoodState()) {
	    ERROR("Error in opening " << clu_file << " file.");
	    return 1;
	}
	//Read cluster
	while(!in_clu_stream->EndOfData()) {
	    ISCluster c;
	    *in_clu_stream >> c;
	    clu_map[clu_label][c.clu_id] = c;
	}
	num_elements++;
    }
    in_cluster_stream.close();

    INFO("Finished reading input files.");
    INFO("Num. read cluster_files: " << num_elements);

    INFO("Started computing labels.");
    //Compute labels
    while(std::getline(in_cluster_csv_stream, line)) {
	stringstream ss(line);
	string matr_id;
	getline(ss, matr_id, ',');
	string token;
	int num_clu = 0;
	int tot_one_lab = 0;
	string single_cons = "";
	string not_single_cons = "";
	std::list< std::pair< string, std::pair< double, int > > > single_lab;
	while(getline(ss, token, ',')) {
	    size_t s_pos;
	    s_pos = token.find(FS_LABEL);
	    if(s_pos == string::npos) {
		ERROR("Error: cluster_id entry problem.");
		return 1;
	    }
	    string label = token.substr(0, s_pos);
	    string clu_id = token.substr(s_pos + 1);
	    if(clu_map.find(label) == clu_map.end()) {
		ERROR("Error in parsing exp name in cluster CSV file.");
		ERROR("Exp. name: " << label);
		return 1;
	    }
	    if(clu_map[label].find(clu_id) == clu_map[label].end()) {
		ERROR("Error in parsing cluster_id in cluster CSV file.");
		ERROR("Exp. name: " << label);
		ERROR("Cluster_id: " << clu_id);
		INFO(line);
		return 1;
	    }
	    num_clu++;
	    int num_label = clu_map[label][clu_id].num_aln;
	    if(num_label == 1) {
		tot_one_lab++;
		single_lab.push_back(make_pair(clu_map[label][clu_id].lab[0].target_id,
					       make_pair(clu_map[label][clu_id].lab[0].centroid,
							 clu_map[label][clu_id].lab[0].offset)));
		if(clu_map[label][clu_id].cons_seq.length() > single_cons.length()) {
		    single_cons = clu_map[label][clu_id].cons_seq;
		}
	    }
	    if(clu_map[label][clu_id].cons_seq.length() > not_single_cons.length()) {
		not_single_cons = clu_map[label][clu_id].cons_seq;
	    }
	}
	if(tot_one_lab >= num_clu/2 && !single_lab.empty()) {
	    bool same = true;
	    bool pos_strand = false;
	    bool neg_strand = false;
	    string target_id = single_lab.front().first;
	    double centroid = single_lab.front().second.first;
	    int offset = single_lab.front().second.second;
	    double tot_centroid = centroid;
	    if(offset > 0) pos_strand = true;
	    else neg_strand = true;
	    single_lab.pop_front();
	    int num = 1;
	    //Check if the single labels are the same
	    std::list< std::pair< string, std::pair< double, int > > >::const_iterator l_it;
	    for(l_it = single_lab.begin(); l_it != single_lab.end(); ++l_it) {
		if(target_id != l_it->first || abs(centroid - l_it->second.first) > PCR_THR) {
		    same = false;
		} else {
		    num++;
		    tot_centroid += l_it->second.first;
		    centroid = tot_centroid / num;
		    if(offset > 0) pos_strand = true;
		    else neg_strand = true;
		}
	    }
	    if(same) {
		out_label_stream << matr_id << ","
				 << target_id << ","
				 << int(centroid) << ",";
		if(pos_strand && ! neg_strand) {
		    out_label_stream << "+,";
		} else {
		    if(!pos_strand && neg_strand) {
			out_label_stream << "-,";
		    } else {
			WARN("Discordant strands found.");
			out_label_stream << "NA,";
		    }
		}
		out_label_stream << single_cons;
	    } else {
		out_label_stream << matr_id << ",NA,NA,NA," << not_single_cons;
	    }
	} else {
	    out_label_stream << matr_id << ",NA,NA,NA," << not_single_cons;
	}
	out_label_stream << std::endl;
    }
    in_cluster_csv_stream.close();
    out_label_stream.flush();
    out_label_stream.close();
    INFO("Finished computing labels.");

    INFO("Started computing representative clusters.");
    //Start computing representative clusters
    while(std::getline(in_repr_clu_stream, line)) {
	stringstream ss(line);
	string matr_id;
	getline(ss, matr_id, ',');
	string repr_clu;
	getline(ss, repr_clu);
	size_t s_pos;
	s_pos = repr_clu.find(FS_LABEL);
	if(s_pos == string::npos) {
	    ERROR("Error: cluster_id entry problem.");
	    return 1;
	}
	string label = repr_clu.substr(0, s_pos);
	string clu_id = repr_clu.substr(s_pos + 1);
	clu_map[label][clu_id].clu_id = label + "_" + clu_id;
	*out_repr_clu_stream << clu_map[label][clu_id];
	//std::cout << matr_id << "\t" << label << "\t" << clu_id << "\n";
    }
    out_repr_clu_stream->Flush();
    INFO("Finished computing representative clusters.");

    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    GetMatrixLabel app;
    return app.AppMain(argc, argv);
}

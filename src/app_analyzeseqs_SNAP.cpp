/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "configuration.hpp"
#include "uniqueseqs.hpp"
#include "subgraph_SNAP.hpp"
#include "consensus.hpp"

#include <assert.h>

#include <serial/serial.hpp>
#include <serial/objistr.hpp>

#include <unistd.h>

USING_NCBI_SCOPE;

class AnalyzeSeqs : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);

    void analyzeSequences_SNAP(const std::map< int, string >& node_id,
			       const std::map< string,
                                          std::list< string > >& fasta_id_map,
			       const int num_threads,
			       const string& out_dir,
			       ::SubGraph_SNAP& g,
			       ofstream& out_cons_stream,
			       const double init_cov,
			       const double init_freq,
			       const double med_cov,
			       const double med_freq,
			       const double high_cov,
			       const double high_freq,
			       const bool print);
                          
    void readGraph_SNAP(::SubGraph_SNAP& g, ifstream& in_subg_stream);
};

void AnalyzeSeqs::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Sequence Analyzation program");

    arg_desc->AddKey
        ("subg", "subgraphs",
         "Input SubGraph file.",
         CArgDescriptions::eInputFile);
    
    arg_desc->AddKey
        ("nodemap", "nodemap",
         "Input Node Id Map file.",
         CArgDescriptions::eInputFile);

     arg_desc->AddDefaultKey
        ("num_threads", "num_threads",
         "Number of threads used by BLAST. Default: 1.",
         CArgDescriptions::eInteger, BLAST_THREADS);

    arg_desc->AddOptionalKey
        ("m", "mapfile",
         "File containing the map of unique sequences "   \
         "obtained with option '-uniqueseqs' of " \
         "makeblastdb program.",
         CArgDescriptions::eInputFile);

    arg_desc->AddOptionalKey
        ("init_cov", "init_cov",
         "Consensus beginning sequence coverage threshold " \
         "(INITIAL SEQUENCE CONVERAGE). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("init_freq", "init_freq",
         "Consensus beginning symbol similarity threshold " \
         "(INTIAL SYMBOL FREQUENCY). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("med_cov", "med_cov",
         "Consensus medium ending sequence coverage threshold " \
         "(MEDIUM SEQUENCE CONVERAGE). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("med_freq", "med_freq",
         "Consensus medium ending symbol similarity threshold " \
         "(MEDIUM SYMBOL FREQUENCY). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("high_cov", "high_cov",
         "Consensus high ending sequence coverage threshold " \
         "(HIGH SEQUENCE CONVERAGE). Default: 0.75",
         CArgDescriptions::eDouble);

    arg_desc->AddOptionalKey
        ("high_freq", "high_freq",
         "Consensus high ending symbol similarity threshold " \
         "(HIGH SYMBOL FREQUENCY). Default: 0.75",
         CArgDescriptions::eDouble);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

template< class T >
int recursiveParseGraph_SNAP(T operation, ::SubGraph_SNAP& root){
    if (root.subgraphs == NULL && !root.filtered) {
        return operation(root);
    } else {
        int sum = 0;
        for (int i=0; i < root.num_subgraphs; ++i) {
            sum += recursiveParseGraph_SNAP(operation, root.subgraphs[i]);
        }
        return sum;
    }
}

struct PrintSubGraph_SNAP {
    int operator() (::SubGraph_SNAP& g) {
        INFO(g.id << "\t" << g.graph->GetNodes() << "\t" <<
             g.graph->GetEdges() << "\t" << g.weight);
        return 0;
    }
};


template < typename MultipleSequenceAligner >
struct CreateFastaOQC_SNAP {
    const std::map< int, string >& node_id;
    const std::map< string, std::list< string > >& fasta_id_map;
    const std::string& subg_dir;
    std::ofstream& out_cons_stream;
    const MultipleSequenceAligner& msa;
    const double init_cov;
    const double init_freq;
    const double med_cov;
    const double med_freq;
    const double high_cov;
    const double high_freq;
    const bool print;

    CreateFastaOQC_SNAP(const std::map< int, string >& _node_id,
			const std::map< string, std::list< string > >& _fasta_id_map,
			const std::string& _subg_dir,
			std::ofstream& _out_cons_stream,
			const MultipleSequenceAligner& _msa,
			const double _init_cov,
			const double _init_freq,
			const double _med_cov,
			const double _med_freq,
			const double _high_cov,
			const double _high_freq,
			const bool _print) :
        node_id(_node_id),
        fasta_id_map(_fasta_id_map),
        subg_dir(_subg_dir),
        out_cons_stream(_out_cons_stream),
        msa(_msa),
        init_cov(_init_cov),
        init_freq(_init_freq),
        med_cov(_med_cov),
        med_freq(_med_freq),
        high_cov(_high_cov),
        high_freq(_high_freq),
        print(_print)
    {}

    int operator() (::SubGraph_SNAP& g) {
	INFO("Subgraph: " << g.id);
	string insubg(subg_dir + "/" + g.id + ".fa");
	string consensus;
	if (g.graph->GetNodes() == 1) {
	    try {
		gzFile fp = gzopen(insubg.c_str(), "r");
		kseq_t* seq = kseq_init(fp);
		kseq_read(seq);
		string id = seq->name.s;
		string s = seq->seq.s;
		int weight = fasta_id_map.at(id).size() + 1;
		std::cout << id << "\t  ";
		std::cout << s << "\t";
		std::cout << s.length() << "\t";
		std::cout << weight << "\t";
		std::cout << "\n";
		consensus = s;
		gzclose(fp);
	    } catch(exception& e) {
		ERROR("Single sequence consensus error: " << e.what());
	    }
	} else {
	    if (g.graph->GetNodes() > CLUSTAL_MAX_SEQ) {
		//Consensus among the most significative sequences
		string repr_insubg(subg_dir + "/repr_" + g.id + ".fa");
		INFO("Computing consensus among the most representative sequences.");
		priority_queue< RepresentativeSeqs,
				std::vector< RepresentativeSeqs >,
				CompareRepresentativeSeqs > repr_seqs;
		try {
		    gzFile fp = gzopen(insubg.c_str(), "r");
		    kseq_t* seq = kseq_init(fp);
		    int count = 0;
		    while ((kseq_read(seq)) >= 0 && count < CLUSTAL_MAX_SEQ) {
			RepresentativeSeqs rs;
			rs.id = seq->name.s;
			rs.seq = seq->seq.s;
			rs.weight = fasta_id_map.at(rs.id).size() + 1;
			repr_seqs.push(rs);
			count++;
		    }
		    while ((kseq_read(seq)) >= 0) {
			RepresentativeSeqs rs;
			rs.id = seq->name.s;
			rs.seq = seq->seq.s;
			rs.weight = fasta_id_map.at(rs.id).size() + 1;
			if(rs.weight > repr_seqs.top().weight) {
			    repr_seqs.pop();
			    repr_seqs.push(rs);
			}
		    }
		    gzclose(fp);
		} catch(exception& e) {
                    ERROR("Multiple sequence consensus error: " << e.what());
                }
		std::ofstream out_repr_seqs_stream(repr_insubg.c_str());
		if(!out_repr_seqs_stream.is_open()) {
		    ERROR("Error in creating representative sequences "
			  << repr_insubg << " file.");
		}
		while(!repr_seqs.empty()) {
		    RepresentativeSeqs rs = repr_seqs.top();
		    out_repr_seqs_stream << ">" << rs.id << "\n";
		    out_repr_seqs_stream << rs.seq << "\n";
		    repr_seqs.pop();
		}
		out_repr_seqs_stream.flush();
		out_repr_seqs_stream.close();
		consensus = representativeString(fasta_id_map,
						 repr_insubg,
						 msa,
						 init_cov,
						 init_freq,
						 med_cov,
						 med_freq,
						 high_cov,
						 high_freq,
						 print);
	    } else {
		//Consensus among all the suences
		consensus = representativeString(fasta_id_map,
						 insubg,
						 msa,
						 init_cov,
						 init_freq,
						 med_cov,
						 med_freq,
						 high_cov,
						 high_freq,
						 print);
	    }
	}
	out_cons_stream << ">" << g.id << "\n";
	out_cons_stream << consensus << "\n";
	out_cons_stream.flush();
	std::cout << "Consensus:\t\t  ";
	std::cout << consensus << "\t";
	std::cout << consensus.length() << std::endl;
	return 1;
    }
};

void AnalyzeSeqs::readGraph_SNAP(::SubGraph_SNAP& g, ifstream& in_subg_stream) {
    readSerializedGraph(g, in_subg_stream);
    if (g.num_subgraphs == 0 || g.filtered) {
        return;
    } else {
        g.subgraphs = new ::SubGraph_SNAP[g.num_subgraphs];
        for (int i = 0; i < g.num_subgraphs; ++i) {
            readGraph_SNAP(g.subgraphs[i], in_subg_stream);
        }
    }
}

void AnalyzeSeqs::analyzeSequences_SNAP(const std::map< int, string >& node_id,
					const std::map< string,
                                                   std::list< string > >& fasta_id_map,
					const int num_threads,
					const string& subg_dir,
					::SubGraph_SNAP& g,
					ofstream& out_cons_stream,
					const double init_cov,
					const double init_freq,
					const double med_cov,
					const double med_freq,
					const double high_cov,
					const double high_freq,
					const bool print) {
    //Multiple sequence alignment and consensus
    std::string bin_path= "";
    char* _bin_path= readlink_malloc("/proc/self/exe");
    if (_bin_path==NULL) {
        ERROR("Impossible to determine the path of the executable. "  <<
              "Let it unspecified (i.e., search in the PATH).");
    } else {
        bin_path= _bin_path;
        free(_bin_path);
        CDirEntry cd(bin_path);
        bin_path= cd.GetDir();
        INFO("The directory of the executable is '" << bin_path << "'.");
    }
    // MultipleSequenceAligner_ClustalW msa_clustalw(bin_path);
    // CreateFastaOQC_SNAP< MultipleSequenceAligner_ClustalW >
    //     fastaOQC_SNAP(node_id,
    // 		      fasta_id_map,
    // 		      subg_dir,
    // 		      out_cons_stream,
    // 		      msa_clustalw,
    // 		      init_cov,
    // 		      init_freq,
    // 		      med_cov,
    // 		      med_freq,
    // 		      high_cov,
    // 		      high_freq,
    // 		      print);
    MultipleSequenceAligner_ClustalW_MPI msa_clustalw(bin_path, num_threads);
    CreateFastaOQC_SNAP< MultipleSequenceAligner_ClustalW_MPI >
        fastaOQC_SNAP(node_id,
    		      fasta_id_map,
    		      subg_dir,
    		      out_cons_stream,
    		      msa_clustalw,
    		      init_cov,
    		      init_freq,
    		      med_cov,
    		      med_freq,
    		      high_cov,
    		      high_freq,
    		      print);
    
    // MultipleSequenceAligner_ClustalOmega msa_clustalo(1);
    // CreateFastaOQC_SNAP<MultipleSequenceAligner_ClustalOmega>
    //     fastaOQC_SNAP(node_id,
    //              fasta_id_map,
    //              subg_dir,
    //              cons,
    //              msa_clustalo,
    //              init_cov,
    //              init_freq,
    //              med_cov,
    //              med_freq,
    //              high_cov,
    //              high_freq,
    //              print);
    recursiveParseGraph_SNAP(fastaOQC_SNAP, g);
}

int AnalyzeSeqs::Run(void) {
    const CArgs& args = GetArgs();
    assert(args.Exist("subg"));
    assert(args.Exist("nodemap"));
    //Check input files
    CDirEntry in_subg_file(args["subg"].AsString());
    if(!in_subg_file.Exists()) {
	ERROR("File " << args["subg"].AsString() << " not found.");
	return 1;
    }
    CFile in_nodemap_file(args["nodemap"].AsString());
    if(!in_nodemap_file.Exists()) {
	ERROR("File " << args["nodemap"].AsString() << " not found.");
	return 1;
    }
    if (args["m"].HasValue()) {
	CFile in_map_file(args["m"].AsString());
	if(!in_map_file.Exists()) {
	    ERROR("File " << args["m"].AsString() << " not found.");
	    return 1;
	}
    }
    int num_threads = 1;
    num_threads = args["num_threads"].AsInteger();
    
    string subg_dir(in_subg_file.GetDir() + "subgraphs");
    string outcons(subg_dir + "/consensus.fa");
    ofstream out_cons_stream(outcons.c_str());
    if(!out_cons_stream.is_open()) {
	ERROR("Error in creating " << outcons << " file.");
	return 1;
    }
    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input SubGraph file: " << args["subg"].AsString());
    INFO("Input Node Id Map file: " << args["nodemap"].AsString());
    if (args["m"].HasValue()) {
        INFO("Input MAP file: " << args["m"].AsString());
    }
    INFO("Num. Threads: " << num_threads);
    //Print output parameters
    INFO("Subgraph dir: " << subg_dir);
    INFO("Consensus file: " << outcons);

    INFO("READING INPUT FILES");
    //Read serialized Node Id Map
    std::map< int, string > node_id;
    {
	auto_ptr< CObjectIStream > in_nodemap_stream
	    (CObjectIStream::Open(ASN_TYPE, args["nodemap"].AsString()));
	if(!in_nodemap_stream->InGoodState()) {
	    ERROR("Error in opening " << args["nodemap"].AsString() << " file.");
	    return 1;
	}
	in_nodemap_stream->Read(&node_id,
				CStlClassInfo_map< int, string >::	\
				GetTypeInfo(CStdTypeInfo< int >::GetTypeInfo(),
					    CStdTypeInfo< string >::GetTypeInfo()) );
	// std::map< int, string >::const_iterator node_it;
	// for ( node_it = node_id.begin(); node_it != node_id.end(); ++node_it) {
	//   std::cout << node_it->first << ": " << node_it->second << "\n";
	// }
    }

    //Read serialized FASTA Id Map
    UniqueSeqs unique_seq_map;
    if (args["m"].HasValue()) {
        auto_ptr< CObjectIStream > in_map_stream
            (CObjectIStream::Open(ASN_TYPE, args["m"].AsString()));
	if(!in_map_stream->InGoodState()) {
	    ERROR("Error in opening " << args["m"].AsString() << " file.");
	    return 1;
	}
        *in_map_stream >> unique_seq_map;
    } else {
        std::map< int, string > ::const_iterator n_it;
        for (n_it = node_id.begin(); n_it != node_id.end(); ++n_it) {
            unique_seq_map.fasta_id_map[n_it->second];
        }
    }
    // std::map< string, std::list< string > >::const_iterator itt;
    // for ( itt = unique_seq_map.fasta_id_map.begin();
    //      itt != unique_seq_map.fasta_id_map.end(); ++itt) {
    //   std::cout << itt->first << ": " << itt->second.size()+1 << "\n";
    // }

    //Read serialized SubGraph structure
    ::SubGraph_SNAP g;
    ifstream in_subg_stream(args["subg"].AsString().c_str());
    if(!in_subg_stream.is_open()) {
	ERROR("Error in opening " << args["subg"].AsString() << " file.");
	return 1;
    }
    readGraph_SNAP(g, in_subg_stream);
    in_subg_stream.close();
    
    //     //Print subgraph details
    //     INFO("Subg.Id\tNode\tEdge\tWeight");
    //     recursiveParseGraph_SNAP(PrintSubGraph_SNAP(), g);

    //Set consensus thresholds
    double init_cov = INIT_W_SEQ_THR;
    if (args["init_cov"].HasValue()) {
	init_cov = args["init_cov"].AsDouble();
    }
    double init_freq = INIT_W_SYM_THR;;
    if (args["init_freq"].HasValue()) {
	init_freq = args["init_freq"].AsDouble();
    }
    double med_cov = END_MED_W_SEQ_THR;
    if (args["med_cov"].HasValue()) {
	med_cov = args["med_cov"].AsDouble();
    }
    double med_freq = END_MED_W_SYM_THR;
    if (args["med_freq"].HasValue()) {
	med_freq = args["med_freq"].AsDouble();
    }
    double high_cov = END_HIGH_W_SEQ_THR;
    if (args["high_cov"].HasValue()) {
	high_cov = args["high_cov"].AsDouble();
    }
    double high_freq = END_HIGH_W_SYM_THR;
    if (args["high_freq"].HasValue()) {
	high_freq = args["high_freq"].AsDouble();
    }
    
    //Analyze Sequences
    INFO("ANALYZE SUBGRAPH SEQUENCES");
    analyzeSequences_SNAP(node_id,
			  unique_seq_map.fasta_id_map,
			  num_threads,
			  subg_dir,
			  g,
			  out_cons_stream,
			  init_cov,
			  init_freq,
			  med_cov,
			  med_freq,
			  high_cov,
			  high_freq,
			  true);
    
    out_cons_stream.close();
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    AnalyzeSeqs app;
    return app.AppMain(argc, argv);
}

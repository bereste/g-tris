/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "configuration.hpp"
#include "score_comp.hpp"
#include "uniqueseqs.hpp"
#include "iscluster.hpp"
#include "consensus.hpp"

#include <serial/serial.hpp>
#include <serial/objostr.hpp>
#include <serial/objistr.hpp>

#include <sstream>
#include <omp.h>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

struct scoreElem {
    ScoreComputation::TScore sc;
    int n_seq_1;
    int n_seq_2;
    scoreElem(const ScoreComputation::TScore& _sc,
	      const int _n_seq_1,
	      const int _n_seq_2) :
	sc(_sc),
	n_seq_1(_n_seq_1),
	n_seq_2(_n_seq_2) {}
};

class AnalyzeClustersSimple : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    int createJSON(const std::vector< ISCluster > clu,
		   const string out_json_name,
		   const string label,
		   const std::set< string > probl_clu = std::set< string >());
};

int AnalyzeClustersSimple::createJSON(const std::vector< ISCluster > clu,
				      const string out_json_name,
				      const string label,
				      const std::set< string > probl_clu) {
    ofstream out_json_stream(out_json_name.c_str(), ios::out);
    if(!out_json_stream.is_open()) {
        ERROR("Error in creating " << out_json_name << "file.");
        return 1;
    }
    int num_single_lab = 0;
    std::map< string, int > single_lab_map;
    int num_repeated = 0;
    std::map< string, std::map< string, int > > repeated_map;
    int num_na = 0;
    for(std::vector< ISCluster >::const_iterator clu_it = clu.begin();
        clu_it != clu.end(); ++clu_it) {
	if(probl_clu.empty() || probl_clu.find(clu_it->clu_id) == probl_clu.end()) {
	    if(clu_it->lab.size() == 1) {
		num_single_lab++;
		single_lab_map[clu_it->lab[0].target_id]++;
	    } else {
		if(!clu_it->masked.empty()) {
		    if(clu_it->masked.size() == 1) {
			num_repeated++;
			for(std::list< string >::const_iterator ru_it = clu_it->masked.begin();
			    ru_it != clu_it->masked.end(); ++ru_it) {
			    std::size_t pos = ru_it->find("_");
			    if(pos == std::string::npos) {
				ERROR("Error in parsing repeat label: " << *ru_it);
				return 1;
			    }
			    repeated_map[ru_it->substr(0,pos)][ru_it->substr(pos+1)]++;
			}
		    } else {
			num_na++;
		    }
		} else {
		    num_na++;
		}
	    }
	}
    }
    out_json_stream << "{ ";
    out_json_stream << "\"Cluster Name\" : { ";
    string token;
    stringstream stream(label);
    int num_fields = 0;
    while( getline(stream, token, '_') ) {
	if(num_fields > 0) {
	    out_json_stream << ", ";
	}
	num_fields++;
	stringstream ss_num;
	ss_num << num_fields;
	out_json_stream << "\"Field" + ss_num.str() + "\" : \"" << token << "\"";
    }
    out_json_stream << "}, ";
    out_json_stream << "\"Cluster Data\" : { ";
    out_json_stream << "\"Single Labels\" : [ ";
    INFO("Num. Single Lab.: " << num_single_lab);
    for(std::map< string, int >::const_iterator sl_it = single_lab_map.begin();
        sl_it != single_lab_map.end(); ++sl_it) {
        if(sl_it != single_lab_map.begin()) {
            out_json_stream << ",";
        }
        out_json_stream << "{ \"name\" : \"" << sl_it->first << "\", ";
        out_json_stream << "\"count\" : " << sl_it->second << " } ";
    }
    out_json_stream << "], ";
    INFO("Num. Repeated: " << num_repeated);
    out_json_stream << "\"Repeated Labels\" : [ ";
    for(std::map< string, std::map< string, int > >::const_iterator rl_it = repeated_map.begin();
        rl_it != repeated_map.end(); ++rl_it) {
        if(rl_it != repeated_map.begin()) {
            out_json_stream << ",";
        }
        out_json_stream << "{ \"name\" : \"" << rl_it->first << "\", ";
        out_json_stream << "\"child\" : [ ";
        for(std::map< string, int >::const_iterator rrl_it = rl_it->second.begin();
            rrl_it != rl_it->second.end(); ++rrl_it) {
            if(rrl_it != rl_it->second.begin()) {
                out_json_stream << ",";
            }
            out_json_stream << " { \"name\" : \"" << rrl_it->first << "\", ";
            out_json_stream << "\"count\" : " << rrl_it->second << " } ";
        }
        out_json_stream << "] } ";
    }
    out_json_stream << "], ";
    INFO("Num. NA: " << num_na);
    out_json_stream << "\"N.A. Labels\" : [ ";
    out_json_stream << "{ \"name\" : \"na\", \"count\" : " << num_na << " } ";
    out_json_stream << "] } ";
    out_json_stream << "}\n";
    out_json_stream.flush();
    out_json_stream.close();
    return 0;
}

void AnalyzeClustersSimple::Init(void) {
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
			      "Cluster Simple Analyze program");

    arg_desc->AddKey("i", "inputfile",
		     "Input file of serialized clusters (.clu).",
		     CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey("label", "label",
		     "Name label containing dataset information: " \
		     "<cell>_<tissue>[_<marker>]_time_point. " \
		     "E.g. CD34_BM_1 or Myeloid_PB_CD61_9.",
		     CArgDescriptions::eString);

    arg_desc->AddDefaultKey("num_threads", "num_threads",
			    "Number of threads used to analyze clusters. Default: 1.",
			    CArgDescriptions::eInteger, "1");

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

int AnalyzeClustersSimple::Run(void) {
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));
    //Check input files
    CFile in_clu_file(args["i"].AsString());
    if(!in_clu_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }
    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input CLU file: " << args["i"].AsString());
    INFO("Label : " << args["label"].AsString());
#ifdef ENABLE_OPENMP
    if (args["num_threads"].AsInteger() > 1) {
	if (args["num_threads"].AsInteger() < omp_get_num_procs()) {
	    omp_set_num_threads(args["num_threads"].AsInteger());
	    INFO("N. Threads: " << args["num_threads"].AsInteger());
	} else {
	    omp_set_num_threads(omp_get_num_procs());
	    INFO("N. Threads: " << omp_get_num_procs());
	}
    } else {
	INFO("N. Threads: 1");
    }
#endif
    //Norprobl clu output
    string out_noprobl_clu_name = in_clu_file.GetDir() + in_clu_file.GetBase() + ".noprobl.clu";
    auto_ptr<CObjectOStream> out_noprobl_clu_stream(CObjectOStream::Open(CLU_TYPE, out_noprobl_clu_name));
    if(!out_noprobl_clu_stream->InGoodState()) {
	ERROR("Error in creating " << out_noprobl_clu_name << " file.");
	return 1;
    }
    INFO("Output CLU file: " << out_noprobl_clu_name);
    //Bed output
    string out_bed = in_clu_file.GetDir() + in_clu_file.GetBase() + ".noprobl.bed";
    ofstream out_bed_stream(out_bed.c_str());
    if(!out_bed_stream.is_open()) {
	ERROR("Error in creating " << out_bed << " file.");
	return 1;
    }
    INFO("Output BED file: " << out_bed);
    INFO("Started reading input files.");
    std::vector< ISCluster > clu;
    {
	auto_ptr< CObjectIStream > in_clu_stream
	    (CObjectIStream::Open(CLU_TYPE, args["i"].AsString()));
	if(!in_clu_stream->InGoodState()) {
	    ERROR("Error in opening " << args["i"].AsString() << " file.");
	    return 1;
	}
	while (!in_clu_stream->EndOfData()) {
	    ISCluster c;
	    *in_clu_stream >> c;
	    clu.push_back(c);
	}
    }
    INFO("Finished reading input files.");
    INFO("Num. Read Clusters: " << clu.size());

    //Generate matrix of consensus alignments
    std::vector< std::vector< bool > > stat_mat(clu.size(),
						std::vector< bool >(clu.size(), false));
    INFO("Started computing similarity score matrix.");
    int num_stat = 0;
    ScoreComputation sc;
    unsigned int num_iterations = clu.size() * (clu.size() - 1) / 2;
    unsigned int off1 = 0;
    unsigned int off2 = off1 + 1;
#pragma omp parallel for schedule(dynamic, 1) shared(num_stat, stat_mat, off1, off2) private(sc)
    for (unsigned int i = 0; i < num_iterations; ++i) {
	int c1_off = 0;
	int c2_off = 0;
#pragma omp critical
	{
	    if(off2 == clu.size()) {
		off1++;
		off2 = off1 + 1;
	    }
	    c1_off = off1;
	    c2_off = off2;
	    off2++;
	}
	ISCluster c1 = clu.at(c1_off);
	ISCluster c2 = clu.at(c2_off);
	ScoreComputation::TScore score;
	score = sc.compute_score(c1.cons_seq, c2.cons_seq);
	//Condition to perform statAnalysis
	if ((score.bit_score > MIN_BITSCORE_THR ||
	     (score.bit_score/score.aln_len) > MIN_BITS_ALNLEN_THR) &&
	    (score.bit_score/score.aln_len) > MIN_ABS_BITS_ALNLEN_THR) {
#pragma omp critical
	    {
		num_stat++;
		stat_mat[c1_off][c2_off] = true;
		stat_mat[c2_off][c1_off] = true;
	    }
	}
    }

    INFO("Finished computing similarity score matrix.");
    INFO("Num. Processed Elements: " << num_iterations);
    INFO("Num. Elements Statistically Evaluated: " << num_stat);
    std::set< string > probl_clu;
    for(unsigned int i = 0; i < stat_mat.size(); ++i) {
	unsigned int sa = 0;
	for(unsigned int j = 0; j < stat_mat[i].size(); ++j) {
	    if(stat_mat[i][j]) {
		sa++;
	    }
	}
	//if(sa > 10) {
	if(sa > clu.size() / 4) {
	    probl_clu.insert(clu[i].clu_id);
	}
    }
    INFO("Num. Clusters: " << clu.size());
    INFO("Num. Problematic Clusters: " << probl_clu.size());
    for(std::set< string >::const_iterator pc_it = probl_clu.begin();
	pc_it != probl_clu.end(); ++pc_it) {
	std::cout << *pc_it << "\n";
    }
    //Write clu and bed files with not problematic clusters
    //Print clusters in BED format
    out_bed_stream << "track name=clusters ";
    out_bed_stream << "description=\"Integration Sites Noprobl. Clusters.\" ";
    out_bed_stream << "useScore=1" << std::endl;
    for(unsigned int i = 0; i < clu.size(); ++i) {
	if(probl_clu.find(clu[i].clu_id) == probl_clu.end()) {
	    *out_noprobl_clu_stream << clu[i];
	    out_bed_stream << clu[i];
	}
    }
    out_noprobl_clu_stream->Flush();
    out_noprobl_clu_stream->Close();
    out_bed_stream.flush();
    out_bed_stream.close();
    //Computation of statistics in JSON format
    INFO("Started computing data summary.");
    string out_json_name = in_clu_file.GetDir() + in_clu_file.GetBase() + ".json";
    INFO("Output all clu JSON file: " << out_json_name);
    createJSON(clu, out_json_name, args["label"].AsString());
    string out_noprob_json_name = in_clu_file.GetDir() + in_clu_file.GetBase() + ".noprobl.json";
    INFO("Output noprobl clu JSON file: " << out_noprob_json_name);
    createJSON(clu, out_noprob_json_name, args["label"].AsString(), probl_clu);
    INFO("Finished computing data summary.");
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    AnalyzeClustersSimple app;
    return app.AppMain(argc, argv);
}

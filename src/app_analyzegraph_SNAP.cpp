/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include "log.hpp"
#include "configuration.hpp"
#include "alignments.hpp"
#include "uniqueseqs.hpp"
#include "subgraph_SNAP.hpp"

#include <serial/serial.hpp>
#include <serial/objostr.hpp>

//Kseq fasta parser
#include <zlib.h>
#include <stdio.h>
#include "kseq.h"

KSEQ_INIT(gzFile, gzread)

USING_NCBI_SCOPE;
USING_SCOPE(objects);

struct DecomposeCC_SNAP {
    const std::map< int, string >& node_id;
    const std::map< string, std::list< string > >& fasta_id_map;
    DecomposeCC_SNAP(const std::map< int, string >& _node_id,
		     const std::map< string, std::list< string > >& _fasta_id_map) :
        node_id(_node_id),
        fasta_id_map(_fasta_id_map) {}
    int operator() (::SubGraph_SNAP& g);
};

struct Decompose2C_SNAP {
    const std::map< int, string >& node_id;
    const std::map< string, std::list< string > >& fasta_id_map;
    Decompose2C_SNAP(const std::map< int, string >& _node_id,
		     const std::map< string, std::list< string > >& _fasta_id_map) :
        node_id(_node_id),
        fasta_id_map(_fasta_id_map) {}
    int operator() (::SubGraph_SNAP& g);
};

struct DecomposeQC_SNAP {
    const std::map< int, string >& node_id;
    const std::map< string, std::list< string > >& fasta_id_map;
    DecomposeQC_SNAP(const std::map< int, string >& _node_id,
		     const std::map< string, std::list< string > >& _fasta_id_map) :
        node_id(_node_id),
        fasta_id_map(_fasta_id_map) {}
    int operator() (::SubGraph_SNAP& g);
};

struct CreateFastaQC_SNAP {
    const std::map< int, string >& node_id;
    const std::map< string, string >& fasta_seq_map;
    const string& out_dir;
    CreateFastaQC_SNAP(const std::map< int, string >& _node_id,
		       const std::map< string, string >& _fasta_seq_map,
		       const string& _out_dir) :
        node_id(_node_id),
        fasta_seq_map(_fasta_seq_map),
        out_dir(_out_dir) {}
    int operator() (::SubGraph_SNAP& g);
};

struct FilterSubGraph_SNAP {
    const int thr_weight;
    FilterSubGraph_SNAP(const int _thr_weight) :
        thr_weight(_thr_weight) {}
    int operator() (::SubGraph_SNAP& g);
};

struct PrintSubGraph_SNAP {
    int operator() (::SubGraph_SNAP& g);
};

class AnalyzeGraph : public CNcbiApplication {
    virtual void Init(void);
    virtual int Run(void);
    
    void createGraph(const Alignments& al,
                     std::set< std::pair< string, string > >& filt_blast_edges);

    void decomposeGraph_SNAP(const std::set< std::pair< string, string > >& filt_blast_edges,
			     std::map< int, string >& node_id,
			     const std::map< string, std::list< string > >& fasta_id_map,
			     const std::map< string, string >& fasta_seq_map,
			     const string& out_dir,
			     ::SubGraph_SNAP& g,
			     const int dec_lev);
    
    void serializeGraph_SNAP(const ::SubGraph_SNAP& g, ofstream& out_subg_stream);
};

void AnalyzeGraph::Init(void){
    auto_ptr<CArgDescriptions> arg_desc(new CArgDescriptions);
    // Specify USAGE context
    CNcbiApplication::DisableArgDescriptions();

    arg_desc->SetUsageContext(GetArguments().GetProgramBasename(),
                              "Graph Creation and Decomposition program");
    
    arg_desc->AddKey
        ("i", "inputfile",
         "Input file in ASN format containing "                         \
         "the alignments of BLAST. ",                                   \
         CArgDescriptions::eInputFile, CArgDescriptions::fPreOpen);

    arg_desc->AddKey
        ("f", "fastafile",
         "Fasta file containing input sequences.",
         CArgDescriptions::eInputFile);

    arg_desc->AddOptionalKey
        ("m", "mapfile",
         "File containing the map of unique sequences "   \
         "obtained with option '-uniqueseqs' of " \
         "makeblastdb program.",
         CArgDescriptions::eInputFile);

    arg_desc->AddOptionalKey
        ("dec_lev", "dec_lev",
         "Incremental Decomposition Level: " \
         "(1) connected components, " \
         "(2) remove bridge edges, " \
         "(3) quasi-cliques.",
         CArgDescriptions::eInteger);

    // Setup arg.descriptions for this application
    SetupArgDescriptions(arg_desc.release());
}

//Create initial graph from BLAST alignments
void AnalyzeGraph::createGraph(const Alignments& al,
                               std::set< std::pair< string,
                                                    string > >& filt_blast_edges) {
    std::list< AlnData >::const_iterator it;
    for (it = al.al_data.begin(); it != al.al_data.end(); ++it) {
        if (it->query_start < PCR_THR &&
            it->target_start < PCR_THR &&
            al.query_len - it->alignment_len - it->query_start < PCR_THR) {

            if(filt_blast_edges.find(make_pair(it->target_id, al.query_id))
               == filt_blast_edges.end()){
                filt_blast_edges.insert(make_pair(al.query_id, it->target_id));
            }
        }
    }
}

template< class T >
int recursiveParseGraph_SNAP(T operation, ::SubGraph_SNAP& root){
    if (root.subgraphs == NULL && !root.filtered) {
        return operation(root);
    } else {
        int sum = 0;
        for (int i=0; i < root.num_subgraphs; ++i) {
            sum += recursiveParseGraph_SNAP(operation, root.subgraphs[i]);
        }
        return sum;
    }
}

int PrintSubGraph_SNAP::operator()(::SubGraph_SNAP& g) {
    INFO(g.id << "\t" << g.graph->GetNodes() << "\t" <<
         g.graph->GetEdges() << "\t" << g.weight);
    return 0;
}

int DecomposeCC_SNAP::operator()(::SubGraph_SNAP& g) {
    assert(g.subgraphs == NULL);
    TCnComV conComV;
    TSnap::GetWccs(g.graph, conComV);
    g.num_subgraphs = conComV.Len();
    g.subgraphs = new ::SubGraph_SNAP[g.num_subgraphs];
    for (int n_sg = 0; n_sg < g.num_subgraphs; ++n_sg) {
    	stringstream ss_id;
	ss_id << g.id << "_" << n_sg;
	g.subgraphs[n_sg].id = ss_id.str();
    	const TIntV& nodeIdV = conComV[n_sg].NIdV;
    	g.subgraphs[n_sg].graph = TSnap::GetSubGraph(g.graph, nodeIdV);
    	for (int i = 0; i < nodeIdV.Len(); i++) {
	    g.subgraphs[n_sg].weight +=
		fasta_id_map.at(node_id.at(nodeIdV[i].Val)).size() + 1;
    	}
    }
    return g.num_subgraphs;
}

int Decompose2C_SNAP::operator() (::SubGraph_SNAP& g) {
    assert(g.subgraphs == NULL);
    //Check for simple cliques
    if (g.graph->GetNodes() == 1 && g.graph->GetEdges() == 0) {
        return 1;
    }
    if (g.graph->GetNodes() == 2 && g.graph->GetEdges() == 1) {
        return 1;
    }
    if (g.graph->GetNodes() == 3 && g.graph->GetEdges() == 3) {
        return 1;
    }
    TIntPrV brEdgeV;
    TSnap::GetEdgeBridges(g.graph, brEdgeV);
    PUNGraph tmpGraph = TUNGraph::New();
    *tmpGraph = *g.graph;
    for(int e = 0; e < brEdgeV.Len(); e++) {
    	tmpGraph->DelEdge(brEdgeV[e].Val1, brEdgeV[e].Val2);
    }
    TCnComV conComV;
    TSnap::GetWccs(tmpGraph, conComV);
    g.num_subgraphs = conComV.Len();
    g.subgraphs = new ::SubGraph_SNAP[g.num_subgraphs];
    for (int n_sg = 0; n_sg < g.num_subgraphs; ++n_sg) {
    	stringstream ss_id;
	ss_id << g.id << "_" << n_sg;
	g.subgraphs[n_sg].id = ss_id.str();
    	const TIntV& nodeIdV = conComV[n_sg].NIdV;
    	g.subgraphs[n_sg].graph = TSnap::GetSubGraph(tmpGraph, nodeIdV);
    	for (int i = 0; i < nodeIdV.Len(); i++) {
	    g.subgraphs[n_sg].weight +=
		fasta_id_map.at(node_id.at(nodeIdV[i].Val)).size() + 1;
    	}
    }
    return g.num_subgraphs;
}

int greedyOQC_SNAP(const PUNGraph& g, TIntV& subgNodesV) {
    PUNGraph tmpGraph = TUNGraph::New();
    *tmpGraph = *g;
    std::multimap< int, int > rank_edges;
    for(TUNGraph::TNodeI n_it = tmpGraph->BegNI(); n_it < tmpGraph->EndNI(); n_it++) {
        rank_edges.insert(make_pair(n_it.GetDeg(), n_it.GetId()));
        subgNodesV.Add(n_it.GetId());
    }
    double best_f = -1;
    std::multimap< int, int >::iterator it;
    while (!rank_edges.empty()) {
        it = rank_edges.begin();     
        double f = tmpGraph->GetEdges() -
	    (ALPHA_QC * (tmpGraph->GetNodes() * (tmpGraph->GetNodes() - 1)) / 2);
        if (f > best_f) {
            best_f = f;
            subgNodesV.Clr();
            for(TUNGraph::TNodeI n_it = tmpGraph->BegNI(); n_it < tmpGraph->EndNI(); n_it++) {
		subgNodesV.Add(n_it.GetId());
	    }
        }
	TUNGraph::TNodeI del_n_it = tmpGraph->GetNI(it->second);
	for(int e = 0; e < del_n_it.GetOutDeg(); e++) {
	    int del_n_adj = del_n_it.GetOutNId(e);
	    int del_n_adj_deg = tmpGraph->GetNI(del_n_adj).GetDeg();
	    std::pair< std::multimap< int, int >::iterator,
                       std::multimap< int, int >::iterator > ret;
	    ret = rank_edges.equal_range(del_n_adj_deg);
	    for(std::multimap< int, int >::iterator r_it=ret.first; r_it != ret.second; ++r_it) {
		if(r_it->second == del_n_adj) {
		    rank_edges.insert(make_pair(del_n_adj_deg - 1, del_n_adj));
		    rank_edges.erase(r_it);
		    break;
                }
	    }
	}
        tmpGraph->DelNode(it->second);
	rank_edges.erase(it);
        // rank_edges.clear();
        // for(TUNGraph::TNodeI n_it = tmpGraph->BegNI(); n_it < tmpGraph->EndNI(); n_it++) {
	//    rank_edges.insert(make_pair(n_it.GetDeg(), n_it.GetId()));
    	// }
    }
    return subgNodesV.Len();
}

int quasiCliques_SNAP(PUNGraph& g, TCnComV& qcComV) {
    int subgraphs = 0;
    int removed = 0;
    int tot_nodes = g->GetNodes();
    PUNGraph tmpGraph = TUNGraph::New();
    *tmpGraph = *g;
    do {
	TCnComV tmpConComV;
	TSnap::GetWccs(tmpGraph, tmpConComV);
	for (int n_sg = 0; n_sg < tmpConComV.Len(); ++n_sg) {
	    TIntV subgNodesV;
	    const TIntV& nodeIdV = tmpConComV[n_sg].NIdV;
	    PUNGraph qcGraph = TSnap::GetSubGraph(tmpGraph, nodeIdV);
	    removed += greedyOQC_SNAP(qcGraph, subgNodesV);
	    qcComV.Add(TCnCom(subgNodesV));
	    subgraphs++;
	    for (int i = 0; i < subgNodesV.Len(); i++) {
		tmpGraph->DelNode(subgNodesV[i]);
	    }
	}
    } while(removed < tot_nodes);
    return subgraphs;
}

int DecomposeQC_SNAP::operator() (::SubGraph_SNAP& g) {
    assert(g.subgraphs == NULL);
    //Check for simple cliques
    if (g.graph->GetNodes() == 1 && g.graph->GetEdges() == 0) {
        return 1;
    }
    if (g.graph->GetNodes() == 2 && g.graph->GetEdges() == 1) {
        return 1;
    }
    if (g.graph->GetNodes() == 3 && g.graph->GetEdges() == 3) {
        return 1;
    }
    TCnComV qcComV;
    quasiCliques_SNAP(g.graph, qcComV);
    g.num_subgraphs = qcComV.Len();
    g.subgraphs = new ::SubGraph_SNAP[g.num_subgraphs];
    for (int n_sg = 0; n_sg < g.num_subgraphs; ++n_sg) {
    	stringstream ss_id;
	ss_id << g.id << "_" << n_sg;
	g.subgraphs[n_sg].id = ss_id.str();
    	const TIntV& nodeIdV = qcComV[n_sg].NIdV;
    	g.subgraphs[n_sg].graph = TSnap::GetSubGraph(g.graph, nodeIdV);
    	for (int i = 0; i < nodeIdV.Len(); i++) {
	    g.subgraphs[n_sg].weight +=
		fasta_id_map.at(node_id.at(nodeIdV[i].Val)).size() + 1;
    	}
    }
    return g.num_subgraphs;
}

int CreateFastaQC_SNAP::operator() (::SubGraph_SNAP& g) {
    INFO("Writing " << g.id << ": " << g.graph->GetNodes() << " sequences");
    string outfasta(out_dir + "/" + g.id + ".fa");
    ofstream out_fasta_stream(outfasta.c_str());
    if(!out_fasta_stream.is_open()) {
	ERROR("Error in creating " << outfasta << " file.");
    }
    for (TUNGraph::TNodeI n_it = g.graph->BegNI(); n_it < g.graph->EndNI(); n_it++) {
	string id = node_id.at(n_it.GetId());
	string seq = fasta_seq_map.at(id);
        out_fasta_stream << ">" << id << "\n";
        out_fasta_stream << seq << "\n";
    }
    out_fasta_stream.flush();
    out_fasta_stream.close();
    return 1;
}

int FilterSubGraph_SNAP::operator() (::SubGraph_SNAP& g) {
    if (g.weight < thr_weight) {
        g.filtered = true;
        return 0;
    }
    return 1;
}

void AnalyzeGraph::decomposeGraph_SNAP(const std::set< 
				       std::pair< string, string > >& filt_blast_edges,
				       std::map< int, string >& node_id,
				       const std::map< string, std::list< string > >& fasta_id_map,
				       const std::map< string, string >& fasta_seq_map,
				       const string& out_dir,
				       ::SubGraph_SNAP& g,
				       const int dec_lev) {
    //Create SNAP graph from filtered BALST alignments
    std::set<std::pair< string, string > >::const_iterator edge_it;
    std::map< string, int > nodes;
    int n_id = 0;
    for (edge_it = filt_blast_edges.begin(); edge_it != filt_blast_edges.end(); ++edge_it) {
        if (nodes.find(edge_it->first) == nodes.end()) {
	    g.graph->AddNode(n_id);
	    node_id[n_id] = edge_it->first.c_str();
	    nodes[edge_it->first] = n_id;
	    n_id++;
        }
        if(edge_it->first != edge_it->second) {
            if (nodes.find(edge_it->second) == nodes.end()) {
		g.graph->AddNode(n_id);
		node_id[n_id] = edge_it->second.c_str();
		nodes[edge_it->second] = n_id;
		n_id++;
            }
	    g.graph->AddEdge(nodes[edge_it->first], nodes[edge_it->second]);
        }
    }
    INFO("GRAPH PROPERTIES");
    INFO("Graph Nodes: " << g.graph->GetNodes());
    INFO("Graph Edges: " << g.graph->GetEdges());

    int g_weight = 0;
    for (TUNGraph::TNodeI NI = g.graph->BegNI(); NI < g.graph->EndNI(); NI++) {
	string n_name(node_id.at(NI.GetId()));
        g_weight += fasta_id_map.at(n_name).size() + 1;
    }
    INFO("Graph Weight: " << g_weight);

    int cc = 0;
    //Decompose in connected components
    INFO("DECOMPOSE CONNECTED COMPONENTS");
    cc = recursiveParseGraph_SNAP(DecomposeCC_SNAP(node_id, fasta_id_map), g);
    INFO("Num. Subgraphs: " << cc);
    //Print subgraph details
    INFO("Subg.Id\tNode\tEdge\tWeight");
    recursiveParseGraph_SNAP(PrintSubGraph_SNAP(), g);
    //Print graph on file
    string outdot = out_dir + "/Subgraphs.dot";
    TSnap::SaveGViz(g.graph, outdot.c_str());
#ifdef FILTER_SMALL_SUBG
    //Filter small components
    FilterSubGraph f(WEIGHT_THR);
    cc = recursiveParseGraph_SNAP(f,g);
    INFO("Filtered Conn.Comp. (Weight < " << WEIGHT_THR << ")");
    INFO("Num. Subgraphs: " << cc);
    //Print subgraph details
    INFO("Subg.Id\tNode\tEdge\tWeight");
    recursiveParseGraph_SNAP(PrintSubGraph_SNAP(), g);
#endif
    if (dec_lev > 1) {
        //Decompose removing bridges
        INFO("REMOVE BRIDGE EDGES");
        cc = recursiveParseGraph_SNAP(Decompose2C_SNAP(node_id, fasta_id_map), g);
        INFO("Num. Subgraphs: " << cc);
        //Print subgraph details
        INFO("Subg.Id\tNode\tEdge\tWeight");
        recursiveParseGraph_SNAP(PrintSubGraph_SNAP(), g);
#ifdef FILTER_SMALL_SUBG
        //Filter small components
        cc = recursiveParseGraph_SNAP(f,g);
        INFO("Filtered Conn.Comp. (Weight < " << WEIGHT_THR << ")");
        INFO("Num. Subgraphs: " << cc);
        //Print subgraph details
        INFO("Subg.Id\tNode\tEdge\tWeight");
        recursiveParseGraph_SNAP(PrintSubGraph_SNAP(), g);
#endif
	if (dec_lev > 2) {
	    //Decompose in quasi-cliques
	    INFO("DECOMPOSE QUASI-CLIQUES");
            cc = recursiveParseGraph_SNAP(DecomposeQC_SNAP(node_id, fasta_id_map), g);
            INFO("Num. Subgraphs: " << cc);
            //Print subgraph details
            INFO("Subg.Id\tNode\tEdge\tWeight");
            recursiveParseGraph_SNAP(PrintSubGraph_SNAP(), g);
#ifdef FILTER_SMALL_SUBG
            //Filter small components
            cc = recursiveParseGraph_SNAP(f,g);
            INFO("Filtered Conn.Comp. (Weight < " << WEIGHT_THR << ")");
            INFO("Num. Subgraphs: " << cc);
            //Print subgraph details
            INFO("Subg.Id\tNode\tEdge\tWeight");
            recursiveParseGraph_SNAP(PrintSubGraph_SNAP(), g);
#endif
        }
    }	
    //Write subgrpah sequences on file
    recursiveParseGraph_SNAP(CreateFastaQC_SNAP(node_id, fasta_seq_map, out_dir), g);
}

void AnalyzeGraph::serializeGraph_SNAP(const ::SubGraph_SNAP& g, ofstream& out_subg_stream) {
    writeSerializedGraph(g, out_subg_stream);
    if (g.subgraphs == NULL) {
        return;
    } else {
        for (int i = 0; i < g.num_subgraphs; ++i) {
            serializeGraph_SNAP(g.subgraphs[i], out_subg_stream);
        }
    }
}

int AnalyzeGraph::Run(void){
    const CArgs& args = GetArgs();
    assert(args.Exist("i"));
    //Check input files
    CDirEntry in_aln_file(args["i"].AsString());
    if(!in_aln_file.Exists()) {
	ERROR("File " << args["i"].AsString() << " not found.");
	return 1;
    }
    CFile in_fasta_file(args["f"].AsString());
    if(!in_fasta_file.Exists()) {
	ERROR("File " << args["f"].AsString() << " not found.");
	return 1;
    }
    if(args["m"].HasValue()) {
	CFile in_map_file(args["m"].AsString());
	if(!in_map_file.Exists()) {
	    ERROR("File " << args["m"].AsString() << " not found.");
	    return 1;
	}
    }
    // Print input parameters
    INFO("PARAMETERS");
    INFO("Input ASN file: " << args["i"].AsString());
    INFO("Input FASTA file: " << args["f"].AsString());
    if(args["m"].HasValue()) {
        INFO("Input MAP file: " << args["m"].AsString());
    }
    // Print output parameters
    string outsubg(in_aln_file.GetDir() + in_aln_file.GetBase() + ".subg");
    string outnodemap(in_aln_file.GetDir() + in_aln_file.GetBase() + ".nodemap");
    string outsubgraphs(in_aln_file.GetDir() + "subgraphs");
    CDir out_subgraphs_dir(outsubgraphs);
    if(!out_subgraphs_dir.Create()) {
        ERROR("Cannot create " << outsubgraphs << " directory.");
    }
    int dec_lev = DEC_LEV;
    if (args["dec_lev"].HasValue()) {
        dec_lev = args["dec_lev"].AsInteger();
    }
    INFO("Decomposition Level: " << dec_lev);
    INFO("Output SubGraph file: " << outsubg);
    INFO("Output Node Id Map file: " << outnodemap);
    INFO("Output Subgraph dir: " << outsubgraphs);
    auto_ptr<CObjectIStream> in_asn_stream
        (CObjectIStream::Open(ASN_TYPE, args["i"].AsString()));
    if(!in_asn_stream->InGoodState()) {
	ERROR("Error in opening " << args["i"].AsString() << " file.");
	return 1;
    }
    //Create initial graph
    std::set< std::pair< string, string > > filt_blast_edges;
    while(!in_asn_stream->EndOfData()) {
        Alignments al;
        *in_asn_stream >> al;
        createGraph(al, filt_blast_edges);
    }
    //Read unique sequence map
    UniqueSeqs unique_seq_map;
    if(args["m"].HasValue()) {
        auto_ptr< CObjectIStream > in_map_stream
            (CObjectIStream::Open(ASN_TYPE, args["m"].AsString()));
	if(!in_map_stream->InGoodState()) {
	    ERROR("Error in opening " << args["m"].AsString() << " file.");
	    return 1;
	}
        *in_map_stream >> unique_seq_map;
    } else {
        std::set<std::pair< string, string > >::const_iterator e_it;
        for (e_it = filt_blast_edges.begin(); e_it != filt_blast_edges.end(); ++e_it) {
            unique_seq_map.fasta_id_map[e_it->first];
            unique_seq_map.fasta_id_map[e_it->second];
        }
    }
    //Read Fasta sequences
    std::map< string, string > fasta_seq_map;
    {
	gzFile fp = gzopen(args["f"].AsString().c_str(), "r");
	kseq_t* seq = kseq_init(fp);
	while ((kseq_read(seq)) >= 0) {
	    fasta_seq_map[seq->name.s] = seq->seq.s;
	}
	gzclose(fp);
    }
    //Decompose Graph
    ::SubGraph_SNAP g;
    std::map< int, string > node_id;
    decomposeGraph_SNAP(filt_blast_edges,
			node_id,
			unique_seq_map.fasta_id_map,
			fasta_seq_map,
			outsubgraphs,
			g,
			dec_lev);
    //Serialize SubGraph on file
    {
    	ofstream out_subg_stream(outsubg.c_str());
    	serializeGraph_SNAP(g, out_subg_stream);
	out_subg_stream.flush();
	out_subg_stream.close();
    }
    //Serialize Node Id Map on file
    {
	auto_ptr< CObjectOStream > out_nodemap_stream
	    (CObjectOStream::Open(ASN_TYPE, outnodemap));
	if(!out_nodemap_stream->InGoodState()) {
	    ERROR("Error in creating " << outnodemap << " file.");
	    return 1;
	}
	out_nodemap_stream->Write(&node_id,
				  CStlClassInfo_map< int, string >::
				  GetTypeInfo(CStdTypeInfo< int >::GetTypeInfo(),
					      CStdTypeInfo< string >::GetTypeInfo()) );
    }
    return 0;
}

int main(int argc, char* argv[]){
    INFO("Program started");
    AnalyzeGraph app;
    return app.AppMain(argc, argv);
}

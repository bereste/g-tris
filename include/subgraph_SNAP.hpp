/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#ifndef _SUBGRAPH_SNAP_HPP_
#define _SUBGRAPH_SNAP_HPP_

#define SIZE_MAX 18446744073709551615UL

#include <sstream>

//SNAP
#include <Snap.h>

//Hierarchical Subgraph class
class SubGraph_SNAP {
public:
    //Subgraph ID
    std::string id;
    //Subgraph tot weight
    int weight;
    //Filtered
    bool filtered;
    //Undirected Graph
    PUNGraph graph;
    //Number of subgraphs in which 'graph' is decomposed
    int num_subgraphs;
    //Pointer to the subgraphs in which 'graph' is decomposed
    SubGraph_SNAP *subgraphs;

    //Constructor
    explicit SubGraph_SNAP() {
        id = "subg";
        weight = 0;
        filtered = false;
        graph = TUNGraph::New();
        num_subgraphs = 0;
        subgraphs = NULL;
    }
};

void writeSerializedGraph(const ::SubGraph_SNAP& g, ofstream& out) {
    out << "##START_GRAPH##\n";
    out << g.id << "\n";
    out << g.weight << "\n";
    out << g.filtered << "\n";
    out << g.num_subgraphs << "\n";
    out << "##NODES##\n";
    for (TUNGraph::TNodeI ni = g.graph->BegNI(); ni < g.graph->EndNI(); ni++) {
	out << ni.GetId() << "\n";
    }
    out << "##EDGES##\n";
    for (TUNGraph::TEdgeI ei = g.graph->BegEI(); ei < g.graph->EndEI(); ei++) {
	out << ei.GetSrcNId() << "\t" << ei.GetDstNId() << "\n";
    }
    out << "##END_GRAPH##\n";
}

void readSerializedGraph(::SubGraph_SNAP& g, ifstream& in) {
    string str = "";
    do {
	std::getline (in, str);
    }while(str != "##START_GRAPH##");
    std::getline (in, g.id);
    if(!(in >> g.weight)) {
    	ERROR("Error in reading subgraph weight of " << g.id);
    }
    if(!(in >> g.filtered)) {
    	ERROR("Error in reading subgraph filter field of " << g.id);
    }
    if(!(in >> g.num_subgraphs)) {
    	ERROR("Error in reading number of subgraphs " << g.id);
    }
    //     std::cout << "ID: " << g.id << std::endl;
    //     std::cout << "W: " << g.weight << std::endl;
    //     if(g.filtered) {
    //     	std::cout << "Filtered" << std::endl;
    //     } else {
    //     	std::cout << "Not Filtered" << std::endl;
    //     }
    //     std::cout << "NS: " << g.num_subgraphs << std::endl;
    do {
	std::getline (in, str);
    }while(str != "##NODES##");
    string prev = "";
    do {
    	std::getline (in, str);
    	if(prev != "") {
	    istringstream s(prev);
	    int nodeId;
	    s >> nodeId;
	    g.graph->AddNode(nodeId);
    	}
    	prev = str;    
    }while(str != "##EDGES##");
    prev = "";
    do {
	std::getline (in, str);
	if(prev != ""){
	    istringstream s(prev);
	    int srcNodeId;
	    s >> srcNodeId;
	    int destNodeId;
	    s >> destNodeId;
	    if (!g.graph->IsNode(srcNodeId)) {
      		ERROR("Error: missing node of " << g.id);
      		return;
	    }
	    if (!g.graph->IsNode(destNodeId)) {
      		ERROR("Error: missing node of " << g.id);
      		return;
	    }
	    g.graph->AddEdge(srcNodeId, destNodeId);
	}
	prev = str;
    }while(str != "##END_GRAPH##");
    //     std::cout << "Nodes: " << g.graph->GetNodes() << std::endl;
    //     std::cout << "Edges: " << g.graph->GetEdges() << std::endl;
    //     std::cout << std::endl;
}
#endif

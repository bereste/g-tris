/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#ifndef _CONFIGURATION_HPP_
#define _CONFIGURATION_HPP_

//Enable OpenMP (comment to disable)
#define ENABLE_OPENMP

#ifndef ASN_TYPE
//#define ASN_TYPE eSerial_AsnText
#define ASN_TYPE eSerial_AsnBinary
#endif

#ifndef CLU_TYPE
#define CLU_TYPE eSerial_AsnText
#endif

//Enable CSV alignment file print
//#define PRINT_CSV_ALN

//CSV field separator
#define FS_CSV ","
//Label field separator
#define FS_LABEL "$"

//Minimum possible bitscore value
#define MIN_BITSCORE_VAL 1.2864

//Minimum sequence length
#define MIN_SEQ_LEN 10

//Blast word size
#define BLAST_WORD_SIZE "11"
//Blast default e-value
#define BLAST_EVALUE "0.01"
//Blast default identity
#define BLAST_IDENTITY "95"
//Blast default batch size
#define BLAST_BATCH_SIZE "1000000"
//Blast default number of threads
#define BLAST_THREADS "1"

//Number of alignments to keep for each consensus sequence
#define MAX_VALID_ALN 1000
//Percentage of common / total alignments for cluster merging
#define COM_THR 0.6

//Min QUERY_LEN not aligned for sequence initial filter
//Min QUERY_LEN not masked for RepeatMasker
#define FILTER_LEN_THR 25
//Percentage of ALN_LEN / QUERY_LEN for valid alignments
#define ALN_PERC_ID 0.6

//BWA alignment score threshold
#define BWA_ALN_THR 30
//BWA Band width. Essentially, gaps longer than INT will not be found.
#define BWA_W 10
//BWA max number of occurences
#define BWA_MAX_OCC 50000
//BWA min seed lenght
#define BWA_MIN_SEED_LEN 10

//Max allowed PCR threshold for the beginning of the alignments
#define PCR_THR 4
//Quasi-clique coefficient
#define ALPHA_QC 0.33
//Default decomposition level (1 to 3)
#define DEC_LEV 3

//Enable/Disable small subgraph filter
//#define FILTER_SMALL_SUBG
//Minimum weight of the subgraphs for being significative (filter)
#define WEIGHT_THR 4

//Threshold for consensus computation
//Maximum number of sequences for Clustal computation
#define CLUSTAL_MAX_SEQ 1000
//Consensus beginning thresholds for sequences and symbol weights
#define INIT_W_SEQ_THR 0.75
#define INIT_W_SYM_THR 0.75
//Consensus (high) ending thresholds for sequences and symbol weights
#define END_HIGH_W_SEQ_THR 0.75
#define END_HIGH_W_SYM_THR 0.75
//Consensus (medium) ending thresholds for sequences and symbol weights
#define END_MED_W_SEQ_THR 0.75
#define END_MED_W_SYM_THR 0.75
//Consensus absolute ending thresholds for sequences and symbol weights
#define END_ABS_W_SEQ_THR 1
#define END_ABS_W_SYM_THR 1
//Maximum number of inserted 'N's (in ambiguous cases)
#define MAX_INS_N 3

//Statistical Analysis
//Minimum Bitscore Threshold
#define MIN_BITSCORE_THR 20
//Minimum Bitscore/Aln_len Threshold
#define MIN_BITS_ALNLEN_THR 1.0
//Minimum Absolute Bitscore/Aln_len Threshold
#define MIN_ABS_BITS_ALNLEN_THR 0.4
//Maximum Number of Sequences for a "Small Statistic"
#define MAX_SEQ_SMALL_STAT_THR 10
//Maximum Number of Sequences for a "Big Statistic"
#define MAX_SEQ_BIG_STAT_THR 50
//Minimum Number of Statistics for being relevant
#define RELEVANT_STAT_THR 10
//Outlier Threshold
#define OUTLIER_THR 0.05
//Number of random generations of permutation test
#define PERM_TEST_RAND_GENERATIONS 512

//Allowed FASTA characters
extern char fasta_nucl[18];

#endif

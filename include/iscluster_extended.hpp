/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#ifndef _ISCLUSTEREXTENDED_HPP_
#define _ISCLUSTEREXTENDED_HPP_

#include "iscluster.hpp"

class ISClusterExtended : public ISCluster {
public:
    std::list< string > weight_labels; // List of read ids of the cluster
    int r2_weight; // Quantification based on R2 reads
    int tag_weight; // Quantification based on UMI tags
    int combo_weight; // Quantification based on R2 + UMI tags
    std::list< string > r2_cons_seq; // List of R2 consensus (corresposnding to 'r2_weight')
    std::list< string > tag_cons_seq; // List of UMI tag consensus (corresposnding to 'tag_weight')
    std::list< string > combo_cons_seq; // List of combo consensus (corresposnding to 'combo_weight')

    ISClusterExtended(ISCluster& c) :
	ISCluster(c),
	weight_labels(),
	r2_weight(0),
	tag_weight(0),
	combo_weight(0),
	r2_cons_seq(),
	tag_cons_seq(),
	combo_cons_seq() {}

    ISClusterExtended(const string& _clu_id = "",
		      const int _weight = 0,
		      const Alignments& al = Alignments()) :
	ISCluster(_clu_id, _weight, al),
	weight_labels(),
	r2_weight(0),
	tag_weight(0),
	combo_weight(0),
	r2_cons_seq(),
	tag_cons_seq(),
	combo_cons_seq() {}

    ISClusterExtended(const string& _clu_id,
	      const int _num_aln,
	      const int _max_aln_score,
	      const int _weight,
	      const Label& l,
	      const std::list< string >& _masked) :
        ISCluster(_clu_id, _num_aln, _max_aln_score, _weight, l, _masked),
	weight_labels(),
	r2_weight(0),
	tag_weight(0),
	combo_weight(0),
	r2_cons_seq(),
	tag_cons_seq(),
	combo_cons_seq() {}
    
    DECLARE_INTERNAL_TYPE_INFO();
};

#endif

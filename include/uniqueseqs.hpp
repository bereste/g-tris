/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#ifndef _UNIQUESEQS_HPP_
#define _UNIQUESEQS_HPP_

#include <algo/blast/format/blast_format.hpp>
#include <serial/serialimpl.hpp>

USING_NCBI_SCOPE;
USING_SCOPE(objects);

//Unique FASTA ID mapping class
class UniqueSeqs {
public:
    //Map of original FASTA ids:
    // key   -> unique FASTA id
    // value -> list of FASTA ids having the same sequence
    std::map< string, std::list< string > > fasta_id_map;
    DECLARE_INTERNAL_TYPE_INFO();
};

#endif

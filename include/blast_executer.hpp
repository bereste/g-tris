/**
 *
 *                           γ-TRIS
 *
 *       Graph-Algorithm Making Multi-labeling Annotations
 *             for Tracking Retroviral Integration Sites
 *
 * Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 *
 * Distributed under the terms of the GNU General Public License (or the Lesser
 * GPL).
 *
 * This file is part of γ-TRIS.
 *
 * γ-TRIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * γ-TRIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 *
**/

#ifndef _BLAST_EXECTUER_HPP_
#define _BLAST_EXECUTER_HPP_

#include "log.hpp"
#include "alignments.hpp"
#include "configuration.hpp"

#include <algo/blast/api/local_blast.hpp>
#include <algo/blast/api/objmgr_query_data.hpp>
#include <algo/blast/blastinput/blast_fasta_input.hpp>
#include <algo/blast/api/blast_nucl_options.hpp>

#include <ncbi_pch.hpp>
#include <corelib/ncbiapp.hpp>
#include <algo/blast/api/local_blast.hpp>
#include <algo/blast/api/remote_blast.hpp>
#include <algo/blast/blastinput/blast_fasta_input.hpp>
#include <algo/blast/blastinput/blastn_args.hpp>
#include <algo/blast/api/objmgr_query_data.hpp>
#include <algo/blast/format/blast_format.hpp>

#include <serial/objistr.hpp>
#include <serial/objostr.hpp>
#include <serial/serialimpl.hpp>
#include <serial/serial.hpp>
#include <serial/iterator.hpp>
#include <objmgr/util/sequence.hpp>

USING_NCBI_SCOPE;
USING_SCOPE(objects);
USING_SCOPE(blast);

struct BlastExecuter{
private:
    string infasta;
    string blast_db;
    string outasn;
#ifdef PRINT_CSV_ALN
    string outcsv;
#endif
    string outmissing;
    int word_size;
    double evalue;
    double perc_identity;
    int batch_size;
    int num_threads;
    bool force_strand_plus;
public:
    int num_input_seqs;
    int num_aln_seqs;
    int tot_alignments;
    int num_missing_seqs;
    BlastExecuter(const string& _infasta,
                  const string& _blast_db,
                  const string& _outasn,
#ifdef PRINT_CSV_ALN
		  const string& _outcsv,
#endif
		  const string& _outmissing,
                  const int _word_size,
                  const double _evalue,
                  const double _perc_identity,
                  const int _batch_size,
                  const int _num_threads,
		  const bool _force_strand_plus) :
    infasta(_infasta),
    blast_db(_blast_db),
    outasn(_outasn),
#ifdef PRINT_CSV_ALN
    outcsv(_outcsv),
#endif
    outmissing(_outmissing),
    word_size(_word_size),
    evalue(_evalue),
    perc_identity(_perc_identity),
    batch_size(_batch_size),
    num_threads(_num_threads),
    force_strand_plus(_force_strand_plus),
    num_input_seqs(0),
    num_aln_seqs(0),
    tot_alignments(0),
    num_missing_seqs(0) {}

/// Modify BLAST options from defaults based upon command-line args.
///
/// @param opts_handle already created CBlastOptionsHandle to modify [in]
  void processCommandLineArgs(CRef<CBlastOptionsHandle> blast_opts_handle,
                              const int word_size,
                              const double evalue,
                              const double perc_identity,
			      const bool force_strand_plus) {
    blast_opts_handle->SetDefaults();
    // Sets BLAST strand mapping option
    if(force_strand_plus) {
	blast_opts_handle->SetOptions().SetStrandOption(eNa_strand_plus);
    }
    // Sets BLAST WordSize
    blast_opts_handle->SetOptions().SetWordSize(word_size);
    // Sets BLAST Evalue
    blast_opts_handle->SetEvalueThreshold(evalue);
    // Sets BLAST PercentIdentity
    blast_opts_handle->SetPercentIdentity(perc_identity);
  }

  int executeBlast() {
      //Create object manager and scope
      CRef< CObjectManager > objmgr = CObjectManager::GetInstance();
      CScope scope(*objmgr);

      //Crete BLAST option handler
      CRef< CBlastOptionsHandle > opts(CBlastOptionsFactory::Create(eBlastn));
      processCommandLineArgs(opts,
                             word_size,
                             evalue,
                             perc_identity,
			     force_strand_plus);
      opts->Validate();

      //Create BLAST input query
      SDataLoaderConfig dlconfig(infasta, false);

      CBlastInputSourceConfig iconfig(dlconfig,
                                      objects::eNa_strand_other,
                                      false,
                                      true);

      CNcbiIfstream in_fasta_stream(infasta.c_str());
      if(!in_fasta_stream.is_open()) {
          ERROR("Error in opening " << infasta << " file.");
          return 1;
      }
      CBlastFastaInputSource fasta_input(in_fasta_stream, iconfig);
      CBlastInput blast_input(&fasta_input);
      blast_input.SetBatchSize(batch_size);

      auto_ptr< CObjectOStream >
          out_asn_stream(CObjectOStream::Open(ASN_TYPE, outasn));
      if(!out_asn_stream->InGoodState()) {
          ERROR("Problem in creating " << outasn << " file.");
          return 1;
      }

      CNcbiOfstream out_missing_stream(outmissing.c_str());
      if(!out_missing_stream.is_open()) {
          ERROR("Error in creating " << outmissing << " file.");
          return 1;
      }
#ifdef PRINT_CSV_ALN
      CNcbiOfstream out_csv_stream(outcsv.c_str());
      if(!out_csv_stream.is_open()) {
          ERROR("Error in creating " << outcsv << " file.");
          return 1;
      }
#endif
      for (; !blast_input.End();) {
          CRef< CBlastQueryVector >
              query_loc(blast_input.GetNextSeqBatch(scope));
          //CRef<CBlastQueryVector> query_loc(blast_input.GetAllSeqs(scope));
#pragma omp critical
	  {
          INFO("Parsing batch of " << query_loc->Size() << " input sequences.");
	  }
          num_input_seqs += query_loc->Size();
          CRef< IQueryFactory > query_factory(new CObjMgr_QueryFactory(*query_loc));

          //Access BLAST DB
          const CSearchDatabase target_db(blast_db,
                                          CSearchDatabase::eBlastDbIsNucleotide);

          //Create and execute BLAST
          {{
              CLocalBlast blaster(query_factory, opts, target_db);
              blaster.SetNumberOfThreads(num_threads);
              CRef<CSearchResultSet> blastn_results = blaster.Run();

              // Get warning messages.
              for (unsigned int i = 0;
                   i < (*blastn_results).GetNumResults(); i++) {
                TQueryMessages messages =
                    (*blastn_results)[i].GetErrors(eBlastSevWarning);
                  if (messages.size() > 0) {
                      CConstRef< CSeq_id > seq_id =
                          (*blastn_results)[i].GetSeqId();
                      if (seq_id.NotEmpty()) {
                          WARN("ID: " << seq_id->AsFastaString());
                      } else {
                          WARN("ID: " << "Unknown");
                      }
                      ITERATE(vector< CRef< CSearchMessage > >, it, messages)
                          WARN((*it)->GetMessage());
                  }
              }

              //Process BLAST output
              int localId = 0;
              NON_CONST_ITERATE(CSearchResultSet, SetIter, *blastn_results) {
                  CSearchResults& Results = **SetIter;
                  CConstRef< CSeq_align_set > AlignSet = Results.GetSeqAlign();
                  if (!AlignSet->IsEmpty()) {
                      num_aln_seqs++;
                      Alignments al;
                      al.query_id =
                          (*query_loc)[localId]->GetQueryId()->GetSeqIdString();
                      al.query_len = (*query_loc)[localId]->GetLength();
                      al.max_score = 0;
                      // CSeq_align_set::Tdata::const_iterator AlignIter;
                      // for(AlignIter = AlignSet->Get().begin();
                      //     AlignIter != AlignSet->Get().end(); ++AlignIter) {
                      ITERATE (CSeq_align_set::Tdata, AlignIter, AlignSet->Get()) {
                          ++tot_alignments;
                          int aln_score;
                          (*AlignIter)->GetNamedScore(CSeq_align::eScore_BitScore,
                                                      aln_score);
                          AlnData data((*AlignIter)->GetSeq_id(1).GetSeqIdString(),
                                       (*AlignIter)->GetSeqStart(0),
                                       (*AlignIter)->GetSeqStop(0),
                                       (*AlignIter)->GetSeqStart(1),
                                       (*AlignIter)->GetSeqStop(1),
                                       ((*AlignIter)->GetSeqStrand(0) ==
                                        eNa_strand_minus ? -1 : +1),
                                       ((*AlignIter)->GetSeqStrand(1) ==
                                        eNa_strand_minus ? -1 : +1),
                                       (*AlignIter)->GetAlignLength(),
                                       aln_score);
                          al.al_data.push_back(data);
                          if (aln_score > al.max_score) {
                            al.max_score = aln_score;
                          }
                      }
                      *out_asn_stream << al;
#ifdef PRINT_CSV_ALN
                      printAln(al, out_csv_stream);
#endif
                  } else {
                      num_missing_seqs++;
                      out_missing_stream <<
                          (*query_loc)[localId]->GetQueryId()->GetSeqIdString() <<
                          std::endl;
                  }
                  ++localId;
              }
              out_asn_stream->Flush();
#ifdef PRINT_CSV_ALN
              out_csv_stream.flush();
#endif
            }}
      }
#ifdef PRINT_CSV_ALN
      out_csv_stream.close();
#endif
      out_missing_stream.close();
      return 0;
  }

};

#endif

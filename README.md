# γ-TRIS
γ-TRIS (Graph-Algorithm Making Multi-labeling Annotations for Tracking Retroviral Integration Sites) is a graph-based method capable of handling vector insertions in gene therapy analysis even if embedded in repeated sequences allowing the generation of a comprehensive repertoire of insertion sites.

---
### Prerequisites
* gcc/g++ compiler
* make
* cmake
* zlib
* git
* GraphViz (optional)
* R (optional)

The optional programs are used to produce the graphical images of some steps of the pipeline (e.g. all-vs-all alignment graph, graph decomposition, statistical analysis).
In addition to the previous packages, it is necessary to index both the genomes used in the alignment steps with BWA and BLAST. To do this, it is possible to use the executables in the bin/ folder of the program, obtained after its installation (see next section). More precisely, to generate the BLAST database of the reference genome and also the viral one, it is possible to use the makeblastdb program (see Execution section for details) and the _bwa program (which is the standard BWA tool obtained by compiling its source code) to generate the indexes for BWA.

---
### Download and Installation
The software is available at Bitbucket (https://bitbucket.org/bereste/g-tris).

For Linux users, the easiest way to obtain the program is to clone the git repository:
```bash
$ git clone git@bitbucket.org:bereste/g-tris.git
```
or download the [zip](https://bitbucket.org/bereste/g-tris/get/v1.0.zip) version and uncompress the file.

To install it type:
```bash
$ cd g-tris
$ make prerequisites
$ make
```
The software is also available on [Docker](https://www.docker.com/).

For the correct execution, the reference genomes (and their indexes) must be available.
They can be downloaded [here](https://drive.google.com/open?id=1zBN5WZHHuT1dbVEEOO-92WLi9_0fguYY) and extracted in a local directory (e.g. `<g-data_dir>`).

To download and run the docker container type:
```bash
$ docker pull imerelli/g-tris
$ docker run -v <g-data_dir>:/opt/g-data -i -t imerelli/g-tris /bin/bash
$ cd /home/g-tris
```


---
### Pre-processing
Since γ-TRIS is focused on the identification of IS from trimmed sequences, γ-TRIS is released without a specific module for pre-processing FASTQ files that may varies according to custom experimental designs and protocols. 
γ-TRIS works with input files in FASTA format containing sequences of only the genomic portion trimmed by all vector sequences (such as long terminal repeat - LTR, or linker cassette - LC, barcode sequences, etc.). 

To process Illumina FASTQ files and obtain input FASTA files for γ-TRIS, we suggest to use [Flexbar](http://sourceforge.net/projects/flexbar/) and EA-Utils, as described in the manuscript of γ-TRIS (Supplementary Section).

---
### Execution
**Full Pipeline**

For the execution of the pipeline we provided a bash script (`exec_pipeline.sh`) which takes as input a FASTA file containing the reads and runs all the steps based on the given configuration file (`config.ini`) containing all the parameters for the execution. This latter file must be in the same folder from which the bash script is executed (or, alternatively, it is possible to change it in the script), so the easiest way to execute the entire pipeline is to put the input FASTA file and the `config.ini` file in the same folder, from which to run the bash script.

To run the pipeline type:
```bash
$ <path-to-g-tris>/exec_pipeline.sh <fasta_file>
```
Here is an example of the given `config.ini` file containing all the execution parameters (with default values):
```
# Organism
# options (case sensitive): human (default) and mouse
ORGANISM=human

# Decomment to avoid final cluster computation
# FINAL_CONSENSUS="-no-consensus"

#Execution Parameters
BLAST_PERC_ID=90 #Blast Identity Percentage
BLAST_EVALUE=0.01 #Blast Evalue
DEC_LEV=2 #Graph Decomposition Level (1, 2, or 3)
INIT_COV=0.75 #Consenus Inivital Coverage Threshold
INIT_FREQ=0.75 #Consenus Inivital Frequence Threshold
MED_COV=0.75 #Consenus Medium Ending Coverage Threshold
MED_FREQ=0.75 #Consenus Medium Ending Frequence Threshold
HIGH_COV=0.75 #Consenus High Ending Coverage Threshold
HIGH_FREQ=0.75 #Consenus High Ending Frequence Threshold
N_THREADS=4 #Number of threads

#Reference Directories
PROG_DIR="<path-to-program-dir-bin>"
VIRAL_GEN="<path-to-viral-geome-file.fasta>"
HUMAN_GEN="<path-to-human-genome-file.fasta>"
```
**Sigle Steps**

Here a detailed list of the programs implementing the steps of the pipeline:

1. `makeblastdb` is used to create the BLAST database, given a FASTA file as input.

        makeblastdb -i <FASTA_FILE> [-max_file_sz <SIZE>]
                    [-uniqueseqs] [-logfile <LOG_FILE>]

    Description:

    * `-i <FASTA_FILE>`: (mandatory) input FASTA file on which to build the BLAST db.
    * `-uniqueseqs`: from the input file fasta, compute only on unique sequences (unique by sequence and not by id). TRUE if set, FALSE if not set.
    * `-max_file_sz <SIZE>`: maximum file size for BLAST database files. Default = 1GB.
    * `-logfile <LOG_FILE>`: file to which redirect the program log. Default = -.
    
 
2. `seqalignment` performs the BLAST alignment.

        seqalignment -i <FASTA_FILE> [-db <DB_NAME>] [-perc_identity <PERC_ID>]
                     [-evalue <E_VALUE>] [-batch_size <BATCH_SIZE>]
                     [-num_threads <N_THREADS>] [-word_size <WORD_SIZE>]
                     [-forcestrandplus]

    Description:

    * `-i <FASTA_FILE>`: (mandatory) input FASTA file of all the sequences.
    * `-db <DB_NAME>`: BLAST database name. Default: input FASTA file name.
    * `-perc_identity <PERC_ID>`: percentage of identity cutoff used by BLAST. Default = 95.
    * `-evalue <E_VALUE>`: expect value (E) used by BLAST for saving hits. Default: 0.01.
    * `-batch_size <BATCH_SIZE>`: number of characters of the sequences to be processed for every batch of the BLAST alignment. Default: 1,000,000 (1M).
    * `-num_threads <N_THREADS>`: number of threads used by BLAST. Default: 1.
    * `-word_size <WORD_SIZE>`: BLAST word size. Default: 11.
    * `-forcestrandplus`: force BLAST to align sequences only on strand plus. TRUE if set, FALSE if not set.

3. `seqfilter` filters the input sequences, based on BLAST alignments (used for initial filter).

        seqfilter -i <FASTA_FILE> -aln <ALN_FILE>


    Description:

    * `-i <FASTA_FILE>`: (mandatory) input FASTA file of all the sequences.
    * `-aln <ALN_FILE>`: (mandatory) input file in ASN format containing the alignments of BLAST of the reads to be filtered.

4. `analyzegraph_SNAP` creates the alignment graph starting from the BLAST alignments and performs its decomposition using the SNAP library.

        analyzegraph_SNAP -i <ALN_FILE> -f <FASTA_FILE> [-m <MAP_FILE>]
                     [-dec_lev <LEVEL>]

    Description:

    * `-i <ALN_FILE>`: (mandatory) input file in ASN format containing the alignments of BLAST.
    * `-f <FASTA_FILE>`: (mandatory) input FASTA file of all the sequences.
    * `-m <MAP_FILE>`: file containing the map of unique sequences obtained with option `-uniqueseqs` of `makeblastdb` program.
    * `-dec_lev <LEVEL>`: graph decomposition level: (1) connected components, (2) remove bridge edges, (3) quasi-cliques. Default: 2.

5. `analyzeseqs_SNAP` computes the consensus sequences for the computed subgraphs (resulting from the last step of the decomposition) using ClustalW and the SNAP library.

        analyzeseqs_SNAP -subg <SUBGRAPH_FILE> -nodemap <NODEMAP_FILE> [-m <MAP_FILE>]
                    [-init_cov <INIT_COV>] [-init_freq <INIT_FREQ>]
                    [-med_cov <MED_COV>] [-med_freq <MED_FREQ>]
                    [-high_cov <HIGH_COV>] [-high_freq <HIGH_FREQ>]

    Description:

    * `-subg <SUBGRAPH_FILE>`: (mandatory) input subgraph file, output of `analyzegraph`.
    * `-nodemap <NODEMAP_FILE>`: (mandatory) input node_id map file, output of `analyzegraph`.
    * `-m <MAP_FILE>`: file containing the map of unique sequences obtained with option `-uniqueseqs` of `makeblastdb` program.
    * `-init_cov <INIT_COV>`: consensus beginning sequence coverage threshold (Initial Sequence Coverage). Default: 0.75.
    * `-init_freq <INIT_FREQ>`: consensus beginning symbol similarity threshold (Initial Symbol Frequency). Default: 0.75.
    * `-med_cov <MED_COV>`: consensus medium ending sequence coverage threshold (Medium Sequence Coverage). Default: 0.75.
    * `-med_freq <MED_FREQ>`: consensus medium ending symbol similarity threshold (Medium Symbol Frequency). Default: 0.75.
    * `-high_cov <HIGH_COV>`: consensus high ending sequence coverage threshold (High Sequence Coverage). Default: 0.75.
    * `-high_freq <HIGH_FREQ>`: consensus high ending symbol similarity threshold (High Symbol Frequency). Default: 0.75.

6. `iterativealignment` performs the BWA + iterative BLAST alignment of the given sequences in the FASTA file, against a genome.

        iterativealignment -i <FASTA_FILE> [-db <DB_NAME>] [-mask <MASK_ALN_FILE>]
                           [-batch_size <BATCH_SIZE>] [-num_threads <N_THREADS>]

    Description:

    * `-i <FASTA_FILE>`: (mandatory) input FASTA file of all the sequences.
    * `-db <DB_NAME>`: BLAST database name. Default: input FASTA file name.
    * `-mask <MASK_ALN_FILE>`: RepeatMasker out file. Default: none.
    * `-batch_size <BATCH_SIZE>`: number of characters of the sequences to be processed for every batch of the BLAST alignment. Default: 1,000,000 (1M).
    * `-num_threads <N_THREADS>`: number of threads. Default: 1.

7. `createclusters_SNAP` computes the final clusters starting from the computed genomic and repeat alignments, by rolling-back the graph decomposition structure.

        createclusters_SNAP -i <CONS_ALN_FILE> -f <CONS_FASTA_FILE>
                       -subg <SUBGRAPH_FILE> [-m <MAP_FILE>]
                       [-approx]

    Description:

    * `-i <CONS_ALN_FILE>`: (mandatory) input file in ASN format containing the alignments of the consensus sequences obtained with the iterativealignment program.
    * `-f <CONS_FASTA_FILE>`: (mandatory) input FASTA file of the consensus sequences.
    * `-subg <SUBGRAPH_FILE>`: (mandatory) input subgraph file, output of `analyzegraph`.
    * `-m <MAP_FILE>`: file containing the map of unique sequences obtained with option `-uniqueseqs` of `makeblastdb` program.
    * `-approx`: compute potentially not the optimal cluster at each iteration. TRUE if set, FALSE if not set.

8. `analyzeclusters_paral` performs the statistical analysis of the computed clusters.

        analyzeclusters_paral -i <CLUSTER_FILE> [-m <MAP_FILE>]
                              [-num_threads <N_THREADS>]
                              [-init_cov <INIT_COV>] [-init_freq <INIT_FREQ>]
                              [-med_cov <MED_COV>] [-med_freq <MED_FREQ>]
                              [-high_cov <HIGH_COV>] [-high_freq <HIGH_FREQ>]

    Description:

    * `-i <CLUSTER_FILE>`: (mandatory) input file of serialized clusters (.clu).
    * `-m <MAP_FILE>`: file containing the map of unique sequences obtained with option `-uniqueseqs`of `makeblastdb` program.
    * `-num_threads <N_THREADS>`: number of threads. Default: 1.
    * `-init_cov <INIT_COV>`: consensus beginning sequence coverage threshold (Initial Sequence Coverage). Default: 0.75.
    * `-init_freq <INIT_FREQ>`: consensus beginning symbol similarity threshold (Initial Symbol Frequency). Default: 0.75.
    * `-med_cov <MED_COV>`: consensus medium ending sequence coverage threshold (Medium Sequence Coverage). Default: 0.75.
    * `-med_freq <MED_FREQ>`: consensus medium ending symbol similarity threshold (Medium Symbol Frequency). Default: 0.75.
    * `-high_cov <HIGH_COV>`: consensus high ending sequence coverage threshold (High Sequence Coverage). Default: 0.75.
    * `-high_freq <HIGH_FREQ>`: consensus high ending symbol similarity threshold (High Symbol Frequency). Default: 0.75.


There are other executable programs in the bin/ directory which are used by the ones listed above, or perform some other functionalities.

---
### Output

After the execution of the pipeline, the folder `iteration0` will contain the output of all the steps and the log files.

The final output files with the predicted integration sites (clusters) are:

* `unique_<input_file_name>.filt.out.all.noprobl.bed`

    BED file containing the labels (genomic positions) of the predicted clusters;

* `unique_<input_file_name>.filt.out.all.noprobl.json`

    JSON file containing the summery of the predicted clusters;

* `unique_<input_file_name>.filt.out.all.noprobl.clu`

    structured file in ASN format containing all the details of the predicted clusters.

---

### Examples
The folder `example-data` contains 9 example datasets with 1000 Integration Sites each.
They have been obtained by simulating the extraction of reads at 3 different coverages (2or3k, 100k, and 250k reads) with 3 different error perturbations (none, X=0.25%, and 2X=0.5%).

To execute the pipeline on a single example dataset type:

```bash
$ cd <single-example-folder>
$ gunzip <read-file>.fa.gz
$ <path-to-g-tris>/exec_pipeline.sh <read-file>.fa
```
---
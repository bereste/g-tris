###
 #
 #                           γ-TRIS
 #
 #       Graph-Algorithm Making Multi-labeling Annotations
 #             for Tracking Retroviral Integration Sites
 #
 # Copyright (C) 2015, 2016 Stefano Beretta, Andrea Calabria, Ivan Merelli, Yuri Pirola
 #
 # Distributed under the terms of the GNU General Public License (or the Lesser
 # GPL).
 #
 # This file is part of γ-TRIS.
 #
 # γ-TRIS is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 # 
 # γ-TRIS is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 # GNU General Public License for more details.
 # 
 # You should have received a copy of the GNU General Public License
 # along with γ-TRIS. If not, see <http://www.gnu.org/licenses/>.
 #
###

# Based on the idea/skeleton presented at
# http://mad-scientist.net/make/multi-arch.html

.SUFFIXES:

# Directories.
export BASE_DIR:=$(CURDIR)
export SRC_DIR:=$(BASE_DIR)/src
export LOC_DIR:=$(BASE_DIR)/local
export 3RD_DIR:=$(BASE_DIR)/thirdparty
export BIN_DIR:=$(BASE_DIR)/bin
export OBJ_DIR:=$(BASE_DIR)/$(OBJ_DIR)

MAKETARGET = $(MAKE) --no-print-directory -C $@ -f $(CURDIR)/Makefile \
	$(MAKECMDGOALS)

.PHONY: $(OBJ_DIR)
$(OBJ_DIR):
	+@[ -d $@ ] || mkdir -p $@
	+@$(MAKETARGET)

Makefile : ;
%.mk :: ;

% :: $(OBJ_DIR) ;



# The clean and depclean targets should stay here since they
# delete the directory which the main Makefile is executed from.

.PHONY: clean
clean:
	@echo '* Cleaning objects and binaries...' ; \
	rm -rf $(OBJ_DIR) $(BIN_DIR)

.PHONY: depclean
depclean: clean
	@echo '* Cleaning local dir...' ; \
	rm -rf $(LOC_DIR) ; \
	echo '* Cleaning pre-requisites...' ; \
	$(MAKE) -C $(3RD_DIR) clean-prerequisites
